# Changelog
Notable changes to NEPHAST are recorded here.



## Current Release - [0.11.2] - 2019-4-23

### Added
- Gaussian analytic PSF.

### Changed
- Equation format and other small items in the Moffat PSF docstring.
- Calculate arithmetic and geometric mean FWHMa in Psf.Analytic.
- Characteristic radii variables renamed.

### Fixed
- Severe bug in the fitting procedure.

### Removed
- References to the first and second characteristic PSF radii as long and
  short, repectively.



## [0.11.1] - 2019-4-4

### Added
- Luminosity function plot can have title.
- Sharpness limits in `analyze.plot_find`.
- Save background value to catalog of stars found.

### Changed
- Increased space between `analyze.plot_find` subplots.

### Fixed
- Error messages in `analyze.plot_cat`.
- Repetition of `plt.subplots_adjust` in `analyze.py`.
- Error message in `fitter.__setattr__`.
- Docstring in `Catalog.__init__`.

### Removed
- Several `save_obj` in `multi_star.py`.



## [0.11] - 2019-3-28

### Added
- Saturated star in `double_star.py`.
- `find` and `find_default` modules.
- Find stars by fitting a PSF.
- Find stars in `multi_star.py`.
- PSF can be evaluated without outer radius.
- PSF can be evaluated within a custom radius.
- PSF flag for spatial variability.
- `__all__` statements in submodules.
- Catalogs plotted on images can optionally show stars' names.
- Script `find_star.py` to test finding objects.
- Store minimum and maximum image values as attributes.
- Functions to get and set items in catalog and image objects.
- Store total number of pixels of an image as attribute.
- `Catalog.add_col` can use masked arrays.
- `Catalog.range` can keep or reject masked values.
- `Catalog.match` outputs also the difference catalogs.
- Plot the luminosity function of a catalog.
- Check if the catalogs in `analyze.plot_quiver` are matched.
- Plot the photometric and astrometric difference of two matched catalogs.

### Changed
- Docstring in `double_star.py`.
- Email and keywords in `setup.py`.
- Docstring in `multi_star.py`.
- `multi_star.py` overplots `cat_in` to the image.
- Magnitude difference is not recorded in `multi_star.py`.
- `original` in `Catalog.intensity_transf` has no default value.
- When initializing a `catalog.Catalog` object, the original intensity can be
  chosen.
- When stacking catalogs, the original intensity can be chosen.
- Many image and catalog functions generate an intermediate array to force
  invoking `__setattr__`.
- `fitter.Fitter` is callable in place of `fit_all`.
- Several configuration parameters can be `numpy.integer`.
- The `sides` configuration parameter can not be a tuple.
- All use of `issubclass` substituted with `isinstance`.

### Fixed
- Docstrings in `psf.Psf`.
- Empty line printed at the end of `Fitter.fit_all`.
- `mesh_grid_flat` in `Psf.psf_pos` is made of integers.
- `mesh_grid` in `Psf.eval_psf` is made of integers.
- `mesh_grid` in `Psf.eval_prof` is made of integers.
- Docstring in `find_star_config.py`.
- `Catalog.rnd` parameter can be NumPy integer.
- Tabulation of several `__set_attr__` functions.

### Removed
- Configuration input in `analyze.plot_history`.
- `Catalog.diff` was not being used.



## [0.10.9] - 2019-2-3

### Added
- Close double star in `double_star.py`.
- Check step size in the fitting iteration.

### Changed
- `near_tol` docstring.
- Default value of `near_tol` is 0.2.

### Fixed
- Quiver plot can have limits and equal ratios.



## [0.10.8] - 2019-2-2

### Added
- Load partial list of configuration parameters from file.
- `misc` folder in `scripts`.
- SNR calculations during fitting.

### Changed
- Load partial list of configuration parameters from file in `multi_star.py`.
- `catalog.Catalog` docstring.
- `observation.Observation` docstring.
- `eval_prof` is masked during fitting.
- Docstring of `image.noise` states that input variance can be `numpy.ndarray`.

### Fixed
- Removed duplicate install requirement in `setup.py`.



## [0.10.7] - 2019-1-31

### Added
- Saturation value in `multi_star.py`.
- Saturation attribute in `image.Image`.
- `image.Image` checks for saturation values when the image `image.array` is
  changed.

### Changed
- Saturation value docstring.
- Empty lines reduced in docstrings.

### Fixed
- High masked value in `mask_val`.
- `analyze` package listed in `__init__.py`.
- `match.py` uses configuration.



## [0.10.6] - 2019-1-30

### Added
- Unmasked bad pixels in `multi_star.py`.
- Check `psfun` parameter when creating a catalog.
- Define `psfun` parameter when creating a random catalog.
- Quiver plot has buffer zone, scale and key scale.

### Changed
- Sigma clipping can be used before stars subtraction.
- PSF functions are calculated for a single pixel.
- Default PSF function evaluation density is 20.

### Fixed
- `pixel_eval.py` plots and messages.



## [0.10.5] - 2019-1-29

### Added
- Isolated star in `double_star.py`.
- Star subtraction and sigma-clipping can be turned off.

### Changed
- `debug` submodule renamed `analyze`.
- Catalog printing moved to `analyze`.
- Image plotting copied in `image.Image`.
- History plotting copied in `fit.Fitter`.
- Catalog plotting copied in `catalog.Catalog`.
- Quiver plotting copied in `catalog.Catalog`.
- `Image.add_bkg_px` does not change the input argument `px`.
- Number of random bad pixels in `multi_star.py` is a variable.
- Bacground slopes in a catalog are referred to a (`bkgx0`,`bkgy0`) pixel.
- No fitting history plot in `multi_star.py`.
- Image from catalog does not add noise by default.
- Mask sigma-clipped pixels only if there are any.
- Default sigma-clipping coefficient is 3.5.

### Fixed
- Masked pixels are removed correctly from the fitting.
- Reduced chi^2 plot in `multi_star.py`.
- Star subtraction and sigma-clipping can have the same value.
- Argument type of background gradients in `Image.add_bkg`.
- Background fitting.
- `analyze.show_image` has fixed image limits.
- Exclusion of masked pixels during fitting.
- Median background is in output catalog.
- Sigma-clipping works with pixel masking.



## [0.10.4] - 2019-1-21

### Added
- Check if `iter_sigma` in configuration is not greater than `iter_sub`.

### Changed
- Fitting gain is halved automatically.
- Near stars are removed during fitting only starting with the iteration that
  subtracts stars.
- Shorter fitting history plots.
- Show different catalogs on images plotted in `double_star.py`.

### Fixed
- Magnitude guesses in `double_star.py`.
- Configuration parameter values are checked in pairs.



## [0.10.3] - 2019-1-20

### Added
- Pixels sigma-clipping during iteration.
- Quiver plot in `double_star.py`.
- Star name on fitting history plot.
- Image plotting can show a catalog overlaid.
- Dynamic fitting gain.

### Changed
- Variance is calculated earlier in the fitting.
- `double_star.py` has more examples of "ad hoc" fitting tests.
- Quiver plot in `multi_star.py`.
- `config.max_fit` is 50 by default.
- Fitting routine rearranged for sigma-clipping.
- `config.iter_sub` is used also for sigma-clipping.
- Position of `psf_radius` changed in `config.Config`.

### Fixed
- Stars that don't converge are dropped correctly.
- Stars don't have negative counts while fitting.
- `Psf.eval_prof` docstring.
- Stars dropped after last iteration are removed from model image.

### Removed
- A star is not debugged during fitting.
- The output catalog variable is not initialized at the beggining of the
  fitting.



## [0.10.2] - 2019-1-13

### Added
- Faint stars are dropped.
- Faint star in `double_star.py`

### Changed
- Legend of fitting history is in black.

### Fixed
- Saturation recording in the configuration.



## [0.10.1] - 2019-1-12

### Added
- Stars outside the image are dropped.
- Indices respect to input catalog are a table column in `Fitter.cat_out` and
  `Fitter.cat_drop`.
- Drop flags are a table column in `Fitter.cat_drop`.
- `Catalog.print` shows string columns.
- `catalog.Catalog` docstring includes `n`.
- Catalogs register name of columns and their number.
- Function to add column to catalog.
- Fitting history records the number of iterations.
- Geometric Moffat FWHM.
- Stars are dropped if they are too close to others.

### Changed
- Default `config.fit_radius` is 5.
- Moffat FWHM is recorded in an attribute by initialization, not by function.
- `double_star.py` has with phantom stars.
- PSF FWHMa are recorded as attributes in the super class `psf.Psf`.
- The fitting output catalog is not calculated again at the end.
- `debug.plot_history` checks if the star has history.

### Fixed
- `single_star.py` and `double_star.py` work with latest updates.
- Data variance when measuring fitting weights is always positive.
- `debug.plot_history` doesn't use the output catalog.
- `exp` input parameter is checked in `Catalog.print`.
- The catalog used during a fitting iteration doesn't change during the
  iteration.
- `Catalog.neighbors` accepts `numpy.integer` as `idx`.

### Removed
- `Fitter.idx_drop` and `Fitter.idx_out`.
- `iter` column is not in catalogs by default.



## [0.10] - 2019-1-9

### Added
- Fitting convergence.
- Fitting history records change in reduced chi^2.
- Fitting history plot shows reduced chi^2 changes.
- Remove stars from catalog with `Catalog.remove`.
- `Fitter.fit_all` outputs the catalog of dropped stars.
- `Fitter.fit_all` outputs the indices of output catalogs respect to input
  catalog.
- Catalogs have a default `iter` column.

### Changed
- Fitting history is always saved.
- Fitting history of sigmas and Deltas has same length as relative quantities.
- Fitting history plots have buffer space between them.
- Final `Fitter.fit_all` verbose message shows also number of stars not
  converged.
- Default minimum and maximum number of iterations is 10 and 100, respectively.
- `Fitter.fit_all` output catalog does not have the stars removed from fitting.
- `Catalog.select` uses the `astropy.table.Table.copy` function.
- Catalog printing can deal with empty catalogs or completely masked columns.
- Removed unused code in `multi_star.py`.

### Fixed
- Not plotting the absolute values of fitted quantities in
  `debug.plot_history`.
- Absolute value symbol for Delta in a `debug.plot_history` plot.
- All `config.Config` attributes are transformed into `float` automatically.
- List of stars remaining at the end of an iteration is a deep copy of a
  temporary list.
- Fitting history plots have colored minor ticks.
- Stars with not enough good pixels are removed from fitting iterations.
- `debug.plot_history` accept all type of integer as input star indices.

### Removed
- `sigma_converge` is not used and is removed from fitting configuration.



## [0.9.5] - 2019-1-6

### Added
- Median background can be calculated using an inner radius.

### Fixed
- `fit_nbkg` equal to 0 is not used in `Catalog.nbkg_change`.
- `Catalog.nbkg_change` is used in the same way in `Fitter.fit_all` for
  `fit_nbkg` equal to 0 and 1.
- `config.Config` attributes are transformed into `float` automatically.



## [0.9.4] - 2019-1-4

### Changed
- `bkg0` renamed `bkgc` in all instances.

### Fixed
- Use of `fit_nbkg` equal to 0 in `Catalog.nbkg_change` and `Fitter.fit_all`.
- History of changes of fitted parameters uses the gain value.



## [0.9.3] - 2018-12-19

### Added
- `catalog.Catalog.nbkg_change` can decrease number of background parameters.

### Changed
- Several objects' attributes are checked through `__setattr__`.
- `catalog.intensity_transf` allows to select the original intensity quantity.
- `catalog.Catalog.nbkg_change` works by masking/unmasking background columns
  of the catalog.

### Removed
- Function `catalog.Catalog.n`.
- Function `catalog.Catalog.check_nbkg`.
- Function `catalog.Catalog.fit_ready`.



## [0.9.2] - 2018-12-14

### Changed
- Fitting iteration verbose message does not show number of converged and
  dropped stars.
- The number of background parameters and fitting parameters is contained in
  `fit.Fitter` instead of `psf.PSF`.
- `image.Image.from_cat` uses the background in the `observation.Observation`
  by default.

### Fixed
- Background slope is fitted and stars do not have anymore an astrometric
  residual in the direction of the slope.
- Parameters `noise` and `bkg` are checked in `image.from_cat`.
- Parameter `bkg0` is checked in `observation.Observation` when parameters
  `bkgx` and `bkgy` are used.

### Removed
- `set_npar_bkg` from `psf.py`.



## [0.9.1] - 2018-12-9

### Added
- A `config.Config` object can be loaded from a chosen configuration file.
- `image.add_bkg` checks if the background is below 0.

### Changed
- `Psf.npar_bkg_change` renamed into `Psf.set_npar_bkg`.
- Default configuration splitted in `fit_default`, `instrument_default`,
  `observation_default`, `psf_default` and `work_default`.
- Most of default parameters for various objects have been moved to a
  configuration file.

### Fixed
- The package `setuptools` is required instead of `pkg_resources`.



## [0.9] - 2018-12-2

### Added
- Fitting is done using the pixels within the fitting radius `fit_radius`.

### Changed
- Configuration is an object `config.Config`.
- Parameter `config.fit_n_px` renamed to `config.fit_npx`.
- PSF and fitting radius are named `psf_radius` and `fit_radius`, respectively,
  in `psf.Psf`.
- `psf_radius` in `psf.Psf` can be float.

### Fixed
- Fitting history does not save background slopes if only a flat background is
  fitted.



## [0.8.2] - 2018-11-11

### Added
- Fitted stars must have a minimum number of good pixels.
- Show number of removed stars during `fit.fit_all`.
- Figures generated by `debug.py` can be showed using `debug.show_figures`.

### Changed
- Figures generated by `debug.py` are not showed immediately.

### Fixed
- In `catalog.Catalog`, `bkg0` can be negative.
- Input data for `debug.plot_2` can be masked.



## [0.8.1] - 2018-11-5

### Added
- Calculate differences between catalogs with `catalog.Catalog.diff`.

### Changed
- Some `catalog.Catalog` attributes are dynamic properties.
- Default magnitude distribution in `catalog.Catalog.rnd` is `unif`.



## [0.8] - 2018-11-5

### Added
- The `catalog.Catalog` object uses a masked table.
- Mask pixels based on position or value.
- Bad pixel simulation for images.
- Figure in `debug.show_img` can show the unmasked image.
- Figure title in `debug.show_img` figure.
- Axes tick labels offset in `debug.show_img` figure.
- A star can be debugged through `fit.fit_all`.

### Changed
- Layout of `debug.show_img` figure.

### Fixed
- Noise variance in `fit.fit_all` is measured on the correct patch of image.
- Correct axis labels in `debug.show_img`.
- Plots use `pyplot.show()` to display correctly.



## [0.7.1] - 2018-10-15

### Added
- 2D scatter plots of data.
- Stacking of `catalog.Catalog` instances.

### Changed
- Transformations between magnitude, counts and peak are done by a function.
- Number of background parameters in a catalog is checked by a function.
- Verbose fitting shows also the total number of iterations.

### Fixed
- Many docstrings for the `catalog.Catalog` object have removed the
  requirements for positions to be in px.



## [0.7] - 2018-8-9

### Added
- Method `Catalog.rnd` to create simulated catalog with random positions and
  magnitudes. Only uniform distribution is available at the moment.
- Method `Catalog.range` selects stars in a catalog with values within a range.
- Warning message class.
- Default working directory.
- `object.Object` class to create all other NEPHAST objects.
- Methods to save and load objects to and from files.
- Debug plot to show positions of up to two catalogs.
- Match catalogs in the same coordinates system using k-d tree.
- Script to test the matching algorithm
- Debug quiver plot of two matched catalogs.
- Script to test fitting of multiple stars.
- Function to print a `Catalog.catalog` object.

### Changed
- `double_star.py` test script has printed output.
- Plot repositioned in `debug.show_img`.
- Docstrings show default values.
- `debug.plot_hist` renamed `debug.plot_history`.
- Moffat profile has default theta and beta parameters.
- The `Fitter.fit_all` method returns the output catalog.
- `debug.plot_history` plots also the iterated values.
- Fitting history plot enhanced.

### Fixed
- `Catalog.__init__` deals correctly with `bkgx` and `bkgy` when they are
  arrays of NaN.
- `Catalog.__init__` checks correctly the sign of values in columns.
- Correct Moffat equation in docstring.
- Correct distance is used in `Catalog.neighbors`.
- Central star is removed from the `Catalog.neighbors` output.

### Removed
- `print_iter` from `debug.py`.



## [0.6] - 2018-7-13

### Added
- Fitting multiple stars.
- `double_star.py` test script.
- Method `Catalog.neighbors` to find stars close to one of them.
- Method `Image.sub_star` to subtract a star from an image.
- Method `Image.sub_stars` to subtract multiple stars from an image.
- Method `Image.add_stars` to add multiple stars to an image.
- Method `Catalog.select` to create a catalog from a subset of another.
- Fitting gain.
- Choose number of fitting iterations before using subtraction of other stars.
- Fitting progress can be checked by using `verbose` from `Fitter.fit_all`.

### Changed
- `Psf.radius` is integer.
- Use `copy.deepcopy` instead of `copy.copy`.
- `Catalog` accepts columns of NaN.
- `Catalog` accepts more than one column of `m`, `counst` and `peak`.
- Move `f` method from `Psf.Analog` to `Psf`.
- `Image.from_cat` uses `Image.add_stars`.

### Fixed
- Reduced chi^2 calculation.
- Integer input values for `Catalog` are accepted if in a `numpy.ndarray`.
- `None` input values for `Catalog` are accepted correctly.
- `Catalog` checks the `sigma_bkgy` input.

### Removed
- `catalog.Star` object and its uses.
- `verbose_star` from `Fitter.fit_all`.



## [0.5.1] - 2018-07-08

### Added
- `Catalog` has a column for the median value of background and its STD.

### Changed
- Single star test inputs.
- The variance during fitting can be measured on the model PSF during the first
  iteration.
- `Catalog` is generated with all columns, even those not provided. Columns not
  provided are left empty.
- When `Catalog` is generated, sigma values of magnitude, counts and peak are
  converted, if provided.
- The iteration printout shows the initial guess and the results at the end of
  each iteration.



## [0.5] - 2018-07-05

### Added
- Plot fitting history of a star.
- Iteration number is recorded in `Fitter.cat_out`.
- Warning messages.

### Changed
- The `Moffat` profile can be biaxial and rotated.
- Added axes labels to image shown.
- Iteration message is now a `debug` function.
- `error` module is now called `message`.
- The beta parameter of the `Moffat` profile has to be larger than 1 (suggested
  at least 2).
- The analytic profile derivative respect to counts is equal to the profile
  itself.

### Fixed
- Pixel evaluation test works again.
- Reduced chi^2 is in `Fitter.hist`.
- Data variance is measured in the correct order.
- Stamp size in `fit_all` has the correct size.

### Removed
- Last remnant of C code interface.



## [0.4.1] - 2018-06-26

### Changed
- Changelog improved.
- Requirements updated.
- All file headers include author, email and license.
- Single star test output display improved.

### Fixed
- Magnitude and peak sigmas are calculated in `fit_all`.
- sigma conversions involving float conversions and negative values is now
  correct.



## [0.4] - 2018-06-24

### Added
- Reduced chi^2 column in `Catalog`.
- sigma columns in `Catalog`.
- `Fitter` stores reduced chi^2 and sigmas.
- `fit_all` calculates reduced chi^2 and sigmas.
- `fit_all` stores the history of the fitted parameters at each iteration.
- Background can be fitted or measured as median from residuals.
- Fitted background can have a gradient.
- Gain and read noise are stored also in `Observation`.
- Subtract two `Image`.
- sigma conversions between magnitude, counts and peak value.
- Minimum and maximum number of fitting iterations.
- `fit_all` outputs a model image and residual image.

### Changed
- `Image` avoid negative values due to read noise.
- Several variables are now deep copied.
- Layout of `show_img` improved.
- `fit_all` print out during iterations improved.
- "Static" values are now referred to as "config" values.
- Single star test improved.

### Fixed
- Noise is calculated correctly.
- Reduced chi^2 is calculated instead of chi^2.



## [0.3] - 2018-06-13

### Added
- Option to choose the image or its model to determine fitting weights.
- Measure chi^2 during the fitting.
- Setup file.
- Package initialization file.
- Readme file.
- Test for pixel evaluation performance.
- PSF evaluation methods 'corners' and 'grid' (with different sampling
  densities).

### Changed
- Default noise generation with `Image.from_cat` is True.
- Test script for profile fitting of single star.

### Fixed
- Keep fitting only stars that haven't converged yet.

### Removed
- PSF evaluation method `integral_c`.



## [0.2] - 2018-04-18

### Added
- Open software license included.
- Background column implemented in `Catalog`.
- An `Image` object can contain also its `noise image`.

### Changed
- `Catalog` use strings for star names.
- Unused `Catalog` columns are filled with NaN.
- `Star` is an `astropy.table.Row` instance.
- `Fitter` uses a `Catalog` input for its initial guesses instead of lists.
- `fit_all` outputs a `Catalog` object.
- An `Image` object doesn't require a catalog to be initialized. That
  functionality has been moved to a new `from_cat` class method.
- The addition of noise is optional when creating a simulated image.

### Fixed
- An updated guess `Catalog` is kept during the fitting.



## [0.1] - 2018-03-26
Every project has a beginning. This is it.

### Added
- Generate catalog of stars.
- Select a single star from catalog.
- Convert between instrumental magnitudes, counts and peak value.
- `Instrument` object to store detector's parameters.
- `Observation` object to store observation's parameters.
- Only the Moffat analytic profile is available.
- *Testing: C code available to calculate analytic profile.*
- Simulate an image with stars and noise.
- Perform profile fitting on a single star.
- Produce plot of an image.
- Test script for profile fitting of single star.
- Test script for timing the generation of a profile.
