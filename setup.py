"""NEPHAST by Paolo Turri <pturri83@gmail.com>
Released under BSD License 2.0

Setup NEPHAST.
"""


from setuptools import setup, find_packages


setup(name='NEPHAST', version='0.11.2',
      description='Not Exactly a PHotometry or AStrometry Tool',
      long_description=open('readme.txt').read(),
      author='Paolo Turri', author_email='turri@berkeley.edu',
      license='BSD 3-clause',
      classifiers=['Programming Language:: Python:: 3.6',
                   'Development Status :: 2 - Pre-Alpha',
                   'License:: OSI Approved:: BSD License',
                   'Natural Language :: English',
                   'Intended Audience :: Science/Research',
                   'Topic :: Scientific/Engineering :: Astronomy'],
      keywords='astrometry photometry PSF-fitting',
      packages=find_packages(), python_requires='>=3',
      install_requires=['astropy', 'matplotlib', 'numpy', 'scipy',
                        'setuptools'])
