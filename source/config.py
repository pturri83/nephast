"""NEPHAST by Paolo Turri <pturri83@gmail.com>
Released under BSD License 2.0

This module is for setting the configuration parameters used in NEPHAST.
"""


from importlib import util
import os

import numpy as np

import message


__all__ = ['Config']


class Config:
    """Configuration object to store NEPHAST parameters.
    """

    def __init__(self, work='work_default', work_module='config_default',
                 instrument='instrument_default',
                 instrument_module='config_default',
                 observation='observation_default',
                 observation_module='config_default', psf='psf_default',
                 psf_module='config_default', find='find_default',
                 find_module='config_default', fit='fit_default',
                 fit_module='config_default'):
        """Initialize the configuration by loading .py files that contains the
        parameters.

        Parameters:
            :param work: .py file of the working configuration
            :type work: str; default 'work_default'
            :param work_module: Module containing the working configuration
            :type work_module: str; default 'config_default'
            :param instrument: .py file of the instrument configuration
            :type instrument: str; default 'instrument_default'
            :param instrument_module: Module containing the instrument
                configuration
            :type instrument_module: str; default 'config_default'
            :param observation_module: Module containing the observation
                configuration
            :type observation_module: str; default 'config_default'
            :param psf: .py file of the PSF configuration
            :type psf: str; default 'psf_default'
            :param psf_module: Module containing the PSF configuration
            :type psf_module: str; default 'config_default'
            :param find: .py file of the finding configuration
            :type find: str; default 'find_default'
            :param find_module: Module containing the finding configuration
            :type find_module: str; default 'config_default'
            :param fit: .py file of the fitting configuration
            :type fit: str; default 'fit_default'
            :param fit_module: Module containing the fitting configuration
            :type fit_module: str; default 'config_default'

        Attributes:
            :attr wd: Working directory, where files are saved and loaded by
                default
            :atype wd: str
            :attr sides: Detector sides (width, height) (px)
            :atype sides: list [2] (int)
            :attr gain: Detector gain (e-/DN)
            :atype gain: float
            :attr rn: Detector read noise STD (e-)
            :atype rn: float
            :attr satur: Saturation limit or value over which the measurements
                are not linear enough to be not reliable (DN)
            :atype satur: float; optional
            :attr coadd: Number of exposures co-added
            :atype coadd: int
            :attr coavg: Number of exposures co-averaged
            :atype coavg: int
            :attr m1c: Instrumental magnitude of a star with 1 DN of intensity
            :atype m1c: float
            :attr eval_method: Pixel evaluation method. Available options:
                'center', 'corners', 'grid' and 'integral'
            :atype eval_method: str
            :attr eval_grid: Pixel evaluation sampling density
            :atype eval_grid: int
            :attr find_radius: Fitting radius during the finding, in units of
                arithmetic mean FWHM
            :atype find_radius: float
            :attr find_bkg_in: Background inner radius during the finding (px).
                'None' uses whole image
            :atype find_bkg_in: float
            :attr find_bkg_out: Background outer radius during the finding (px).
                'None' uses whole image
            :atype find_bkg_out: float
            :attr find_npx: Minimum number of good pixels that an object must
                have for finding, or it is dropped
            :atype find_npx: int
            :attr find_std: Number of background STD above which a star is found
            :atype find_std: float
            :attr sharp: Sharpness limits for stars found (minimum,vmaximum)
            :atype sharp: list [2] (float)
            :attr min_fit: Minimum number of fitting iterations
            :atype min_fit: int
            :attr max_fit: Maximum number of fitting iterations
            :atype max_fit: int
            :attr iter_sub: Initial fitting iterations that do not use star
                subtraction. Use a value larger than 'max_fit' to disable star
                subtraction
            :atype iter_sub: int
            :attr iter_sigma: Initial fitting iterations that do not use star
                sigma-clipping. Use a value larger than 'max_fit' to disable
                star subtraction
            :atype iter_sigma: int
            :attr psf_radius: Radius over which the PSF is defined (px). Pixels'
                centers have to be within it
            :atype psf_radius: float
            :attr fit_radius: Radius over which the PSF is fitted (px). Pixels'
                centers have to be within it. It has to be smaller than or equal
                to 'psf_radius'
            :atype fit_radius: float
            :attr bkg_radius_in: Inner radius over which the background is
                calculated with a median when 'fit_nbkg' is 0
            :atype bkg_radius_in: float
            :attr sigma_clip: Number of sigmas for counts of residual to remove
                pixels from fitting
            :atype sigma_clip: float; optional
            :attr fit_npx: Minimum number of good pixels that an object must
                have for fitting, or it is dropped. The number of fitting
                parameters is the lower limit
            :atype fit_npx: int
            :attr fit_noise: Method used to calculate the noise in assigning
                weights during the fitting. Available options are 'data'
                (variance equals the image) and 'model' (variance equals the
                model image of stars and background plus read noise)
            :atype fit_noise: str
            :attr fit_nbkg: Number of parameters used to fit the background. 0:
                background not fitted. 1: flat background. 3: background with
                gradient
            :atype fit_nbkg: int
            :attr fit_gain: Initial fitting gain
            :atype fit_gain: float
            :attr fit_max: Maximum relative step size in counts of a fitting
                iteration
            :atype fit_max: float
            :attr iter_gain: Number of iterations after 'min_fit' before halving
                the fitting gain
            :atype iter_gain: int
            :attr chi2r_tol: Fraction of reduced chi^2 to converge to
            :atype chi2r_tol: float
            :attr near_tol: Distance between stars in FWHM to consider them too
                close
            :atype near_tol: float
            :attr faint_tol: Number of sigmas for counts when stars are
                considered too faint
            :atype faint_tol: float
        """

        # Check parameters
        if not isinstance(work, (str, type(None))):
            err_msg = "'work' has to be str"
            raise message.NephastError(err_msg)

        if not isinstance(work_module, (str, type(None))):
            err_msg = "'work_module' has to be str"
            raise message.NephastError(err_msg)

        work_module_spec = util.find_spec(work_module + '.' + work)

        if work_module_spec is None:
            err_msg = "module '{0}.{1}' does not exist".format(work_module,
                                                               work)
            raise message.NephastError(err_msg)

        if not isinstance(instrument, (str, type(None))):
            err_msg = "'instrument' has to be str"
            raise message.NephastError(err_msg)

        if not isinstance(instrument_module, (str, type(None))):
            err_msg = "'instrument_module' has to be str"
            raise message.NephastError(err_msg)

        instrument_module_spec = util.find_spec(instrument_module + '.' +
                                                instrument)

        if instrument_module_spec is None:
            err_msg = "module '{0}.{1}' does not exist".format(
                instrument_module, instrument)
            raise message.NephastError(err_msg)

        if not isinstance(observation, (str, type(None))):
            err_msg = "'observation' has to be str"
            raise message.NephastError(err_msg)

        if not isinstance(observation_module, (str, type(None))):
            err_msg = "'observation_module' has to be str"
            raise message.NephastError(err_msg)

        observation_module_spec = util.find_spec(observation_module + '.' +
                                                 observation)

        if observation_module_spec is None:
            err_msg = "module '{0}.{1}' does not exist".format(
                observation_module, observation)
            raise message.NephastError(err_msg)

        if not isinstance(psf, (str, type(None))):
            err_msg = "'psf' has to be str"
            raise message.NephastError(err_msg)

        if not isinstance(psf_module, (str, type(None))):
            err_msg = "'psf_module' has to be str"
            raise message.NephastError(err_msg)

        psf_module_spec = util.find_spec(psf_module + '.' + psf)

        if psf_module_spec is None:
            err_msg = "module '{0}.{1}' does not exist".format(psf_module, psf)
            raise message.NephastError(err_msg)

        if not isinstance(find, (str, type(None))):
            err_msg = "'find' has to be str"
            raise message.NephastError(err_msg)

        if not isinstance(find_module, (str, type(None))):
            err_msg = "'find_module' has to be str"
            raise message.NephastError(err_msg)

        find_module_spec = util.find_spec(find_module + '.' + find)

        if find_module_spec is None:
            err_msg = "module '{0}.{1}' does not exist".format(find_module,
                                                               find)
            raise message.NephastError(err_msg)

        if not isinstance(fit, (str, type(None))):
            err_msg = "'fit' has to be str"
            raise message.NephastError(err_msg)

        if not isinstance(fit_module, (str, type(None))):
            err_msg = "'fit_module' has to be str"
            raise message.NephastError(err_msg)

        fit_module_spec = util.find_spec(fit_module + '.' + fit)

        if fit_module_spec is None:
            err_msg = "module '{0}.{1}' does not exist".format(fit_module, fit)
            raise message.NephastError(err_msg)

        # Load default configuration
        work_config = getattr(__import__(work_module, fromlist=[work]), work)
        instrument_config = getattr(__import__(instrument_module,
                                               fromlist=[instrument]),
                                    instrument)
        observation_config = getattr(__import__(observation_module,
                                                fromlist=[observation]),
                                     observation)
        psf_config = getattr(__import__(psf_module, fromlist=[psf]), psf)
        find_config = getattr(__import__(find_module, fromlist=[find]), find)
        fit_config = getattr(__import__(fit_module, fromlist=[fit]), fit)

        # Check and load default parameters
        try:
            wd = work_config.wd
        except AttributeError:
            err_msg = "'wd' is not defined in the working configuration"
            raise message.NephastError(err_msg)

        try:
            sides = instrument_config.sides
        except AttributeError:
            err_msg = "'sides' is not defined in the instrument configuration"
            raise message.NephastError(err_msg)

        try:
            gain = instrument_config.gain
        except AttributeError:
            err_msg = "'gain' is not defined in the instrument configuration"
            raise message.NephastError(err_msg)

        try:
            rn = instrument_config.rn
        except AttributeError:
            err_msg = "'rn' is not defined in the instrument configuration"
            raise message.NephastError(err_msg)

        try:
            satur = instrument_config.satur
        except AttributeError:
            err_msg = "'satur' is not defined in the instrument configuration"
            raise message.NephastError(err_msg)

        try:
            coadd = observation_config.coadd
        except AttributeError:
            err_msg = "'coadd' is not defined in the observation configuration"
            raise message.NephastError(err_msg)

        try:
            coavg = observation_config.coavg
        except AttributeError:
            err_msg = "'coavg' is not defined in the observation configuration"
            raise message.NephastError(err_msg)

        try:
            m1c = psf_config.m1c
        except AttributeError:
            err_msg = "'m1c' is not defined in the PSF configuration"
            raise message.NephastError(err_msg)

        try:
            eval_method = psf_config.eval_method
        except AttributeError:
            err_msg = "'eval_method' is not defined in the PSF configuration"
            raise message.NephastError(err_msg)

        try:
            eval_grid = psf_config.eval_grid
        except AttributeError:
            err_msg = "'eval_grid' is not defined in the PSF configuration"
            raise message.NephastError(err_msg)

        try:
            find_radius = find_config.find_radius
        except AttributeError:
            err_msg = "'find_radius' is not defined in the fitting " + \
                "configuration"
            raise message.NephastError(err_msg)

        try:
            find_bkg_in = find_config.find_bkg_in
        except AttributeError:
            err_msg = "'find_bkg_in' is not defined in the finding " + \
                "configuration"
            raise message.NephastError(err_msg)

        try:
            find_bkg_out = find_config.find_bkg_out
        except AttributeError:
            err_msg = "'find_bkg_out' is not defined in the finding " + \
                "configuration"
            raise message.NephastError(err_msg)

        try:
            find_npx = find_config.find_npx
        except AttributeError:
            err_msg = "'find_npx' is not defined in the finding configuration"
            raise message.NephastError(err_msg)

        try:
            find_std = find_config.find_std
        except AttributeError:
            err_msg = "'find_std' is not defined in the finding configuration"
            raise message.NephastError(err_msg)

        try:
            sharp = find_config.sharp
        except AttributeError:
            err_msg = "'sharp' is not defined in the finding configuration"
            raise message.NephastError(err_msg)

        try:
            min_fit = fit_config.min_fit
        except AttributeError:
            err_msg = "'min_fit' is not defined in the fitting configuration"
            raise message.NephastError(err_msg)

        try:
            max_fit = fit_config.max_fit
        except AttributeError:
            err_msg = "'max_fit' is not defined in the fitting configuration"
            raise message.NephastError(err_msg)

        try:
            iter_sub = fit_config.iter_sub
        except AttributeError:
            err_msg = "'iter_sub' is not defined in the fitting configuration"
            raise message.NephastError(err_msg)

        try:
            iter_sigma = fit_config.iter_sigma
        except AttributeError:
            err_msg = "'iter_sigma' is not defined in the fitting configuration"
            raise message.NephastError(err_msg)

        try:
            psf_radius = psf_config.psf_radius
        except AttributeError:
            err_msg = "'psf_radius' is not defined in the PSF configuration"
            raise message.NephastError(err_msg)

        try:
            fit_radius = fit_config.fit_radius
        except AttributeError:
            err_msg = "'fit_radius' is not defined in the fitting configuration"
            raise message.NephastError(err_msg)

        try:
            bkg_radius_in = fit_config.bkg_radius_in
        except AttributeError:
            err_msg = \
                "'bkg_radius_in' is not defined in the fitting configuration"
            raise message.NephastError(err_msg)

        try:
            sigma_clip = fit_config.sigma_clip
        except AttributeError:
            err_msg = \
                "'sigma_clip' is not defined in the fitting configuration"
            raise message.NephastError(err_msg)

        try:
            fit_npx = fit_config.fit_npx
        except AttributeError:
            err_msg = "'fit_npx' is not defined in the fitting configuration"
            raise message.NephastError(err_msg)

        try:
            fit_noise = fit_config.fit_noise
        except AttributeError:
            err_msg = "'fit_noise' is not defined in the fitting configuration"
            raise message.NephastError(err_msg)

        try:
            fit_nbkg = fit_config.fit_nbkg
        except AttributeError:
            err_msg = "'fit_nbkg' is not defined in the fitting configuration"
            raise message.NephastError(err_msg)

        try:
            fit_gain = fit_config.fit_gain
        except AttributeError:
            err_msg = "'fit_gain' is not defined in the fititng configuration"
            raise message.NephastError(err_msg)

        try:
            fit_max = fit_config.fit_max
        except AttributeError:
            err_msg = "'fit_max' is not defined in the fititng configuration"
            raise message.NephastError(err_msg)

        try:
            iter_gain = fit_config.iter_gain
        except AttributeError:
            err_msg = "'iter_gain' is not defined in the fititng configuration"
            raise message.NephastError(err_msg)

        try:
            chi2r_tol = fit_config.chi2r_tol
        except AttributeError:
            err_msg = "'chi2r_tol' is not defined in the fitting configuration"
            raise message.NephastError(err_msg)

        try:
            near_tol = fit_config.near_tol
        except AttributeError:
            err_msg = "'near_tol' is not defined in the fitting configuration"
            raise message.NephastError(err_msg)

        try:
            faint_tol = fit_config.faint_tol
        except AttributeError:
            err_msg = "'faint_tol' is not defined in the fitting configuration"
            raise message.NephastError(err_msg)

        # Store attributes
        self.wd = wd
        self.sides = sides
        self.gain = gain
        self.rn = rn
        self.satur = satur
        self.coadd = coadd
        self.coavg = coavg
        self.m1c = m1c
        self.eval_method = eval_method
        self.eval_grid = eval_grid
        self.find_radius = find_radius
        self.find_bkg_in = find_bkg_in
        self.find_bkg_out = find_bkg_out
        self.find_npx = find_npx
        self.find_std = find_std
        self.sharp = sharp
        self.min_fit = min_fit
        self.max_fit = max_fit
        self.iter_sub = iter_sub
        self.iter_sigma = iter_sigma
        self.psf_radius = psf_radius
        self.fit_radius = fit_radius
        self.bkg_radius_in = bkg_radius_in
        self.sigma_clip = sigma_clip
        self.fit_npx = fit_npx
        self.fit_noise = fit_noise
        self.fit_nbkg = fit_nbkg
        self.fit_gain = fit_gain
        self.fit_max = fit_max
        self.iter_gain = iter_gain
        self.chi2r_tol = chi2r_tol
        self.near_tol = near_tol
        self.faint_tol = faint_tol

    def __setattr__(self, name, val):
        """Set attribute.

        Parameters:
            :param name: Attribute name
            :type name: str
            :param val: Attribute value
            :type val: any
        """

        # Check attribute
        if name is 'wd':
            if not isinstance(val, str):
                err_msg = "'{0}' has to be str".format(name)
                raise message.NephastError(err_msg)

            if not os.path.isdir(val):
                err_msg = "'{0}' has to be an existing directory".format(name)
                raise message.NephastError(err_msg)

        if name is 'sides':
            if not isinstance(val, list):
                err_msg = "'{0}' has to be list".format(name)
                raise message.NephastError(err_msg)

            if not (len(val) == 2):
                err_msg = "'{0}' has to have 2 elements".format(name)
                raise message.NephastError(err_msg)

            if not all(isinstance(item, (np.integer, int)) for item in val):
                err_msg = "'{0}' elements have to be int".format(name)
                raise message.NephastError(err_msg)

            if not all((item > 0) for item in val):
                err_msg = "'{0}' elements have to be positive".format(name)
                raise message.NephastError(err_msg)

        if name is 'gain':
            if not isinstance(val, (np.integer, int, float)):
                err_msg = "'{0}' has to be int or float".format(name)
                raise message.NephastError(err_msg)

            if not (val > 0):
                err_msg = "'{0}' has to be positive".format(name)
                raise message.NephastError(err_msg)

            val = np.float64(val)

        if name is 'rn':
            if not isinstance(val, (np.integer, int, float)):
                err_msg = "'{0}' has to be int or float".format(name)
                raise message.NephastError(err_msg)

            if not (val >= 0):
                err_msg = "'{0}' has to be non-negative".format(name)
                raise message.NephastError(err_msg)

            val = np.float64(val)

        if name is 'satur':
            if not isinstance(val, (np.integer, int, float, type(None))):
                err_msg = "'{0}' has to be int, float or None".format(name)
                raise message.NephastError(err_msg)

            if isinstance(val, (np.integer, int, float)) and not (val > 0):
                err_msg = "'{0}' has to be positive".format(name)
                raise message.NephastError(err_msg)

            val = np.float64(val)

        if name is 'coadd':
            if not isinstance(val, (np.integer, int)):
                err_msg = "'{0}' has to be int".format(name)
                raise message.NephastError(err_msg)

            if not (val > 0):
                err_msg = "'{0}' has to be positive".format(name)
                raise message.NephastError(err_msg)

        if name is 'coavg':
            if not isinstance(val, (np.integer, int)):
                err_msg = "'{0}' has to be int".format(name)
                raise message.NephastError(err_msg)

            if not (val > 0):
                err_msg = "'{0}' has to be positive".format(name)
                raise message.NephastError(err_msg)

        if name is 'm1c':
            if not isinstance(val, (np.integer, int, float)):
                err_msg = "'{0}' has to be int or float".format(name)
                raise message.NephastError(err_msg)

            val = np.float64(val)

        if name is 'eval_method':
            if not isinstance(val, str):
                err_msg = "'{0}' has to be str".format(name)
                raise message.NephastError(err_msg)

            if val not in ('center', 'corners', 'grid', 'integral'):
                err_msg = \
                    "'{0}' has to be 'center', 'corners', 'grid' or " + \
                    "'integral'".format(name)
                raise message.NephastError(err_msg)

        if name is 'eval_grid':
            if not isinstance(val, (np.integer, int)):
                err_msg = "'{0}' has to be int".format(name)
                raise message.NephastError(err_msg)

            if not (val > 0):
                err_msg = "'{0}' has to be positive".format(name)
                raise message.NephastError(err_msg)

        if name is 'find_radius':
            if not isinstance(val, (np.integer, int, float)):
                err_msg = "'{0}' has to be int or float".format(name)
                raise message.NephastError(err_msg)

            if not (val > 0):
                err_msg = "'{0}' has to be positive".format(name)
                raise message.NephastError(err_msg)

            val = np.float64(val)

        if name is 'find_bkg_in':
            if not isinstance(val, (np.integer, int, float)):
                err_msg = "'{0}' has to be int or float".format(name)
                raise message.NephastError(err_msg)

            if not (val > 0):
                err_msg = "'{0}' has to be positive".format(name)
                raise message.NephastError(err_msg)

            val = np.float64(val)

        if name is 'find_bkg_out':
            if not isinstance(val, (np.integer, int, float)):
                err_msg = "'{0}' has to be int or float".format(name)
                raise message.NephastError(err_msg)

            if not (val > (self.find_bkg_in + 1)):
                err_msg = ("'{0}' has to be greater than 'find_bkg_in' + " +
                           "1").format(name)
                raise message.NephastError(err_msg)

            val = np.float64(val)

        if name is 'find_npx':
            if not isinstance(val, (np.integer, int)):
                err_msg = "'{0}' has to be int".format(name)
                raise message.NephastError(err_msg)

            if not (val > 1):
                err_msg = "'{0}' has to be greater than 1".format(name)
                raise message.NephastError(err_msg)

        if name is 'find_std':
            if not isinstance(val, (np.integer, int, float)):
                err_msg = "'{0}' has to be int or float".format(name)
                raise message.NephastError(err_msg)

            if not (val > 0):
                err_msg = "'{0}' has to be positive".format(name)
                raise message.NephastError(err_msg)

            val = np.float64(val)

        if name is 'sharp':
            if not isinstance(val, list):
                err_msg = "'{0}' has to be list".format(name)
                raise message.NephastError(err_msg)

            if not (len(val) == 2):
                err_msg = "'{0}' has to have 2 elements".format(name)
                raise message.NephastError(err_msg)

            if not all(isinstance(item, (np.integer, int, float)) for item in
                       val):
                err_msg = "'{0}' elements have to be int or float".format(name)
                raise message.NephastError(err_msg)

            if not all((item >= 0) for item in val):
                err_msg = "'{0}' elements have to be non-negative".format(name)
                raise message.NephastError(err_msg)

            if not val[0] <= val[1]:
                err_msg = ("The first element of '{0}'has to be smaller than " +
                           "or equal to the second").format(name)
                raise message.NephastError(err_msg)

            val = np.float64(val)

        if name is 'min_fit':
            if not isinstance(val, (np.integer, int)):
                err_msg = "'{0}' has to be int".format(name)
                raise message.NephastError(err_msg)

            if not (val > 0):
                err_msg = "'{0}' has to be positive".format(name)
                raise message.NephastError(err_msg)

            try:
                if not (val <= self.max_fit):
                    err_msg = ("'{0}' has to be smaller than or equal to " +
                               "'max_fit'").format(name)
                    raise message.NephastError(err_msg)
            except AttributeError:
                pass

        if name is 'max_fit':
            if not isinstance(val, (np.integer, int)):
                err_msg = "'{0}' has to be int".format(name)
                raise message.NephastError(err_msg)

            if not (val > 0):
                err_msg = "'{0}' has to be positive".format(name)
                raise message.NephastError(err_msg)

            try:
                if not (val >= self.min_fit):
                    err_msg = ("'{0}' has to be greater than or equal to " +
                               "'min_fit'").format(name)
                    raise message.NephastError(err_msg)
            except AttributeError:
                pass

        if name is 'iter_sub':
            if not isinstance(val, (np.integer, int)):
                err_msg = "'{0}' has to be int".format(name)
                raise message.NephastError(err_msg)

            if not (val >= 0):
                err_msg = "'{0}' has to be non-negative".format(name)
                raise message.NephastError(err_msg)

        if name is 'iter_sigma':
            if not isinstance(val, (np.integer, int)):
                err_msg = "'{0}' has to be int".format(name)
                raise message.NephastError(err_msg)

            if not (val >= 0):
                err_msg = "'{0}' has to be non-negative".format(name)
                raise message.NephastError(err_msg)

        if name is 'psf_radius':
            if not isinstance(val, (np.integer, int, float)):
                err_msg = "'{0}' has to be int or float".format(name)
                raise message.NephastError(err_msg)

            if not (val > 0):
                err_msg = "'{0}' has to be positive".format(name)
                raise message.NephastError(err_msg)

            try:
                if not (val >= self.fit_radius):
                    err_msg = ("'{0}' has to be greater than or equal to " +
                               "'fit_radius'").format(name)
                    raise message.NephastError(err_msg)
            except AttributeError:
                pass

            val = np.float64(val)

        if name is 'fit_radius':
            if not isinstance(val, (np.integer, int, float)):
                err_msg = "'{0}' has to be int or float".format(name)
                raise message.NephastError(err_msg)

            if not (val > 0):
                err_msg = "'{0}' has to be positive".format(name)
                raise message.NephastError(err_msg)

            try:
                if not (val <= self.psf_radius):
                    err_msg = ("'{0}' has to be smaller than or equal to " +
                               "'psf_radius'").format(name)
                    raise message.NephastError(err_msg)
            except AttributeError:
                pass

            try:
                if not (val > self.bkg_radius_in):
                    err_msg = ("'{0}' has to be greater than " +
                               "'bkg_radius_in'").format(name)
                    raise message.NephastError(err_msg)
            except AttributeError:
                pass

            val = np.float64(val)

        if name is 'bkg_radius_in':
            if not isinstance(val, (np.integer, int, float)):
                err_msg = "'{0}' has to be int or float".format(name)
                raise message.NephastError(err_msg)

            if not (val >= 0):
                err_msg = "'{0}' has to be non-negative".format(name)
                raise message.NephastError(err_msg)

            try:
                if not (val < self.fit_radius):
                    err_msg = ("'{0}' has to be smaller than " +
                               "'fit_radius'").format(name)
                    raise message.NephastError(err_msg)
            except AttributeError:
                pass

            val = np.float64(val)

        if name is 'sigma_clip':
            if not isinstance(val, (np.integer, int, float, type(None))):
                err_msg = "'{0}' has to be int or float".format(name)
                raise message.NephastError(err_msg)

            if isinstance(val, (np.integer, int, float)) and not (val > 0):
                err_msg = "'{0}' has to be positive".format(name)
                raise message.NephastError(err_msg)

            val = np.float64(val)

        if name is 'fit_npx':
            if not isinstance(val, (np.integer, int)):
                err_msg = "'{0}' has to be int".format(name)
                raise message.NephastError(err_msg)

            if not (val > 0):
                err_msg = "'{0}' has to be positive".format(name)
                raise message.NephastError(err_msg)

        if name is 'fit_noise':
            if not isinstance(val, str):
                err_msg = "'{0}' has to be str".format(name)
                raise message.NephastError(err_msg)

            if val not in ('data', 'model'):
                err_msg = "'{0}' has to be 'data' or 'model'".format(name)
                raise message.NephastError(err_msg)

        if name is 'fit_nbkg':
            if not isinstance(val, (np.integer, int)):
                err_msg = "'{0}' has to be int".format(name)
                raise message.NephastError(err_msg)

            if not (val in (0, 1, 3)):
                err_msg = "'{0}' has to be 0, 1 or 3".format(name)
                raise message.NephastError(err_msg)

        if name is 'fit_gain':
            if not isinstance(val, (np.integer, int, float)):
                err_msg = "'{0}' has to be int or float".format(name)
                raise message.NephastError(err_msg)

            if not (val > 0):
                err_msg = "'{0}' has to be positive".format(name)
                raise message.NephastError(err_msg)

            val = np.float64(val)

        if name is 'fit_max':
            if not isinstance(val, (np.integer, int, float)):
                err_msg = "'{0}' has to be int or float".format(name)
                raise message.NephastError(err_msg)

            if not (val > 0):
                err_msg = "'{0}' has to be positive".format(name)
                raise message.NephastError(err_msg)

            val = np.float64(val)

        if name is 'iter_gain':
            if not isinstance(val, (np.integer, int)):
                err_msg = "'{0}' has to be int".format(name)
                raise message.NephastError(err_msg)

            if not (val > 0):
                err_msg = "'{0}' has to be positive".format(name)
                raise message.NephastError(err_msg)

        if name is 'chi2r_tol':
            if not isinstance(val, (np.integer, int, float)):
                err_msg = "'{0}' has to be int or float".format(name)
                raise message.NephastError(err_msg)

            if not (val > 0):
                err_msg = "'{0}' has to be positive".format(name)
                raise message.NephastError(err_msg)

            val = np.float64(val)

        if name is 'near_tol':
            if not isinstance(val, (np.integer, int, float)):
                err_msg = "'{0}' has to be int or float".format(name)
                raise message.NephastError(err_msg)

            if not (val > 0):
                err_msg = "'{0}' has to be positive".format(name)
                raise message.NephastError(err_msg)

            val = np.float64(val)

        if name is 'faint_tol':
            if not isinstance(val, (np.integer, int, float)):
                err_msg = "'{0}' has to be int or float".format(name)
                raise message.NephastError(err_msg)

            if not (val >= 0):
                err_msg = "'{0}' has to be positive".format(name)
                raise message.NephastError(err_msg)

            val = np.float64(val)

        # Set attribute
        super().__setattr__(name, val)

    def load(self, conf, conf_module):
        """Load new configuration parameters from a .py file.

        Parameters:
            :param conf: .py file of the configuration
            :type conf: str
            :param conf_module: Module containing the configuration
            :type conf_module: str
        """

        # Check parameters
        if not isinstance(conf, str):
            err_msg = "'conf' has to be str"
            raise message.NephastError(err_msg)

        if not isinstance(conf_module, str):
            err_msg = "'conf_module' has to be str"
            raise message.NephastError(err_msg)

        conf_module_spec = util.find_spec(conf_module + '.' + conf)

        if conf_module_spec is None:
            err_msg = "module '{0}.{1}' does not exist".format(conf_module,
                                                               conf)
            raise message.NephastError(err_msg)

        # Load configuration
        config = getattr(__import__(conf_module, fromlist=[conf]), conf)

        # Store attributes
        config_trim = {}

        for key in config.__dict__.keys():
            if not key.startswith('__'):
                config_trim[key] = config.__dict__[key]

        self.__dict__.update(config_trim)
