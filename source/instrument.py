"""NEPHAST by Paolo Turri <pturri83@gmail.com>
Released under BSD License 2.0

This module deals with the instrument class used to generate and analyze
images.
"""


import config
import message
import object


__all__ = ['Instrument']


class Instrument(object.Object):
    """Instrument object used to create and analyze image frames.
    """

    def __init__(self, conf=None):
        """Initialize an instrument that contains parameters used to create and
        analyze image frame.

        Parameters:
            :param conf: Configuration
            :type conf: config.Config; optional

        Attributes:
            :attr side_x: Detector width (px)
            :atype side_x: int
            :attr side_y: Detector height (px)
            :atype side_y: int
            :attr gain: Detector gain (e-/DN)
            :atype gain: float
            :attr rn: Detector read noise STD (e-)
            :atype rn: float
            :attr satur: Saturation limit (DN). Higher values are ignored in
                during profile fitting but are still used in generating images
            :atype satur: float, None
        """

        # Check arguments
        if not isinstance(conf, (config.Config, type(None))):
            err_msg = "'conf' has to be config.Config"
            raise message.NephastError(err_msg)

        # Define configuration
        if conf is None:
            conf = config.Config()

        # Store attributes
        self.side_x = conf.sides[0]
        self.side_y = conf.sides[1]
        self.gain = conf.gain
        self.rn = conf.rn
        self.satur = conf.satur
