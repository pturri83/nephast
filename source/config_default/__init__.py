"""
NEPHAST by Paolo Turri <pturri83@gmail.com>
Released under BSD License 2.0

Initialize NEPHAST default configuration module.
"""


# Default configuration files in NEPHAST
__all__ = ['find_default','fit_default', 'instrument_default',
           'observation_default', 'psf_default', 'work_default']
