"""NEPHAST by Paolo Turri <pturri83@gmail.com>
Released under BSD License 2.0

Collection of default configuration parameters for the instrument.
For a description of these parameters, refer to the attributes listed in the
'config.Config.__init__' docstring.
"""


sides = [1000, 1000]
gain = 1
rn = 0
satur = None
