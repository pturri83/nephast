"""NEPHAST by Paolo Turri <pturri83@gmail.com>
Released under BSD License 2.0

Collection of default configuration parameters for the PSF.
For a description of these parameters, refer to the attributes listed in the
'config.Config.__init__' docstring.
"""


m1c = 30
eval_method = 'grid'
eval_grid = 20
psf_radius = 10
