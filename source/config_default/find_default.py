"""NEPHAST by Paolo Turri <pturri83@gmail.com>
Released under BSD License 2.0

Collection of default configuration parameters for star finding.
For a description of these parameters, refer to the attributes listed in the
'config.Config.__init__' docstring.
"""


find_radius = 4
find_bkg_in = 2
find_bkg_out = 10
find_npx = 10
find_std = 4
sharp = [0.4, 1.2]
