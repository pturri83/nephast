"""NEPHAST by Paolo Turri <pturri83@gmail.com>
Released under BSD License 2.0

Collection of default configuration parameters for PSF fitting.
For a description of these parameters, refer to the attributes listed in the
'config.Config.__init__' docstring.
"""


min_fit = 10
max_fit = 100
iter_sub = 3
iter_sigma = 3
fit_radius = 5
bkg_radius_in = 2
sigma_clip = 3.5
fit_npx = 10
fit_noise = 'model'
fit_nbkg = 1
fit_gain = 0.7
fit_max = 0.5
iter_gain = 20
chi2r_tol = 1e-3
near_tol = 0.2
faint_tol = 3
