"""NEPHAST by Paolo Turri <pturri83@gmail.com>
Released under BSD License 2.0

Collection of default configuration parameters for working with NEPHAST.
For a description of these parameters, refer to the attributes listed in the
'config.Config.__init__' docstring.
"""


import os


wd = os.path.join(os.path.expanduser('~'), 'nephast')
