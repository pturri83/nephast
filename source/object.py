"""NEPHAST by Paolo Turri <pturri83@gmail.com>
Released under BSD License 2.0

This module deals with generic NEPHAST objects.
"""


import os
import pickle
import warnings

import config
import message


class Object:
    """Generic NEPHAST object used for all other objects.
    """

    def save_obj(self, file=None, folder=None, overwrite=False, keep=True,
                 conf=None, verbose=False):
        """Save object on disk as a file. If the destination folder doesn't
        exist, it is created. If the destination file already exists, overwrite
        is disbled and keeping both is enabled, the object is saved with
        "_copy[n]" appended to the name. If the destination file already exists,
        overwrite is disbled and keeping both is disabled, the program exits
        with a warning.
        The default file name is the object's class (in lower case).

        Parameters:
            :param file: Destination file name
            :type file: str; optional
            :param folder: Destination folder (absolute or relative path)
            :type folder: str; optional
            :param overwrite: Overwrite destination file
            :type overwrite: bool; default False
            :param keep: If destination file already exists, keep both.
            :type keep: bool; default True
            :param conf: Configuration
            :type conf: config.Config
            :param verbose: Show process messages
            :type verbose: bool; default False
        """

        # Check arguments
        if file is None:
            file = self.__class__.__name__.lower() + '.nph'

        if not isinstance(file, str):
            err_msg = "'file' has to be str"
            raise message.NephastError(err_msg)

        if not isinstance(folder, (str, type(None))):
            err_msg = "'folder' has to be str"
            raise message.NephastError(err_msg)

        if not isinstance(overwrite, bool):
            err_msg = "'overwrite' has to be bool"
            raise message.NephastError(err_msg)

        if not isinstance(keep, bool):
            err_msg = "'keep' has to be bool"
            raise message.NephastError(err_msg)

        if not isinstance(conf, (config.Config, type(None))):
            err_msg = "'conf' has to be config.Config"
            raise message.NephastError(err_msg)

        if not isinstance(verbose, bool):
            err_msg = "'verbose' has to be bool"
            raise message.NephastError(err_msg)

        # Define configuration
        if conf is None:
            conf = config.Config()

        # Check file existence
        if folder is None:
            folder = conf.wd

        if os.path.isfile(os.path.join(folder, file)) and not overwrite and \
                not keep:
            warn_msg = ("'{0}' already exists and 'overwrite' and 'keep' are " +
                        "False").format(os.path.abspath(os.path.join(folder,
                                                                     file)))
            warnings.warn(message.NephastWarning(warn_msg))
            return

        # Save file
        if not os.path.isdir(folder):
            os.mkdir(folder)

        file_copy = os.path.join(folder, file)

        if os.path.isfile(os.path.join(folder, file)) and not overwrite:
            saved = False
            file_n = 1

            while not saved:
                file_copy = os.path.splitext(os.path.join(folder, file))[0] + \
                            '_' + str(file_n) + \
                            os.path.splitext(os.path.join(folder, file))[1]

                if os.path.isfile(file_copy):
                    file_n += 1
                else:
                    saved = True

        file_obj = open(file_copy, 'wb')
        pickle.dump(self, file_obj)

        if verbose:
            print("File '{0}' saved".format(os.path.abspath(file_copy)))

    @staticmethod
    def load_obj(file, folder=None, conf=None, verbose=False):
        """Load object on disk as a file.

        Parameters:
            :param file: Source file name
            :type file: str
            :param folder: Source folder (absolute or relative path)
            :type folder: str; optional
            :param conf: Configuration
            :type conf: config.Config
            :param verbose: Show process messages
            :type verbose: bool; default False
        """

        # Check arguments
        if not isinstance(file, str):
            err_msg = "'file' has to be str"
            raise message.NephastError(err_msg)

        if not isinstance(folder, (str, type(None))):
            err_msg = "'folder' has to be str"
            raise message.NephastError(err_msg)

        if not isinstance(conf, (config.Config, type(None))):
            err_msg = "'conf' has to be config.Config"
            raise message.NephastError(err_msg)

        if not isinstance(verbose, bool):
            err_msg = "'verbose' has to be bool"
            raise message.NephastError(err_msg)

        # Check file existence
        if folder is None:
            folder = conf.wd

        if not os.path.isfile(os.path.join(folder, file)):
            err_msg = "'{0}' does not exist".format(
                os.path.abspath(os.path.join(folder, file)))
            raise message.NephastError(err_msg)

        # Load file
        file_obj = open(os.path.join(folder, file), 'rb')
        obj = pickle.load(file_obj)

        if verbose:
            print("File '{0}' loaded".format(
                os.path.abspath(os.path.join(folder, file))))

        return obj
