"""NEPHAST by Paolo Turri <pturri83@gmail.com>
Released under BSD License 2.0

This module deals with the generation and modification of images.
"""


from astropy import table
import numpy as np

import analyze
import catalog
import instrument
import message
import object
import observation
import psf


__all__ = ['Image']


class Image(object.Object):
    """Frame object.
    """

    def __init__(self, inst, obs, n_bad=0):
        """Initialize an image frame.

        Parameters:
            :param inst: Detector
            :type inst: instrument.Instrument
            :param obs: Observation
            :type obs: observation.Observation
            :param n_bad: Number of random bad pixels
            :type n_bad: int; default 0

        Attributes:
            :attr side_x: Image width
            :atype side_x: int
            :attr side_y: Image height
            :atype side_y: int
            :attr n_px: Total number of pixels
            :atype n_px: int
            :attr mesh_grid: x, y coordinates of the pixels in the PSF
            :atype mesh_grid: list [2] (numpy.ndarray (float))
            :attr gain: Image gain (considering co-averaged exposures) (e-/DN)
            :atype gain: float
            :attr rn: Image read noise (considering co-added and co-averaged
                exposures) (e-)
            :atype rn: float
            :attr satur: Saturation limit or value over which the measurements
                are not linear enough to be not reliable (DN)
            :atype satur: float
            :attr array: Image array (DN)
            :atype array: numpy.ndarray [side_y, side_x] (float)
            :attr min: Minimum value of the unmasked image (DN)
            :atype min: float
            :attr max: Maximum value of the unmasked image (DN)
            :atype max: float
            :attr min_mask: Minimum value of the masked image (DN)
            :atype min_mask: float
            :attr max_mask: Maximum value of the masked image (DN)
            :atype max_mask: float
        """

        # Check arguments
        if not isinstance(inst, instrument.Instrument):
            err_msg = "'inst' has to be instrument.Instrument"
            raise message.NephastError(err_msg)

        if not isinstance(obs, observation.Observation):
            err_msg = "'obs' has to be observation.Observation"
            raise message.NephastError(err_msg)

        if not isinstance(n_bad, (np.integer, int)):
            err_msg = "'n_bad' has to be int"
            raise message.NephastError(err_msg)

        if not n_bad >= 0:
            err_msg = "'n_bad' has to be non-negative"
            raise message.NephastError(err_msg)

        # Store attributes
        self.side_x = inst.side_x
        self.side_y = inst.side_y
        self.n_px = self.side_x * self.side_y
        mesh_grid = np.mgrid[: inst.side_y, : inst.side_x]
        self.mesh_grid = [mesh_grid[1], mesh_grid[0]]
        self.gain = obs.gain
        self.rn = obs.rn
        self.satur = inst.satur
        self.array = np.ma.masked_array(np.zeros([self.side_y, self.side_x]))
        self.array.mask = np.ma.make_mask_none(self.array.shape)
        self.min = 0
        self.max = 0
        self.min_mask = 0
        self.max_mask = 0

        if n_bad > 0:
            self.add_bad_px(n_bad)

    def __setattr__(self, name, val):
        """Set attribute.

        Parameters:
            :param name: Attribute name
            :type name: str
            :param val: Attribute value
            :type val: any
        """

        # Set attribute
        super().__setattr__(name, val)

        # Mask saturated values and find extreme values
        if name is 'array':
            self.mask_val(val_hi=self.satur)
            self.min = np.min(val.data)
            self.max = np.max(val.data)
            self.min_mask = np.ma.min(val)
            self.max_mask = np.ma.max(val)

    def __getitem__(self, coor):
        """Get unmasked pixel value of coordinates x and y.

        Parameters:
            :param coor: Pixel coordinates (x, y)
            :type coor: tuple [2] (slice, int, list, numpy.ndarray (int))

        Returns:
            :return val: Pixel value
            :rtype val: int, float, list, numpy.ndarray (int, float)
        """

        # Check arguments
        if not isinstance(coor, tuple):
            err_msg = "'coor' has to be tuple"
            raise message.NephastError(err_msg)

        if not (len(coor) == 2):
            err_msg = "'coor' has to have 2 elements"
            raise message.NephastError(err_msg)

        if not all([isinstance(i, (slice, list, np.ndarray, np.integer, int))
                    for i in coor]):
            err_msg = \
                "'coor' elements have to be slice, list, numpy.ndarray or int"
            raise message.NephastError(err_msg)

        coor_name = ['x', 'y']
        side = [self.side_x, self.side_y]

        for i_coor in [0, 1]:
            if isinstance(coor[i_coor], (np.integer, int)):
                if not ((coor[i_coor] >= 0) and (coor[0] < side[i_coor])):
                    err_msg = ("The {0:s} coordinate of 'coor' has to be " +
                               "non-negative and smaller than or equal to " +
                               "{1:d}").format(coor_name[i_coor],
                                               (side[i_coor] - 1))
                    raise message.NephastError(err_msg)

            if isinstance(coor[i_coor], slice):
                if coor[i_coor].start is not None:
                    if not ((coor[i_coor].start >= 0) and
                            (coor[i_coor].start <= side[i_coor])):
                        err_msg = ("The start of the {0:s} coordinate of " +
                                   "'coor' has to be non-negative and " +
                                   "smaller than or equal to " +
                                   "{1:d}").format(coor_name[i_coor],
                                                   side[i_coor])
                        raise message.NephastError(err_msg)

                    if coor[i_coor].stop is not None:
                        if not (coor[i_coor].start <= coor[i_coor].stop):
                            err_msg = ("The start of the {0:s} coordinate " +
                                       "of 'coor' has to be smaller than or " +
                                       "equal to the end of the {0:s} " +
                                       "coordinate of " +
                                       "'coor'").format(coor_name[i_coor])
                            raise message.NephastError(err_msg)

                        if coor[i_coor].stop is not None:
                            if not (coor[i_coor].stop <= side[i_coor]):
                                err_msg = ("The end of the {0:s} coordinate " +
                                           "of 'coor' has to be smaller than " +
                                           "or equal to the start of the " +
                                           "{1:s} coordinate of " +
                                           "'coor'").format(coor_name[i_coor])
                                raise message.NephastError(err_msg)

            if isinstance(coor[i_coor], (list, np.ndarray)):
                if not np.all([isinstance(i, (np.integer, int)) for i in
                               coor[i_coor]]):
                    err_msg = ("{0:s} coordinate of 'coor' elements have to " +
                               "be int").format(coor_name[i_coor])
                    raise message.NephastError(err_msg)

                if not (np.all([(i >= 0) for i in coor[i_coor]]) and
                        np.all([(i < side[i_coor]) for i in coor[i_coor]])):
                    err_msg = ("{0:s} coordinate of 'coor' elements have to " +
                               "be non-negative and smaller than or equal to " +
                               "{1:d}").format(coor_name[i_coor],
                                               (side[i_coor] - 1))
                    raise message.NephastError(err_msg)

        # Select value
        val = self.array[coor[1], coor[0]]

        return val

    def __setitem__(self, coor, val):
        """Set pixel value of coordinates x and y. The mask value is not
        changed.

        Parameters:
            :param coor: Pixel coordinates (x, y)
            :type coor: tuple [2] (slice, int, list, numpy.ndarray (int))
            :param val: Pixel value
            :type val: int, float, list, numpy.ndarray (int, float)
        """

        # Check arguments
        if not isinstance(coor, tuple):
            err_msg = "'coor' has to be tuple"
            raise message.NephastError(err_msg)

        if not (len(coor) == 2):
            err_msg = "'coor' has to have 2 elements"
            raise message.NephastError(err_msg)

        if not all([isinstance(i, (slice, list, np.ndarray, np.integer, int))
                    for i in coor]):
            err_msg = \
                "'coor' elements have to be slice, list, numpy.ndarray or int"
            raise message.NephastError(err_msg)

        coor_name = ['x', 'y']
        side = [self.side_x, self.side_y]

        for i_coor in [0, 1]:
            if isinstance(coor[i_coor], (np.integer, int)):
                if not ((coor[i_coor] >= 0) and (coor[0] < side[i_coor])):
                    err_msg = ("The {0:s} coordinate of 'coor' has to be " +
                               "non-negative and smaller than or equal to " +
                               "{1:d}").format(coor_name[i_coor],
                                               (side[i_coor] - 1))
                    raise message.NephastError(err_msg)

            if isinstance(coor[i_coor], slice):
                if coor[i_coor].start is not None:
                    if not ((coor[i_coor].start >= 0) and
                            (coor[i_coor].start <= side[i_coor])):
                        err_msg = ("The start of the {0:s} coordinate of " +
                                   "'coor' has to be non-negative and " +
                                   "smaller than or equal to " +
                                   "{1:d}").format(coor_name[i_coor],
                                                   side[i_coor])
                        raise message.NephastError(err_msg)

                    if coor[i_coor].stop is not None:
                        if not (coor[i_coor].start <= coor[i_coor].stop):
                            err_msg = ("The start of the {0:s} coordinate " +
                                       "of 'coor' has to be smaller than or " +
                                       "equal to the end of the {0:s} " +
                                       "coordinate of " +
                                       "'coor'").format(coor_name[i_coor])
                            raise message.NephastError(err_msg)

                        if coor[i_coor].stop is not None:
                            if not (coor[i_coor].stop <= side[i_coor]):
                                err_msg = ("The end of the {0:s} coordinate " +
                                           "of 'coor' has to be smaller than " +
                                           "or equal to the start of the " +
                                           "{1:s} coordinate of " +
                                           "'coor'").format(coor_name[i_coor])
                                raise message.NephastError(err_msg)

            if isinstance(coor[i_coor], (list, np.ndarray)):
                if not np.all([isinstance(i, (np.integer, int)) for i in
                               coor[i_coor]]):
                    err_msg = ("{0:s} coordinate of 'coor' elements have to " +
                               "be int").format(coor_name[i_coor])
                    raise message.NephastError(err_msg)

                if not (np.all([(i >= 0) for i in coor[i_coor]]) and
                        np.all([(i < side[i_coor]) for i in coor[i_coor]])):
                    err_msg = ("{0:s} coordinate of 'coor' elements have to " +
                               "be non-negative and smaller than or equal to " +
                               "{1:d}").format(coor_name[i_coor],
                                               (side[i_coor] - 1))
                    raise message.NephastError(err_msg)

        if not isinstance(val, (list, np.ndarray, np.integer, int, float)):
            err_msg = "'val' has to be list, numpy.ndarray, int or float"
            raise message.NephastError(err_msg)

        # Change value
        self.array.data[coor[1], coor[0]] = val

    @classmethod
    def from_cat(cls, inst, obs, cat, psfun, noise=False, bkg=True, n_bad=0):
        """Simulate an image from a catlog of stars and background.
        Background and noise can be simulated or not.

        Parameters:
            :param inst: Detector
            :type inst: instrument.Instrument
            :param obs: Observation
            :type obs: observation.Observation
            :param cat: Stars catalog
            :type cat: catalog.Catalog
            :param psfun: Point spread function
            :type psfun: psf.Psf
            :param noise: Add noise to the image
            :type noise: bool; default False
            :param bkg: Generate the background defined in 'obs'
            :type bkg: bool; default True
            :param n_bad: Number of random bad pixels
            :type n_bad: int; default 0

        Returns:
            :return img: Simulated image
            :rtype img: image.Image
        """

        # Check arguments
        if not isinstance(inst, instrument.Instrument):
            err_msg = "'inst' has to be instrument.Instrument"
            raise message.NephastError(err_msg)

        if not isinstance(obs, observation.Observation):
            err_msg = "'obs' has to be observation.Observation"
            raise message.NephastError(err_msg)

        if not isinstance(cat, catalog.Catalog):
            err_msg = "'cat' has to be catalog.Catalog"
            raise message.NephastError(err_msg)

        if not isinstance(psfun, psf.Psf):
            err_msg = "'psfun' has to be psf.Psf"
            raise message.NephastError(err_msg)

        if not isinstance(noise, bool):
            err_msg = "'noise' has to be bool"
            raise message.NephastError(err_msg)

        if not isinstance(bkg, bool):
            err_msg = "'bkg' has to be bool"
            raise message.NephastError(err_msg)

        if not isinstance(n_bad, (np.integer, int)):
            err_msg = "'n_bad' has to be int"
            raise message.NephastError(err_msg)

        if not n_bad >= 0:
            err_msg = "'n_bad' has to be non-negative"
            raise message.NephastError(err_msg)

        # Generate empty image
        img = cls(inst, obs, n_bad=n_bad)

        # Add stars
        img.add_stars(cat, psfun)

        # Add background
        if bkg:
            if obs.nbkg == 1:
                img.add_bkg(obs.bkgc)
            elif obs.nbkg == 3:
                img.add_bkg(obs.bkgc, obs.bkgx, obs.bkgy)

        # Add noise
        if noise:
            img.add_noise()
        else:
            img.rn = 0

        return img

    @classmethod
    def sub(cls, img1, img2, inst, obs):
        """Simulate an image from a subtraction of other two. The two images are
        considered to have been taken with the same detector and observation
        conditions.

        Parameters:
            :param img1: First image (minuend)
            :type img1: image.Image [m, n]
            :param img2: Second image (subtrahend)
            :type img2: image.Image [m, n]
            :param inst: Detector of one of the two images
            :type inst: instrument.Instrument
            :param obs: Observation of one of the two images
            :type obs: observation.Observation

        Returns:
            :return img: Subtracted image
            :rtype img: image.Image
        """

        # Check arguments
        if not isinstance(img1, Image):
            err_msg = "'img1' has to be image.Image"
            raise message.NephastError(err_msg)

        if not isinstance(img2, Image):
            err_msg = "'img2' has to be image.Image"
            raise message.NephastError(err_msg)

        if not ((img1.side_x == img2.side_x) and (img1.side_y == img2.side_y)):
            err_msg = "'img1' and 'img2' have to be of the same size"
            raise message.NephastError(err_msg)

        if not isinstance(inst, instrument.Instrument):
            err_msg = "'inst' has to be instrument.Instrument"
            raise message.NephastError(err_msg)

        if not isinstance(obs, observation.Observation):
            err_msg = "'obs' has to be observation.Observation"
            raise message.NephastError(err_msg)

        # Generate empty image
        img = cls(inst, obs)
        img.array = img1.array - img2.array
        img.rn = np.sqrt((img1.rn ** 2) + (img1.rn ** 2))

        return img

    def add_star(self, star, psfun):
        """Add a star to the frame.

        Parameters:
            :param star: Star added
            :type star: astropy.table.Row
            :param psfun: Point spread function
            :type psfun: psf.Psf
        """

        # Check arguments
        if not isinstance(star, table.Row):
            err_msg = "'star' has to be astropy.table.Row"
            raise message.NephastError(err_msg)

        if not isinstance(psfun, psf.Psf):
            err_msg = "'psfun' has to be psf.Psf"
            raise message.NephastError(err_msg)

        # Evaluate pixels in the profile using the star's magnitude
        prof_eval, prof_pos, _ = psfun.eval_prof(self.side_x, self.side_y,
                                                 star['x'], star['y'],
                                                 m=star['m'])

        # Add star to image
        array_tmp = self.array
        array_tmp[np.unravel_index(prof_pos, (self.side_y, self.side_x))] += \
            prof_eval
        self.array = array_tmp

    def add_stars(self, stars, psfun):
        """Add multiple stars from the frame.

        Parameters:
            :param stars: Stars added
            :type stars: catalog.Catalog
            :param psfun: Point spread function
            :type psfun: psf.Psf
        """

        # Check arguments
        if not isinstance(stars, catalog.Catalog):
            err_msg = "'stars' has to be catalog.Catalog"
            raise message.NephastError(err_msg)

        if not isinstance(psfun, psf.Psf):
            err_msg = "'psfun' has to be psf.Psf"
            raise message.NephastError(err_msg)

        # Add stars from image
        for idx in range(stars.n):

            if (stars['x', idx] >= -(psfun.psf_radius + 1)) and \
                    (stars['x', idx] <= (self.side_x + psfun.psf_radius + 1)) \
                    and (stars['y', idx] >= -(psfun.psf_radius + 1)) and \
                    (stars['y', idx] <= (self.side_y + psfun.psf_radius + 1)):
                self.add_star(stars[idx], psfun)

    def sub_star(self, star, psfun):
        """Subtract a star from the frame.

        Parameters:
            :param star: Star subtracted
            :type star: astropy.table.Row
            :param psfun: Point spread function
            :type psfun: psf.Psf
        """

        # Check arguments
        if not isinstance(star, table.Row):
            err_msg = "'star' has to be astropy.table.Row"
            raise message.NephastError(err_msg)

        if not isinstance(psfun, psf.Psf):
            err_msg = "'psfun' has to be psf.Psf"
            raise message.NephastError(err_msg)

        # Evaluate pixels in the profile using the star's magnitude
        prof_eval, prof_pos, _ = psfun.eval_prof(self.side_x, self.side_y,
                                                 star['x'], star['y'],
                                                 m=star['m'])

        # Subtract star from image
        array_tmp = self.array
        array_tmp[np.unravel_index(prof_pos, (self.side_y, self.side_x))] -= \
            prof_eval
        self.array = array_tmp

    def sub_stars(self, stars, psfun):
        """Subtract multiple stars from the frame.

        Parameters:
            :param stars: Stars subtracted
            :type stars: catalog.Catalog
            :param psfun: Point spread function
            :type psfun: psf.Psf
        """

        # Check arguments
        if not isinstance(stars, catalog.Catalog):
            err_msg = "'stars' has to be catalog.Catalog"
            raise message.NephastError(err_msg)

        if not isinstance(psfun, psf.Psf):
            err_msg = "'psfun' has to be psf.Psf"
            raise message.NephastError(err_msg)

        # Subtract stars from image
        for idx in range(stars.n):
            if (stars['x', idx] >= -(psfun.psf_radius + 1)) and \
                    (stars['x', idx] <= (self.side_x + psfun.psf_radius + 1)) \
                    and (stars['y', idx] >= -(psfun.psf_radius + 1)) and \
                    (stars['y', idx] <= (self.side_y + psfun.psf_radius + 1)):
                self.sub_star(stars[idx], psfun)

    def add_bkg(self, bkgc, *args):
        """Add a background (sky + instrument + dark) to the image. It can be
        flat or with a gradient across the field, measured from the (0, 0) pixel
        of the image.
        The backround is checked for values lower than 0.

        Parameters:
            :param bkgc: Background constant intensity (DN/px)
            :type bkgc: int, float

        Optional arguments:
            :ar bkgx: Background x gradient (DN/px/px)
            :artype bkgx: int, float
            :ar bkgy: Background y gradient (DN/px/px)
            :artype bkgy: int, float
        """

        # Check background values
        bkg_tmp = (args[0] * self.mesh_grid[0]) + \
                  (args[1] * self.mesh_grid[1]) + bkgc

        if (len(args) == 2) and not np.all(bkg_tmp >= 0):
            err_msg = "the background is lower than 0"
            raise message.NephastError(err_msg)

        # Add the background
        array_tmp = self.array
        array_tmp += bkgc

        if len(args) == 2:
            array_tmp += (args[0] * self.mesh_grid[0]) + \
                         (args[1] * self.mesh_grid[1])

        self.array = array_tmp

    @staticmethod
    def add_bkg_px(px, bkgc, *args):
        """Add a background (sky + instrument + dark) to individual pixels
        instead of an image. It can be flat or with a gradient across the field.

        Parameters:
            :param px: Pixel values (DN)
            :type px: list [n] (int, float)
            :param bkgc: Background constant intensity (DN/px)
            :type bkgc: int, float

        Optional arguments:
            :ar bkgx: Background x gradient (DN/px/px)
            :artype bkgx: float
            :ar bkgx0: x coordinate reference of the background gradient (px)
            :artype bkgx0: int
            :ar bkgy: Background y gradient (DN/px/px)
            :artype bkgy: float
            :ar bkgy0: y coordinate reference of the background gradient (px)
            :artype bkgy0: int
            :ar bkg_mesh_grid: x, y coordinates of the pixels, for using
                gradients (px)
            :artype bkg_mesh_grid: list [2] (numpy.ndarray (int [n]))

        Returns:
            :return px_new: Pixel values (DN)
            :rtype px_new: list [n] (float)
        """

        # Add the background
        px_tmp = px + bkgc

        if len(args) == 5:
            px_tmp += (args[0] * (args[4][0] - args[1])) + \
                      (args[2] * (args[4][1] - args[3]))

        px_new = np.float64(px_tmp)

        return px_new

    def add_noise(self):
        """Calculate and add shot noise and read noise to an image.
        Shot noise has Poissonian statistics and it includes photon and
        background noise. Background value includes sky, optics thermal and
        detector thermal backgrounds. Counts are first "transformed" to
        electrons by gain and rounding.
        Read noise has Gaussian statistics and it includes electronic and
        quantization noise. Gaussian noise can cause negative values on the
        image. If some pixels are negative, the image is offset to have a
        minimum value of 0.
        """

        # Add shot noise
        array_tmp = self.array
        mask_tmp = array_tmp.mask
        array_tmp = np.ma.array((np.random.poisson(array_tmp * self.gain) /
                                 self.gain), mask=mask_tmp)

        # Add read noise
        array_tmp += np.random.normal(scale=self.rn / self.gain,
                                      size=(self.side_y, self.side_x))

        if np.any(array_tmp < 0):
            array_tmp -= np.min(array_tmp)

        self.array = array_tmp

    @staticmethod
    def noise(px_val, gain, rn):
        """Calculate the noise (standard deviation) of pixels assuming only shot
        noise and read noise.

        Parameters:
            :param px_val: Pixel value(s) (DN)
            :type px_val: int, float, list, numpy.ndarray [n] (int, float)
            :param gain: Image gain (considering co-averaged exposures) (e-/DN)
            :type gain: int, float
            :param rn: Image read noise (considering co-added and co-averaged
                exposures) (e-)
            :type rn: int, float

        Returns:
            :return noise: Pixel standard deviation(s) (DN)
            :rtype noise: float, list [n] (float)
        """

        # Estimate shot noise and read noise variances (DN)
        var_sn = px_val / gain
        var_rn = (rn / gain) ** 2

        # Calculate noise
        noise = np.sqrt(var_sn + var_rn)

        return noise

    def add_bad_px(self, n, val=-1, masked=True):
        """Add to the image random bad pixels, that can also be masked.

        Parameters:
            :param n: Number of random bad pixels
            :type n: int
            :param val: Value of dead pixels
            :type val: int, float; default 0
            :param masked: Value of dead pixels
            :type masked: bool; default True
        """

        # Check arguments
        if not isinstance(n, (np.integer, int)):
            err_msg = "'n' has to be int"
            raise message.NephastError(err_msg)

        if not n > 0:
            err_msg = "'n' has to be positive"
            raise message.NephastError(err_msg)

        if not isinstance(val, (np.integer, int, float)):
            err_msg = "'val' has to be int or float"
            raise message.NephastError(err_msg)

        if not isinstance(masked, bool):
            err_msg = "'masked' has to be bool"
            raise message.NephastError(err_msg)

        # Chose bad pixels
        xy_bad = np.empty((0, 2))

        while len(xy_bad) < n:

            # Generate random pixels
            new_bad = n - len(xy_bad)
            x_bad = np.floor(np.random.uniform(low=0, high=self.side_x,
                                               size=new_bad)).astype(int)
            y_bad = np.floor(np.random.uniform(low=0, high=self.side_y,
                                               size=new_bad)).astype(int)
            xy_bad_tmp = [[x_bad[i], y_bad[i]] for i in range(new_bad)]
            xy_bad = np.concatenate((xy_bad, xy_bad_tmp))

            # Check that the random pixels are unique
            xy_bad = np.unique(xy_bad, axis=0)

        # Add bad pixel
        xy_bad = xy_bad.astype(int)
        array_tmp = self.array
        array_tmp[xy_bad[:, 1], xy_bad[:, 0]] = val
        self.array = array_tmp

        if masked:
            self.mask_pos(x=xy_bad[:, 0], y=xy_bad[:, 1])

    def mask_val(self, val=None, val_lo=None, val_hi=None):
        """Mask one or more pixels in an image if their value is equal to,
        larger or smaller than a target number.

        Parameters:
            :param val: Masked value
            :type val: int, float, list, numpy.ndarray (int, float); optional
            :param val_lo: Limit of low values to be masked (inclusive)
            :type val_lo: int, float; optional
            :param val_hi: Limit of high values to be masked (inclusive)
            :type val_hi: int, float; optional
        """

        # Check arguments
        if (val is None) and (val_lo is None) and (val_hi is None):
            err_msg = "at least one of 'val', 'val_lo' or 'val_hi' has not to" \
                      " be None"
            raise message.NephastError(err_msg)

        if not isinstance(val, (np.integer, int, float, list, np.ndarray,
                                type(None))):
            err_msg = "'val' has to be int, float, list or numpy.ndarray"
            raise message.NephastError(err_msg)

        if isinstance(val, (list, np.ndarray)):
            if not all(isinstance(i, (np.integer, int, float)) for i in val):
                err_msg = "'val' elements have to be int or float"
                raise message.NephastError(err_msg)

        if not isinstance(val_lo, (np.integer, int, float, type(None))):
            err_msg = "'val_lo' has to be int or float"
            raise message.NephastError(err_msg)

        if not isinstance(val_hi, (np.integer, int, float, type(None))):
            err_msg = "'val_hi' has to be int or float"
            raise message.NephastError(err_msg)

        # Mask pixels
        if isinstance(val, (np.integer, int, float)):
            val = [val]

        if val is not None:
            for i_val in val:
                np.ma.masked_equal(self.array, i_val, copy=False)

        if val_lo is not None:
            np.ma.masked_less_equal(self.array, val_lo, copy=False)

        if val_hi is not None:
            np.ma.masked_greater_equal(self.array, val_hi, copy=False)

    def mask_pos(self, x, y):
        """Mask one or more pixels in an image based on their position.

        Parameters:
            :param x: Masked x coordinates
            :type x: int, list, numpy.ndarray [n] (int)
            :param y: Masked y coordinates
            :type y: int, list, numpy.ndarray [n] (int)
        """

        # Check arguments
        if not isinstance(x, (np.integer, int, list, np.ndarray)):
            err_msg = "'x' has to be int, list or numpy.ndarray"
            raise message.NephastError(err_msg)

        if not type(x) == type(y):
            err_msg = "'y' has to be of the same type of 'x'"
            raise message.NephastError(err_msg)

        if isinstance(x, (list, np.ndarray)):
            if not all(isinstance(i, (np.integer, int)) for i in x):
                err_msg = "'x' elements have to be int"
                raise message.NephastError(err_msg)

            if not len(x) == len(y):
                err_msg = "'x' and 'y' have to be of the same size"
                raise message.NephastError(err_msg)

        if isinstance(y, (list, np.ndarray)):
            if not all(isinstance(i, (np.integer, int)) for i in x):
                err_msg = "'x' elements have to be int"
                raise message.NephastError(err_msg)

        # Mask pixels
        array_tmp = self.array
        array_tmp[y, x] = np.ma.masked
        self.array = array_tmp

    def show(self, cat=None, vmin=None, vmax=None, scale='lin', cmap='jet',
             x_offset=0, y_offset=0, unmask=False, title=None, cat_mark_col='k',
             cat_text_col='k'):
        """Display an array or image object as an image with some statistics. A
        catalog can be overlaid.

        Parameters:
            :param cat: Catalog to display with markersand stars' names on top
                of the image
            :type cat: catalog.Catalog; optional
            :param vmin: Minimum value colored
            :type vmin: int, float; optional
            :param vmax: Maximum value colored
            :type vmax: int, float; optional
            :param scale: Color scale, linear ('lin') or logarithmic ('log')
            :type scale: str; default 'lin'
            :param cmap: Colormap name
            :type cmap: str; default 'jet'
            :param x_offset: Offset of the x axis tick labels
            :type x_offset: int; default 0
            :param y_offset: Offset of the y axis tick labels
            :type y_offset: int; default 0
            :param unmask: Unmask image, if 'arr' is a numpy.ma.MaskedArray
                instance
            :type unmask: bool; default False
            :param title: Title of the figure
            :type title: str; optional
            :param cat_mark_col: matplotlib color of the catalog marker
            :type cat_mark_col: str; default 'k'
            :param cat_text_col: matplotlib color of the catalog text
            :type cat_text_col: str; default 'k'
        """

        # Show image
        analyze.show_img(self, cat=cat, vmin=vmin, vmax=vmax, scale=scale,
                         cmap=cmap, x_offset=x_offset, y_offset=y_offset,
                         unmask=unmask, title=title, cat_mark_col=cat_mark_col,
                         cat_text_col=cat_text_col)
