"""
NEPHAST by Paolo Turri <pturri83@gmail.com>
Released under BSD License 2.0

Initialize NEPHAST package.
"""


import pkg_resources


# Modules in NEPHAST
__all__ = ['analyze', 'catalog', 'config', 'find', 'fit', 'image', 'instrument',
           'message', 'object', 'observation', 'psf']

# Import setup
__name__ = pkg_resources.get_distribution('nephast').name
__version__ = pkg_resources.get_distribution('nephast').version
__author__ = pkg_resources.get_distribution('nephast').author
