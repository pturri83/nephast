"""NEPHAST by Paolo Turri <pturri83@gmail.com>
Released under BSD License 2.0

This module deals with the catalog class where information of stars in a
field are stored.
"""


import copy
import math

from astropy import table
import numpy as np
from scipy import spatial

import analyze
import config
import message
import object
import psf


__all__ = ['Catalog']


class Catalog(object.Object):
    """Catalog of stars.
    """

    def __init__(self, x, y, sigma_x=None, sigma_y=None, name=None, m=None,
                 sigma_m=None, counts=None, sigma_counts=None, peak=None,
                 sigma_peak=None, bkg=None, sigma_bkg=None, bkgc=None,
                 sigma_bkgc=None, bkgx=None, bkgx0=None, sigma_bkgx=None,
                 bkgy=None, bkgy0=None, sigma_bkgy=None, snr=None, chi2r=None,
                 psfun=None, original='m', conf=None):
        """Initialize a catalog of stars.
        Intensity has to be defined by instrumental magnitudes, counts or, if a
        PSF is provided, by the peak value. Magnitudes and counts are always
        present in the catalog.
        If a background value is not provided, it is set as NaN.
        If a PSF is not provided, the peak value is set as NaN.
        If a name is not provided, the scheme used is 'obj_' followed by a
        sequence of consecutive positive integers starting with 1.
        The background of a star can be flat or with a gradient across the
        field, measured from the (`bkgx0`,`bkgy0`) pixel coordinate.

        Parameters:
            :param x: x coordinate of the stars
            :type x: list, numpy.ndarray [n] (int, float)
            :param y: y coordinate of the stars
            :type y: list, numpy.ndarray [n] (int, float)
            :param sigma_x: x coordinate STD of the stars
            :type sigma_x: list, numpy.ndarray [n] (int, float); optional
            :param sigma_y: y coordinate STD of the stars
            :type sigma_y: list, numpy.ndarray [n] (int, float); optional
            :param name: Names of the stars
            :type name: list, numpy.ndarray [n] (str); optional
            :param m: Instrumental magnitudes of the stars
            :type m: list, numpy.ndarray [n] (int, float); optional
            :param sigma_m: Instrumental magnitudes STD of the stars
            :type sigma_m: list, numpy.ndarray [n] (int, float); optional
            :param counts: Counts of the stars (DN)
            :type counts: list, numpy.ndarray [n] (int, float); optional
            :param sigma_counts: Counts STD of the stars (DN)
            :type sigma_counts: list, numpy.ndarray [n] (int, float); optional
            :param peak: Peak values of the stars
            :type peak: list, numpy.ndarray [n] (int, float); optional
            :param sigma_peak: Peak values STD of the stars
            :type sigma_peak: list, numpy.ndarray [n] (int, float); optional
            :param bkg: Background median intensities of the stars (DN/px)
            :type bkg: list, numpy.ndarray [n] (int, float); optional
            :param sigma_bkg: Background median intensities STD of the stars
                (DN/px)
            :type sigma_bkg: list, numpy.ndarray [n] (int, float); optional
            :param bkgc: Background constant intensities of the stars (DN/px)
            :type bkgc: list, numpy.ndarray [n] (int, float); optional
            :param sigma_bkgc: Background constant intensities STD of the stars
                (DN/px)
            :type sigma_bkgc: list, numpy.ndarray [n] (int, float); optional
            :param bkgx: Background x gradients of the stars (DN/px/px)
            :type bkgx: list, numpy.ndarray [n] (int, float); optional
            :param bkgx0: x coordinate reference of the background gradients
                (px)
            :type bkgx0: list, numpy.ndarray [n] (int); optional
            :param sigma_bkgx: Background x gradients STD of the stars
                (DN/px/px)
            :type sigma_bkgx: list, numpy.ndarray [n] (int, float); optional
            :param bkgy: Background y gradients of the stars (DN/px/px)
            :type bkgy: list, numpy.ndarray [n] (int, float); optional
            :param bkgy0: y coordinate reference of the background gradients
                (px)
            :type bkgy0: list, numpy.ndarray [n] (int); optional
            :param sigma_bkgy: Background y gradients STD of the stars
                (DN/px/px)
            :type sigma_bkgy: list, numpy.ndarray [n] (int, float); optional
            :param snr: SNR
            :type snr: list, numpy.ndarray [n] (int, float); optional
            :param chi2r: Reduced chi^2
            :type chi2r: list, numpy.ndarray [n] (int, float); optional
            :param psfun: PSF of the stars
            :type psfun: psf.Psf; optional
            :param original: Original intensity quantity. 'm': magnitude.
                'counts': counts. 'peak': peak values
            :type original: str; default 'm'
            :param conf: Configuration
            :type conf: config.Config; optional

        Attributes:
            :attr fit_ready: Catalog is ready for fitting
            :atype fit_ready: bool
            :attr snr_read: Catalog is ready for calculating SNR
            :atype snr_ready: bool
            :attr psf: PSF of the stars
            :atype psf: psf.Psf, None
            :attr n: Number of stars
            :atype n: int
            :attr colnames: Column names
            :atype colnames: list
            :attr n_col: Number of columns
            :atype n_col: int
            :attr nbkg: Number of parameters used to describe the background. 1:
                flat background. 3: background with gradient
            :atype nbkg: int
            :attr tab: Data table containing coordinates and instrumental
                magnitude
            :atype tab: astropy.table.Table [n]
                :col name: Names of the stars
                :ctype name: str
                :col x: x coordinate of the stars
                :ctype x: float
                :col sigma_x: x coordinate STD of the stars
                :ctype sigma_x: float
                :col y: y coordinate of the stars
                :ctype y: float
                :col sigma_y: y coordinate STD of the stars
                :ctype sigma_y: float
                :col m: Instrumental magnitude of the stars
                :ctype m: float
                :col sigma_m: Instrumental magnitude STD of the stars
                :ctype sigma_m: float
                :col counts: Counts of the stars (DN)
                :ctype counts: float
                :col sigma_counts: Counts STD of the stars (DN)
                :ctype sigma_counts: float
                :col peak: Peak values of the stars
                :ctype peak: float
                :col sigma_peak: Peak values STD of the stars
                :ctype sigma_peak: float
                :col bkg: Background median intensities of the stars (DN/px)
                :ctype bkg: float
                :col sigma_bkg: Background median intensities STD of the
                    stars (DN/px)
                :ctype sigma_bkg: float
                :col bkgc: Background constant intensities of the stars (DN/px)
                :ctype bkgc: float
                :col sigma_bkgc: Background constant intensities STD of the
                    stars (DN/px)
                :ctype sigma_bkgc: float
                :col bkgx: Background x gradients of the stars (DN/px/px)
                :ctype bkgx: float
                :col bkgx0: x coordinate reference of the background gradients
                    (px)
                :ctype bkgx0: int
                :col sigma_bkgx: Background x gradients STD of the stars
                    (DN/px/px)
                :ctype sigma_bkgx: float
                :col bkgy: Background y gradients of the stars (DN/px/px)
                :ctype bkgy: float
                :col bkgy0: y coordinate reference of the background gradients
                    (px)
                :ctype bkgy0: int
                :col sigma_bkgy: Background y gradients STD of the stars
                    (DN/px/px)
                :ctype sigma_bkgy: float
                :col snr: SNR
                :ctype snr: float
                :col chi2r: Reduced chi^2
                :ctype chi2r: float
            :attr m1c: Instrumental magnitude of a star with 1 DN of intensity
            :atype m1c: float
        """

        # Check arguments
        if not isinstance(x, (list, np.ndarray)):
            err_msg = "'x' has to be list or numpy.ndarray"
            raise message.NephastError(err_msg)

        if not all(isinstance(i, (np.ma.MaskedArray, np.integer, int, float))
                   for i in x):
            err_msg = "'x' elements have to be int or float"
            raise message.NephastError(err_msg)

        if not isinstance(y, (list, np.ndarray)):
            err_msg = "'y' has to be list or numpy.ndarray"
            raise message.NephastError(err_msg)

        length = len(x)

        if not (len(y) == length):
            err_msg = "'y' has to have the same length as 'x'"
            raise message.NephastError(err_msg)

        if not all(isinstance(i, (np.ma.MaskedArray, np.integer, int, float))
                   for i in y):
            err_msg = "'y' elements have to be int or float"
            raise message.NephastError(err_msg)

        if sigma_x is not None:
            if not isinstance(sigma_x, (list, np.ndarray)):
                err_msg = "'sigma_x' has to be list, numpy.ndarray or None"
                raise message.NephastError(err_msg)

            if not (len(sigma_x) == length):
                err_msg = "'sigma_x' has to have the same length as 'x'"
                raise message.NephastError(err_msg)

            if not all(isinstance(i, (np.ma.MaskedArray, np.integer, int,
                                      float)) for i in sigma_x):
                err_msg = "'sigma_x' elements have to be int or float"
                raise message.NephastError(err_msg)

            if any((i <= 0) for i in sigma_x):
                err_msg = "'sigma_x' elements have to be positive"
                raise message.NephastError(err_msg)

        if sigma_y is not None:
            if not isinstance(sigma_y, (list, np.ndarray)):
                err_msg = "'sigma_y' has to be list, numpy.ndarray or None"
                raise message.NephastError(err_msg)

            if not (len(sigma_y) == length):
                err_msg = "'sigma_y' has to have the same length as 'x'"
                raise message.NephastError(err_msg)

            if not all(isinstance(i, (np.ma.MaskedArray, np.integer, int,
                                      float)) for i in sigma_y):
                err_msg = "'sigma_y' elements have to be int or float"
                raise message.NephastError(err_msg)

            if any((i <= 0) for i in sigma_y):
                err_msg = "'sigma_y' elements have to be positive"
                raise message.NephastError(err_msg)

        if name is not None:
            if not isinstance(name, (list, np.ndarray)):
                err_msg = "'name' has to be list, numpy.ndarray or None"
                raise message.NephastError(err_msg)

            if not (len(name) == length):
                err_msg = "'name' has to have the same length as 'x'"
                raise message.NephastError(err_msg)

            if not all(isinstance(i, str) for i in name):
                err_msg = "'name' elements have to be str"
                raise message.NephastError(err_msg)

        if all(i is None for i in (m, counts, peak)):
            err_msg = "one of 'm', 'counts' and 'peak' has not to be None"
            raise message.NephastError(err_msg)
        elif sum(i is not None for i in (m, counts, peak)) > 1:
            err_msg = "only one of 'm', 'counts' and 'peak' has not to be None"
            raise message.NephastError(err_msg)

        if sum(i is not None for i in (sigma_m, sigma_counts, sigma_peak)) > 1:
            err_msg = "only one of 'sigma_m', 'sigma_counts' and " \
                      "'sigma_peak' has not to be None"
            raise message.NephastError(err_msg)

        if m is not None:
            if not isinstance(m, (list, np.ndarray)):
                err_msg = "'m' has to be list, numpy.ndarray or None"
                raise message.NephastError(err_msg)

            if not (len(m) == length):
                err_msg = "'m' has to have the same length as 'x'"
                raise message.NephastError(err_msg)

            if not all(isinstance(i, (np.ma.MaskedArray, np.integer, int,
                                      float)) for i in m):
                err_msg = "'m' elements have to be int or float"
                raise message.NephastError(err_msg)

        if sigma_m is not None:
            if m is None:
                err_msg = "'m' has to be defined for 'sigma_m'"
                raise message.NephastError(err_msg)

            if not isinstance(sigma_m, (list, np.ndarray)):
                err_msg = "'sigma_m' has to be list, numpy.ndarray or None"
                raise message.NephastError(err_msg)

            if not (len(sigma_m) == length):
                err_msg = "'sigma_m' has to have the same length as 'x'"
                raise message.NephastError(err_msg)

            if not all(isinstance(i, (np.ma.MaskedArray, np.integer, int,
                                      float)) for i in sigma_m):
                err_msg = "'sigma_m' elements have to be int or float"
                raise message.NephastError(err_msg)

            if any((i <= 0) for i in sigma_m):
                err_msg = "'sigma_m' elements have to be positive"
                raise message.NephastError(err_msg)

        if counts is not None:
            if not isinstance(counts, (list, np.ndarray)):
                err_msg = "'counts' has to be list, numpy.ndarray or None"
                raise message.NephastError(err_msg)

            if not (len(counts) == length):
                err_msg = "'counts' has to have the same length as 'x'"
                raise message.NephastError(err_msg)

            if not all(isinstance(i, (np.ma.MaskedArray, np.integer, int,
                                      float)) for i in counts):
                err_msg = "'counts' elements have to be int or float"
                raise message.NephastError(err_msg)

            if any((i <= 0) for i in counts):
                err_msg = "'counts' elements have to be positive"
                raise message.NephastError(err_msg)

        if sigma_counts is not None:
            if counts is None:
                err_msg = "'counts' has to be defined for 'sigma_counts'"
                raise message.NephastError(err_msg)

            if not isinstance(sigma_counts, (list, np.ndarray)):
                err_msg = "'sigma_counts' has to be list, numpy.ndarray or None"
                raise message.NephastError(err_msg)

            if not (len(sigma_counts) == length):
                err_msg = "'sigma_counts' has to have the same length as 'x'"
                raise message.NephastError(err_msg)

            if not all(isinstance(i, (np.ma.MaskedArray, np.integer, int,
                                      float)) for i in sigma_counts):
                err_msg = "'sigma_counts' elements have to be int or float"
                raise message.NephastError(err_msg)

            if any((i <= 0) for i in sigma_counts):
                err_msg = "'sigma_counts' elements have to be positive"
                raise message.NephastError(err_msg)

        if peak is not None:
            if not isinstance(peak, (list, np.ndarray)):
                err_msg = "'peak' has to be list, numpy.ndarray or None"
                raise message.NephastError(err_msg)

            if not (len(peak) == length):
                err_msg = "'peak' has to have the same length as 'x'"
                raise message.NephastError(err_msg)

            if not all(isinstance(i, (np.ma.MaskedArray, np.integer, int,
                                      float)) for i in peak):
                err_msg = "'peak' elements have to be int or float"
                raise message.NephastError(err_msg)

            if not isinstance(psfun, psf.Psf):
                err_msg = "'psfun' has to be defined for 'peak'"
                raise message.NephastError(err_msg)

            if any((i <= 0) for i in peak):
                err_msg = "'peak' elements have to be positive"
                raise message.NephastError(err_msg)

        if sigma_peak is not None:
            if peak is None:
                err_msg = "'peak' has to be defined for 'sigma_peak'"
                raise message.NephastError(err_msg)

            if not isinstance(sigma_peak, (list, np.ndarray)):
                err_msg = "'sigma_peak' has to be list, numpy.ndarray or None"
                raise message.NephastError(err_msg)

            if not (len(sigma_peak) == length):
                err_msg = "'sigma_peak' has to have the same length as 'x'"
                raise message.NephastError(err_msg)

            if not all(isinstance(i, (np.ma.MaskedArray, np.integer, int,
                                      float)) for i in sigma_peak):
                err_msg = "'sigma_peak' elements have to be int or float"
                raise message.NephastError(err_msg)

            if not isinstance(psfun, psf.Psf):
                err_msg = "'psfun' has to be defined for 'sigma_peak'"
                raise message.NephastError(err_msg)

            if any((i <= 0) for i in sigma_peak):
                err_msg = "'sigma_peak' elements have to be positive"
                raise message.NephastError(err_msg)

        if bkg is not None:
            if not isinstance(bkg, (list, np.ndarray)):
                err_msg = "'bkg' has to be list, numpy.ndarray or None"
                raise message.NephastError(err_msg)

            if not (len(bkg) == length):
                err_msg = "'bkg' has to have the same length as 'x'"
                raise message.NephastError(err_msg)

            if not all(isinstance(i, (np.ma.MaskedArray, np.integer, int,
                                      float)) for i in bkg):
                err_msg = "'bkg' elements have to be int or float"
                raise message.NephastError(err_msg)

            if any((i < 0) for i in bkg):
                err_msg = "'bkg' elements have to be non-negative"
                raise message.NephastError(err_msg)

        if sigma_bkg is not None:
            if bkg is None:
                err_msg = "'bkg' has to be defined for 'sigma_bkg'"
                raise message.NephastError(err_msg)

            if not isinstance(sigma_bkg, (list, np.ndarray)):
                err_msg = "'sigma_bkg' has to be list, numpy.ndarray or None"
                raise message.NephastError(err_msg)

            if not (len(sigma_bkg) == length):
                err_msg = "'sigma_bkg' has to have the same length as 'x'"
                raise message.NephastError(err_msg)

            if not all(isinstance(i, (np.ma.MaskedArray, np.integer, int,
                                      float)) for i in sigma_bkg):
                err_msg = "'sigma_bkg' elements have to be int or float"
                raise message.NephastError(err_msg)

            if any((i <= 0) for i in sigma_bkg):
                err_msg = "'sigma_bkg' elements have to be positive"
                raise message.NephastError(err_msg)

        if bkgc is not None:
            if not isinstance(bkgc, (list, np.ndarray)):
                err_msg = "'bkgc' has to be list, numpy.ndarray or None"
                raise message.NephastError(err_msg)

            if not (len(bkgc) == length):
                err_msg = "'bkgc' has to have the same length as 'x'"
                raise message.NephastError(err_msg)

            if not all(isinstance(i, (np.ma.MaskedArray, np.integer, int,
                                      float)) for i in bkgc):
                err_msg = "'bkgc' elements have to be int or float"
                raise message.NephastError(err_msg)

        if sigma_bkgc is not None:
            if bkgc is None:
                err_msg = "'bkgc' has to be defined for 'sigma_bkgc'"
                raise message.NephastError(err_msg)

            if not isinstance(sigma_bkgc, (list, np.ndarray)):
                err_msg = "'sigma_bkgc' has to be list, numpy.ndarray or None"
                raise message.NephastError(err_msg)

            if not (len(sigma_bkgc) == length):
                err_msg = "'sigma_bkgc' has to have the same length as 'x'"
                raise message.NephastError(err_msg)

            if not all(isinstance(i, (np.ma.MaskedArray, np.integer, int,
                                      float)) for i in sigma_bkgc):
                err_msg = "'sigma_bkgc' elements have to be int or float"
                raise message.NephastError(err_msg)

            if any((i <= 0) for i in sigma_bkgc):
                err_msg = "'sigma_bkgc' elements have to be positive"
                raise message.NephastError(err_msg)

        if bkgx is not None:
            if bkgc is None:
                err_msg = "'bkgx' requires to declare 'bkgc'"
                raise message.NephastError(err_msg)

            if bkgy is None:
                err_msg = "Both 'bkgx' and 'bkgy' have to be declared"
                raise message.NephastError(err_msg)

            if not isinstance(bkgx, (list, np.ndarray)):
                err_msg = "'bkgx' has to be list, numpy.ndarray or None"
                raise message.NephastError(err_msg)

            if not (len(bkgx) == length):
                err_msg = "'bkgx' has to have the same length as 'x'"
                raise message.NephastError(err_msg)

            if not all(isinstance(i, (np.ma.MaskedArray, np.integer, int,
                                      float)) for i in bkgx):
                err_msg = "'bkgx' elements have to be int or float"
                raise message.NephastError(err_msg)

        if bkgx0 is not None:
            if bkgx is None:
                err_msg = "'bkgx0' requires to declare 'bkgx'"
                raise message.NephastError(err_msg)

            if not isinstance(bkgx0, (list, np.ndarray)):
                err_msg = "'bkgx0' has to be list, numpy.ndarray or None"
                raise message.NephastError(err_msg)

            if not (len(bkgx0) == length):
                err_msg = "'bkgx0' has to have the same length as 'x'"
                raise message.NephastError(err_msg)

            if not all(isinstance(i, (np.ma.MaskedArray, np.integer, int)) for i
                       in bkgx0):
                err_msg = "'bkgx0' elements have to be int"
                raise message.NephastError(err_msg)

        if sigma_bkgx is not None:
            if bkgx is None:
                err_msg = "'bkgx' has to be defined for 'sigma_bkgx'"
                raise message.NephastError(err_msg)

            if sigma_bkgy is None:
                err_msg = \
                    "Both 'sigma_bkgx' and 'sigma_bkgy' have to be declared"
                raise message.NephastError(err_msg)

            if not isinstance(sigma_bkgx, (list, np.ndarray)):
                err_msg = "'sigma_bkgx' has to be list, numpy.ndarray or None"
                raise message.NephastError(err_msg)

            if not (len(sigma_bkgx) == length):
                err_msg = "'sigma_bkgx' has to have the same length as 'x'"
                raise message.NephastError(err_msg)

            if not all(isinstance(i, (np.ma.MaskedArray, np.integer, int,
                                      float)) for i in sigma_bkgx):
                err_msg = "'sigma_bkgx' elements have to be int or float"
                raise message.NephastError(err_msg)

            if any((i <= 0) for i in sigma_bkgx):
                err_msg = "'sigma_bkgx' elements have to be positive"
                raise message.NephastError(err_msg)

        if bkgy is not None:
            if bkgc is None:
                err_msg = "'bkgy' requires to declare 'bkgc'"
                raise message.NephastError(err_msg)

            if bkgx is None:
                err_msg = "Both 'bkgx' and 'bkgy' have to be declared"
                raise message.NephastError(err_msg)

            if not isinstance(bkgy, (list, np.ndarray)):
                err_msg = "'bkgy' has to be list, numpy.ndarray or None"
                raise message.NephastError(err_msg)

            if not (len(bkgy) == length):
                err_msg = "'bkgy' has to have the same length as 'x'"
                raise message.NephastError(err_msg)

            if not all(isinstance(i, (np.ma.MaskedArray, np.integer, int,
                                      float)) for i in bkgy):
                err_msg = "'bkgy' elements have to be int or float"
                raise message.NephastError(err_msg)

        if bkgy0 is not None:
            if bkgy is None:
                err_msg = "'bkgy0' requires to declare 'bkgy'"
                raise message.NephastError(err_msg)

            if not isinstance(bkgy0, (list, np.ndarray)):
                err_msg = "'bkgy0' has to be list, numpy.ndarray or None"
                raise message.NephastError(err_msg)

            if not (len(bkgy0) == length):
                err_msg = "'bkgy0' has to have the same length as 'x'"
                raise message.NephastError(err_msg)

            if not all(isinstance(i, (np.ma.MaskedArray, np.integer, int)) for i
                       in bkgy0):
                err_msg = "'bkgy0' elements have to be int"
                raise message.NephastError(err_msg)

        if sigma_bkgy is not None:
            if bkgy is None:
                err_msg = "'bkgy' has to be defined for 'sigma_bkgy'"
                raise message.NephastError(err_msg)

            if sigma_bkgx is None:
                err_msg = \
                    "Both 'sigma_bkgx' and 'sigma_bkgy' have to be declared"
                raise message.NephastError(err_msg)

            if not isinstance(sigma_bkgy, (list, np.ndarray)):
                err_msg = "'sigma_bkgy' has to be list, numpy.ndarray or None"
                raise message.NephastError(err_msg)

            if not (len(sigma_bkgy) == length):
                err_msg = "'sigma_bkgy' has to have the same length as 'x'"
                raise message.NephastError(err_msg)

            if not all(isinstance(i, (np.ma.MaskedArray, np.integer, int,
                                      float)) for i in sigma_bkgy):
                err_msg = "'sigma_bkgy' elements have to be int or float"
                raise message.NephastError(err_msg)

            if any((i <= 0) for i in sigma_bkgy):
                err_msg = "'sigma_bkgx' elements have to be positive"
                raise message.NephastError(err_msg)

        if snr is not None:
            if not isinstance(snr, (list, np.ndarray)):
                err_msg = "'snr' has to be list, numpy.ndarray or None"
                raise message.NephastError(err_msg)

            if not (len(snr) == length):
                err_msg = "'snr' has to have the same length as 'x'"
                raise message.NephastError(err_msg)

            if not all(isinstance(i, (np.ma.MaskedArray, np.integer, int,
                                      float)) for i in snr):
                err_msg = "'snr' elements have to be int or float"
                raise message.NephastError(err_msg)

            if any((i <= 0) for i in snr):
                err_msg = "'snr' elements have to be positive"
                raise message.NephastError(err_msg)

        if chi2r is not None:
            if not isinstance(chi2r, (list, np.ndarray)):
                err_msg = "'chi2r' has to be list, numpy.ndarray or None"
                raise message.NephastError(err_msg)

            if not (len(chi2r) == length):
                err_msg = "'chi2r' has to have the same length as 'x'"
                raise message.NephastError(err_msg)

            if not all(isinstance(i, (np.ma.MaskedArray, np.integer, int,
                                      float)) for i in chi2r):
                err_msg = "'chi2r' elements have to be int or float"
                raise message.NephastError(err_msg)

            if any((i <= 0) for i in chi2r):
                err_msg = "'chi2r' elements have to be positive"
                raise message.NephastError(err_msg)

        if not isinstance(psfun, (psf.Psf, type(None))):
            err_msg = "'psfun' has to be psf.Psf"
            raise message.NephastError(err_msg)

        if not isinstance(original, str):
            err_msg = "'original' has to be str"
            raise message.NephastError(err_msg)

        if original not in ('m', 'counts', 'peak'):
            err_msg = "'original' has to be 'm', 'counts' or 'peak'"
            raise message.NephastError(err_msg)

        if (original is 'm') and (m is None):
            err_msg = "'m' has not to be None when 'original' is 'm'"
            raise message.NephastError(err_msg)

        if (original is 'counts') and (counts is None):
            err_msg = "'counts' has not to be None when 'original' is 'counts'"
            raise message.NephastError(err_msg)

        if (original is 'peak') and (peak is None):
            err_msg = "'peak' has not to be None when 'original' is 'peak'"
            raise message.NephastError(err_msg)

        if (original is 'peak') and (psfun is None):
            err_msg = "'psfun' has not to be None when 'original' is 'peak'"
            raise message.NephastError(err_msg)

        if not isinstance(conf, (config. Config, type(None))):
            err_msg = "'conf' has to be config.Config"
            raise message.NephastError(err_msg)

        # Define configuration
        if conf is None:
            conf = config.Config()

        # Prepare empty columns
        if m is None:
            m = [None] * length

        if counts is None:
            counts = [None] * length

        if peak is None:
            peak = [None] * length

        if sigma_m is None:
            sigma_m = [None] * length

        if sigma_counts is None:
            sigma_counts = [None] * length

        if sigma_peak is None:
            sigma_peak = [None] * length

        if name is None:
            name = [('obj_' + i) for i in
                    np.arange(1, (length + 1)).astype('str')]

        if sigma_x is None:
            sigma_x = [None] * length

        if sigma_y is None:
            sigma_y = [None] * length

        if bkg is None:
            bkg = [None] * length

        if sigma_bkg is None:
            sigma_bkg = [None] * length

        if bkgc is None:
            bkgc = [None] * length

        if sigma_bkgc is None:
            sigma_bkgc = [None] * length

        if bkgx is None:
            bkgx = [None] * length

        if bkgx0 is None:
            x_tmp = np.round(np.array(x))
            bkgx0 = x_tmp.astype(int)

        if sigma_bkgx is None:
            sigma_bkgx = [None] * length

        if bkgy is None:
            bkgy = [None] * length

        if bkgy0 is None:
            y_tmp = np.round(np.array(y))
            bkgy0 = y_tmp.astype(int)

        if sigma_bkgy is None:
            sigma_bkgy = [None] * length

        if snr is None:
            snr = [None] * length

        if chi2r is None:
            chi2r = [None] * length

        # Store attributes
        self.fit_ready = False
        self.snr_ready = False
        self.psf = None
        self.n = 0
        self.colnames = []
        self.n_col = 0
        self.nbkg = 0

        tab = table.Table([name, x, sigma_x, y, sigma_y, m, sigma_m, counts,
                           sigma_counts, peak, sigma_peak, bkg, sigma_bkg, bkgc,
                           sigma_bkgc, bkgx, bkgx0, sigma_bkgx, bkgy, bkgy0,
                           sigma_bkgy, snr, chi2r],
                          names=('name', 'x', 'sigma_x', 'y', 'sigma_y', 'm',
                                 'sigma_m', 'counts', 'sigma_counts', 'peak',
                                 'sigma_peak',  'bkg', 'sigma_bkg', 'bkgc',
                                 'sigma_bkgc', 'bkgx', 'bkgx0', 'sigma_bkgx',
                                 'bkgy', 'bkgy0', 'sigma_bkgy', 'snr', 'chi2r'),
                          dtype=(np.str, np.float, np.float, np.float, np.float,
                                 np.float, np.float, np.float, np.float,
                                 np.float, np.float, np.float, np.float,
                                 np.float, np.float, np.float, np.integer,
                                 np.float, np.float, np.integer, np.float,
                                 np.float, np.float),
                          masked=True)
        tab['name'] = tab['name'].astype('U12')

        for i_col in tab.colnames[1:]:
            tab[i_col] = np.ma.masked_invalid(tab[i_col])

        self.tab = tab
        self.psf = psfun
        self.m1c = conf.m1c
        self.intensity_transf(original=original)

    def __setattr__(self, name, val):
        """Set attribute.

        Parameters:
            :param name: Attribute name
            :type name: str
            :param val: Attribute value
            :type val: any
        """

        # Check attribute
        if name is 'psf':
            if not isinstance(val, (psf.Psf, type(None))):
                err_msg = "'{0}' has to be psf.Psf".format(name)
                raise message.NephastError(err_msg)

        # Change catalog length and number of background parameters
        if name is 'tab':
            self.n = len(val)
            self.colnames = val.colnames
            self.n_col = len(self.colnames)

            if all([not np.ma.is_masked(i) for i in val['bkgx']]) and \
                    all([not np.ma.is_masked(i) for i in val['bkgy']]):
                self.nbkg = 3
            else:
                self.nbkg = 1

        # Set attribute
        super().__setattr__(name, val)

        # Check if it is ready for fitting
        if (name in ('psf', 'tab')) and all([hasattr(self, i) for i in
                                             ('psf', 'tab')]):
            if isinstance(self.psf, psf.Psf) and not \
                    np.ma.is_masked(self['bkgc']):
                self.fit_ready = True
            else:
                self.fit_ready = False

    def __getitem__(self, idx):
        """Get item. Stars are selected by integer single index, list,
        numpy.ndarray or slice.
        Columns are selected by string single index, list or numpy.ndarray.

        Parameters:
            :param idx: Item index
            :type idx: slice, int, str, list, numpy.ndarray (int, str), tuple
                [2] (slice, int, str, list, numpy.ndarray (int, str))

        Returns:
            :return val: Item value
            :rtype val: astropy.table.Table, astropy.table.row.Row,
            astropy.table.column.Column, int, float, str
        """

        # Check arguments
        idx_row = None
        idx_col = None

        if not isinstance(idx, (tuple, slice, list, np.ndarray, np.integer, int,
                                str)):
            err_msg = "'idx' has to be tuple, slice, list, numpy.ndarray, " + \
                      "int or str"
            raise message.NephastError(err_msg)

        if isinstance(idx, (np.integer, int)):
            if not ((idx >= 0) and (idx < self.n)):
                err_msg = ("'idx' has to be non-negative and smaller than or " +
                           "equal to {0:d}").format(self.n - 1)
                raise message.NephastError(err_msg)

        if isinstance(idx, str):
            if idx not in self.colnames:
                err_msg = "'idx' has to be a valid column name"
                raise message.NephastError(err_msg)

        if isinstance(idx, slice):
            if idx.start is not None:
                if not ((idx.start >= 0) and (idx.start <= self.n)):
                    err_msg = ("The start of 'idx' has to be non-negative " +
                               "and smaller than or equal to " +
                               "{0:d}").format(self.n)
                    raise message.NephastError(err_msg)

                if idx.stop is not None:
                    if not (idx.start <= idx.stop):
                        err_msg = "The start of 'idx' has to be smaller " + \
                                  "than or equal to the end of 'idx'"
                        raise message.NephastError(err_msg)

            if idx.stop is not None:
                if not (idx.stop <= self.n):
                    err_msg = "The end of 'idx' has to be smaller than or " + \
                              "equal to the start of 'idx'"
                    raise message.NephastError(err_msg)

        if isinstance(idx, (list, np.ndarray)):
            list_row = np.all([isinstance(i, (np.integer, int)) for i in idx])
            list_col = np.all([isinstance(i, str) for i in idx])

            if not (list_row or list_col):
                err_msg = "'idx' has to have all int or str"
                raise message.NephastError(err_msg)

            if list_row:
                if not (np.all([(i >= 0) for i in idx]) and
                        np.all([(i < self.n) for i in idx])):
                    err_msg = ("'idx' elements have to be non-negative and " +
                               "smaller than or equal to {0:d}").format(self.n
                                                                        - 1)
                    raise message.NephastError(err_msg)
            else:
                if not np.all([(i in self.colnames) for i in idx]):
                    err_msg = "'idx' elements have to be valid column names"
                    raise message.NephastError(err_msg)

        if isinstance(idx, tuple):
            if not (len(idx) == 2):
                err_msg = "'idx' has to have 2 elements"
                raise message.NephastError(err_msg)

            bool_row = [isinstance(i, (slice, np.integer, int)) for i in idx]
            bool_col = [isinstance(i, str) for i in idx]
            bool_list = [isinstance(i, (list, np.ndarray)) for i in idx]

            for i_list in np.where(bool_list)[0]:
                if np.all([isinstance(i, (np.integer, int)) for i in
                           idx[i_list]]):
                    bool_row[i_list] = True
                elif np.all([isinstance(i, str) for i in idx[i_list]]):
                    bool_col[i_list] = True

            idx_row = np.where(bool_row)[0][0]
            idx_col = np.where(bool_col)[0][0]

            if not (any(bool_row) and any(bool_col)):
                err_msg = "'idx' has to have a slice or int or list or " + \
                          "numpy.ndarray of int and a str or list or " + \
                          "numpy.ndarray of str"
                raise message.NephastError(err_msg)

            if isinstance(idx[idx_row], (np.integer, int)):
                if not ((idx[idx_row] >= 0) and (idx[idx_row] < self.n)):
                    err_msg = ("The integer element of 'idx' has to be " +
                               "non-negative and smaller than or equal to " +
                               "{0:d}").format(self.n - 1)
                    raise message.NephastError(err_msg)

            if isinstance(idx[idx_row], slice):
                if idx[idx_row].start is not None:
                    if not ((idx[idx_row].start >= 0) and
                            (idx[idx_row].start <= self.n)):
                        err_msg = ("The start of the row element of 'idx' " +
                                   "has to be non-negative and smaller than " +
                                   "or equal to {0:d}").format(self.n)
                        raise message.NephastError(err_msg)

                    if idx[idx_row].stop is not None:
                        if not (idx[idx_row].start <= idx[idx_row].stop):
                            err_msg = "The start of the row element of " + \
                                      "'idx' has to be smaller than or " + \
                                      "equal to the end of the row element " + \
                                      "of 'idx'"
                            raise message.NephastError(err_msg)

                if idx[idx_row].stop is not None:
                    if not (idx[idx_row].stop <= self.n):
                        err_msg = "The end of the row element of 'idx' has " + \
                                  "to be smaller than or equal to the " + \
                                  "start of the row element of 'idx'"
                        raise message.NephastError(err_msg)

            if isinstance(idx[idx_row], (list, np.ndarray)):
                if not (np.all([(i >= 0) for i in idx[idx_row]]) and
                        np.all([(i < self.n) for i in idx[idx_row]])):
                    err_msg = ("The 'idx' row elements have to be " +
                               "non-negative and smaller than or equal to " +
                               "{0:d}").format(self.n - 1)
                    raise message.NephastError(err_msg)

            if isinstance(idx[idx_col], str):
                if idx[idx_col] not in self.colnames:
                    err_msg = "The column element of 'idx' has to be a " + \
                              "valid column name"
                    raise message.NephastError(err_msg)

            if isinstance(idx[idx_col], (list, np.ndarray)):
                if not np.all([(i in self.colnames) for i in idx[idx_col]]):
                    err_msg = "The 'idx' column elements have to be valid " + \
                              "column names"
                    raise message.NephastError(err_msg)

        # Select item
        if isinstance(idx, (slice, list, np.ndarray, int, str)):
            val = self.tab[idx]
        else:
            val = self.tab[idx[idx_col]][idx[idx_row]]

        return val

    def __setitem__(self, idx, val):
        """Set item. Stars are selected by integer single index, list,
        numpy.ndarray or slice.
        Columns are selected by string single index, list or numpy.ndarray.

        Parameters:
            :param idx: Item index
            :type idx: slice, int, str, list, numpy.ndarray (int, str), tuple
                [2] (slice, int, str, list, numpy.ndarray (int, str))
            :param val: Item value
            :type val: astropy.table.Table, astropy.table.row.Row,
                astropy.table.column.Column, int, float, str, list,
                numpy.ndarray (int, float, str)
        """

        # Check arguments
        idx_row = None
        idx_col = None

        if not isinstance(idx, (tuple, slice, list, np.ndarray, np.integer, int,
                                str)):
            err_msg = "'idx' has to be tuple, slice, list, numpy.ndarray, " + \
                      "int or str"
            raise message.NephastError(err_msg)

        if isinstance(idx, (np.integer, int)):
            if not ((idx >= 0) and (idx < self.n)):
                err_msg = ("'idx' has to be non-negative and smaller than or " +
                           "equal to {0:d}").format(self.n - 1)
                raise message.NephastError(err_msg)

            if not isinstance(val, table.Row):
                err_msg = "'val' has to be astropy.table.row.Row"
                raise message.NephastError(err_msg)

        if isinstance(idx, str):
            if idx not in self.colnames:
                err_msg = "'idx' has to be a valid column name"
                raise message.NephastError(err_msg)

            if not isinstance(val, table.column.Column):
                err_msg = "'val' has to be astropy.table.column.Column"
                raise message.NephastError(err_msg)

        if isinstance(idx, slice):
            if idx.start is not None:
                if not ((idx.start >= 0) and (idx.start <= self.n)):
                    err_msg = ("The start of 'idx' has to be non-negative " +
                               "and smaller than or equal to " +
                               "{0:d}").format(self.n)
                    raise message.NephastError(err_msg)

                if idx.stop is not None:
                    if not (idx.start <= idx.stop):
                        err_msg = "The start of 'idx' has to be smaller " + \
                                  "than or equal to the end of 'idx'"
                        raise message.NephastError(err_msg)

            if idx.stop is not None:
                if not (idx.stop <= self.n):
                    err_msg = "The end of 'idx' has to be smaller than or " + \
                              "equal to the start of 'idx'"
                    raise message.NephastError(err_msg)

        if isinstance(idx, (list, np.ndarray)):
            list_row = np.all([isinstance(i, (np.integer, int)) for i in idx])
            list_col = np.all([isinstance(i, str) for i in idx])

            if not (list_row or list_col):
                err_msg = "'idx' elements have to be int or str"
                raise message.NephastError(err_msg)

            if list_row:
                if not (np.all([(i >= 0) for i in idx]) and
                        np.all([(i < self.n) for i in idx])):
                    err_msg = ("'idx' elements have to be non-negative and " +
                               "smaller than or equal to {0:d}").format(self.n
                                                                        - 1)
                    raise message.NephastError(err_msg)
            else:
                if not np.all([(i in self.colnames) for i in idx]):
                    err_msg = "'idx' elements have to be valid column names"
                    raise message.NephastError(err_msg)

        if isinstance(idx, tuple):
            if not (len(idx) == 2):
                err_msg = "'idx' has to have 2 elements"
                raise message.NephastError(err_msg)

            bool_row = [isinstance(i, (slice, np.integer, int)) for i in idx]
            bool_col = [isinstance(i, str) for i in idx]
            bool_list = [isinstance(i, (list, np.ndarray)) for i in idx]

            for i_list in np.where(bool_list)[0]:
                if np.all([isinstance(i, (np.integer, int)) for i in
                           idx[i_list]]):
                    bool_row[i_list] = True
                elif np.all([isinstance(i, str) for i in idx[i_list]]):
                    bool_col[i_list] = True

            idx_row = np.where(bool_row)[0][0]
            idx_col = np.where(bool_col)[0][0]

            if not (any(bool_row) and any(bool_col)):
                err_msg = "'idx' has to have a slice, int or list or " + \
                          "numpy.ndarray of int and a str or list or " + \
                          "numpy.ndarray of str"
                raise message.NephastError(err_msg)

            if isinstance(idx[idx_row], (np.integer, int)):
                if not ((idx[idx_row] >= 0) and (idx[idx_row] < self.n)):
                    err_msg = ("The integer element of 'idx' has to be " +
                               "non-negative and smaller than or equal to " +
                               "{0:d}").format(self.n - 1)
                    raise message.NephastError(err_msg)

            if isinstance(idx[idx_row], slice):
                if idx[idx_row].start is not None:
                    if not ((idx[idx_row].start >= 0) and
                            (idx[idx_row].start <= self.n)):
                        err_msg = ("The start of the row element of 'idx' " +
                                   "has to be non-negative and smaller than " +
                                   "or equal to {0:d}").format(self.n)
                        raise message.NephastError(err_msg)

                    if idx[idx_row].stop is not None:
                        if not (idx[idx_row].start <= idx[idx_row].stop):
                            err_msg = "The start of the row element of " + \
                                      "'idx' has to be smaller than or " + \
                                      "equal to the end of the row element " + \
                                      "of 'idx'"
                            raise message.NephastError(err_msg)

                if idx[idx_row].stop is not None:
                    if not (idx[idx_row].stop <= self.n):
                        err_msg = "The end of the row element of 'idx' has " + \
                                  "to be smaller than or equal to the " + \
                                  "start of the row element of 'idx'"
                        raise message.NephastError(err_msg)

            if isinstance(idx[idx_row], (list, np.ndarray)):
                if not (np.all([(i >= 0) for i in idx[idx_row]]) and
                        np.all([(i < self.n) for i in idx[idx_row]])):
                    err_msg = ("The 'idx' row elements have to be " +
                               "non-negative and smaller than or equal to " +
                               "{0:d}").format(self.n - 1)
                    raise message.NephastError(err_msg)

            if isinstance(idx[idx_col], str):
                if idx[idx_col] not in self.colnames:
                    err_msg = "The column element of 'idx' has to be a " + \
                              "valid column name"
                    raise message.NephastError(err_msg)

            if isinstance(idx[idx_col], (list, np.ndarray)):
                if not np.all([(i in self.colnames) for i in idx[idx_col]]):
                    err_msg = "The 'idx' column elements have to be valid " + \
                              "column names"
                    raise message.NephastError(err_msg)

        # Change value
        if isinstance(idx, (slice, list, np.ndarray, int, str)):
            self.tab[idx] = val
        else:
            self.tab[idx[idx_col]][idx[idx_row]] = val

    @classmethod
    def rnd(cls, n, xy_range, m_range, m_distr='unif', psfun=None, conf=None):
        """Initialize a simulated catalog of stars with random positions and
        magnitudes.
        Magnitudes can have a uniform or power law distribution.

        Parameters:
            :param n: Number of stars simulated
            :type n: int
            :param xy_range: Range of star positions ([minimum x, maximum x,
                minimum y, maximum y])
            :type xy_range: list [4] (int, float)
            :param m_range: Range of magnitude positions ([minimum magnitude,
                maximum magnitude])
            :type m_range: list [2] (int, float)
            :param m_distr: Magnitude distribution ('unif' for uniform)
            :type m_distr: str, default 'unif'
            :param psfun: PSF of the stars
            :type psfun: psf.Psf; optional
            :param conf: Configuration
            :type conf: config.Config; optional

        Returns:
            :return cat: Simulated catalog
            :rtype cat: catalog.Catalog
        """

        # Check arguments
        if not isinstance(n, (np.integer, int)):
            err_msg = "'n' has to be int"
            raise message.NephastError(err_msg)

        if not isinstance(xy_range, list):
            err_msg = "'xy_range' has to be list"
            raise message.NephastError(err_msg)

        if not (len(xy_range) == 4):
            err_msg = "'xy_range' has to have 4 elements"
            raise message.NephastError(err_msg)

        if not all(isinstance(i, (np.ma.MaskedArray, np.integer, int, float))
                   for i in xy_range):
            err_msg = "'xy_range' elements have to be int or float"
            raise message.NephastError(err_msg)

        if not isinstance(m_range, list):
            err_msg = "'m_range' has to be list"
            raise message.NephastError(err_msg)

        if not (len(m_range) == 2):
            err_msg = "'m_range' has to have 2 elements"
            raise message.NephastError(err_msg)

        if not all(isinstance(i, (np.ma.MaskedArray, np.integer, int, float))
                   for i in m_range):
            err_msg = "'m_range' elements have to be int or float"
            raise message.NephastError(err_msg)

        if not m_distr == 'unif':
            err_msg = "'m_distr' has to be 'unif'"
            raise message.NephastError(err_msg)

        if not isinstance(psfun, (psf.Psf, type(None))):
            err_msg = "'psfun' has to be psf.Psf"
            raise message.NephastError(err_msg)

        if not isinstance(conf, (config.Config, type(None))):
            err_msg = "'conf' has to be config.Config"
            raise message.NephastError(err_msg)

        # Generate random catalog
        x = np.random.uniform(low=xy_range[0], high=xy_range[1], size=n)
        y = np.random.uniform(low=xy_range[2], high=xy_range[3], size=n)
        m = []

        if m_distr == 'unif':
            m = np.random.uniform(low=m_range[0], high=m_range[1], size=n)

        cat = cls(x, y, m=m, psfun=psfun, conf=conf)

        return cat

    def select(self, stars):
        """Select stars from a catalog and create a new catalog.

        Parameters:
            :param stars: Selected stars' indices
            :type stars: list, numpy.ndarray (int)

        Returns:
            :return cat_select: Catalog of selected stars
            :rtype cat_select: catalog.Catalog
        """

        # Check arguments
        if not isinstance(stars, (list, np.ndarray)):
            err_msg = "'stars' has to be list or numpy.ndarray"
            raise message.NephastError(err_msg)

        if not all(isinstance(i, (np.ma.MaskedArray, np.integer, int, float))
                   for i in stars):
            err_msg = "'stars' elements have to be int or float"
            raise message.NephastError(err_msg)

        if not all((star >= 0) and (star < self.n) for star in stars):
            err_msg = \
                "'stars' values have to be within 0 and {0:d}".format(self.n -
                                                                      1)
            raise message.NephastError(err_msg)

        # Select stars
        cat_select = copy.deepcopy(self)
        cat_select.tab = self[stars].copy()

        return cat_select

    def remove(self, stars):
        """Remove stars from a catalog.

        Parameters:
            :param stars: Removed stars' indices
            :type stars: list, numpy.ndarray (int)
        """

        # Check arguments
        if not isinstance(stars, (list, np.ndarray)):
            err_msg = "'stars' has to be list or numpy.ndarray"
            raise message.NephastError(err_msg)

        if not all(isinstance(i, (np.ma.MaskedArray, np.integer, int, float))
                   for i in stars):
            err_msg = "'stars' elements have to be int or float"
            raise message.NephastError(err_msg)

        if not all((star >= 0) and (star < self.n) for star in stars):
            err_msg = \
                "'stars' values have to be within 0 and {0:d}".format(self.n -
                                                                      1)
            raise message.NephastError(err_msg)

        # Remove stars
        tab_tmp = self.tab
        tab_tmp.remove_rows(stars)
        self.tab = tab_tmp

    def range(self, colname, low=None, high=None, exclude_masked=True):
        """Select stars from a catalog with values in a column within a range
        and create a new catalog.
        Counts and peak value (and their sigmas) columns are discarded and
        calculated from instrumental magnitude and its sigma. The minimum and
        maximum values are included in the selection, so if they are equal, only
        that value is selected. A 'None' value means no limit. Objects with
        masked values in the column can be either removed or kept.

        Parameters:
            :param colname: Column name
            :type colname: str
            :param low: Minimum value
            :type low: int, float; optional
            :param high: Maximum value
            :type high: int, float; optional
            :param exclude_masked: Exclude masked values from the output
            :type exclude_masked: bool; default True

        Returns:
            :return cat_range: Catalog of stars within a range
            :rtype cat_range: catalog.Catalog
        """

        # Check arguments
        if colname not in self.tab.colnames:
            err_msg = "'colname' has to be a valid column name"
            raise message.NephastError(err_msg)

        if low is not None:
            if not isinstance(low, (np.ma.MaskedArray, np.integer, int, float)):
                err_msg = "'low' has to be int or float"
                raise message.NephastError(err_msg)

        if high is not None:
            if not isinstance(high, (np.ma.MaskedArray, np.integer, int,
                                     float)):
                err_msg = "'high' has to be int or float"
                raise message.NephastError(err_msg)

        if (low is not None) and (high is not None):
            if high < low:
                err_msg = "'high' has to greater than or equal to 'low'"
                raise message.NephastError(err_msg)

        if (low is None) and (high is None):
            err_msg = "at least one of 'low' and 'high' has not to be None"
            raise message.NephastError(err_msg)

        if not isinstance(exclude_masked, bool):
            err_msg = "'exclude_masked' has to be bool"
            raise message.NephastError(err_msg)

        # Select stars
        cat_range = copy.deepcopy(self)
        idx_nomask = np.where(~cat_range[colname].mask)[0]
        idx_select = np.arange(0, cat_range.n)

        if low is not None:
            idx_remove_low = np.ma.where(cat_range[colname][idx_nomask] <
                                         low)[0]
            idx_select = np.setdiff1d(idx_select, idx_nomask[idx_remove_low])

        if high is not None:
            idx_remove_high = np.ma.where(cat_range[colname][idx_nomask] >
                                          high)[0]
            idx_select = np.setdiff1d(idx_select, idx_nomask[idx_remove_high])

        if exclude_masked:
            idx_mask = np.where(cat_range[colname].mask)[0]
            idx_select = np.setdiff1d(idx_select, idx_mask)

        cat_range = cat_range.select(idx_select)

        return cat_range

    def rnd_guess(self, sigma_r, sigma_counts):
        """Change positions and counts of a catalog randomly using a normal
        distribution. Istrumental magnitudes and peak values are then converted.
        The position sigma is an absolute value, while the counts sigma is
        relative. Both are the same for every star.

        Parameters:
            :param sigma_r: Absolute STD of positions
            :type sigma_r: int, float
            :param sigma_counts: Relative STD of counts (DN)
            :type sigma_counts: int, float
         """

        # Check arguments
        if not isinstance(sigma_r, (np.ma.MaskedArray, np.integer, int, float)):
            err_msg = "'sigma_r' has to be int or float"
            raise message.NephastError(err_msg)

        if not (sigma_r > 0):
            err_msg = "'sigma_r' has to be positive"
            raise message.NephastError(err_msg)

        if not isinstance(sigma_counts, (np.ma.MaskedArray, np.integer, int,
                                         float)):
            err_msg = "'sigma_counts' has to be int or float"
            raise message.NephastError(err_msg)

        if not (sigma_counts > 0):
            err_msg = "'sigma_counts' has to be positive"
            raise message.NephastError(err_msg)

        # Calculate changes
        thetas = np.random.uniform(low=0, high=(2 * math.pi), size=self.n)
        rads = np.random.normal(scale=sigma_r, size=self.n)
        dx = rads * np.cos(thetas)
        dy = rads * np.sin(thetas)
        dcounts = np.random.normal(scale=(self['counts'] * sigma_counts))

        # Change attributes
        tab_tmp = self.tab
        tab_tmp['x'] += dx
        tab_tmp['y'] += dy
        tab_tmp['counts'] += dcounts
        tab_tmp['m'] = self.counts_to_m(tab_tmp['counts'], m1c=self.m1c)

        if self.psf:
            tab_tmp['peak'] = self.counts_to_peak(tab_tmp['counts'],
                                                  self.psf.peak)

        self.tab = tab_tmp

    def nbkg_change(self, nbkg):
        """Change the number of parameters used to describe the background.
        If the number of background parameters is increased, masked values in
        'bkgx' and 'bkgy' are converted to 0, in 'sigma_bkgx' and 'sigma_bkgy'
        are converted to 1. If the number of background parameters is decreased,
        'bkgx', 'bkgy', 'sigma_bkgx' and 'sigma_bkgy' are masked.

        Parameters:
            :param nbkg: Number of parameters used to describe the background.
                1: flat background. 3: background with gradient
            :type nbkg: int
         """

        # Check arguments
        if not (nbkg in (1, 3)):
            err_msg = "'nbkg' has to be 1 or 3"
            raise message.NephastError(err_msg)

        # Increase background parameters
        if nbkg > self.nbkg:
            tab = self.tab
            np.ma.set_fill_value(tab['bkgx'], 0)
            tab['bkgx'] = np.ma.array(tab['bkgx'].filled())
            np.ma.set_fill_value(tab['sigma_bkgx'], 1)
            tab['sigma_bkgx'] = np.ma.array(tab['sigma_bkgx'].filled())
            np.ma.set_fill_value(tab['bkgy'], 0)
            tab['bkgy'] = np.ma.array(tab['bkgy'].filled())
            np.ma.set_fill_value(tab['sigma_bkgy'], 1)
            tab['sigma_bkgy'] = np.ma.array(tab['sigma_bkgy'].filled())
            self.tab = tab

        # Decrease background parameters
        elif nbkg < self.nbkg:
            tab = self.tab
            tab['bkgx'].mask = True
            tab['sigma_bkgx'].mask = True
            tab['bkgy'].mask = True
            tab['sigma_bkgy'].mask = True
            self.tab = tab

    def neighbors(self, idx, radius):
        """Find the stars in a catalog that are within a radius from one of#####
        them. The star itself is not returned.

        Parameters:
            :param idx: Index in the catalog of the star measured from
            :type idx: int
            :param radius: Distance within which stars are found
            :type radius: int, float

        Returns:
            :return near: Indices of the stars in the catalog within the radius
            :rtype near: numpy.ndarray (int)
        """

        # Check arguments
        if not isinstance(idx, (np.integer, int)):
            err_msg = "'idx' has to be int"
            raise message.NephastError(err_msg)

        if not ((idx >= 0) and (idx < self.n)):
            err_msg = "'idx' has to be in the range 0: {0:d}".format(self.n - 1)
            raise message.NephastError(err_msg)

        if not isinstance(radius, (np.ma.MaskedArray, np.integer, int, float)):
            err_msg = "'radius' has to be int or float"
            raise message.NephastError(err_msg)

        if not (radius > 0):
            err_msg = "'radius' has to be positive"
            raise message.NephastError(err_msg)

        # Find neighboring stars
        dists = np.hypot((self['x'].data - self['x'][idx]),
                         (self['y'].data - self['y'][idx]))
        near = np.where(dists <= radius)[0]
        near = near[near != idx]

        return near

    @staticmethod
    def m_to_counts(m, m1c=None):
        """Convert instrumental magnitudes to counts.
        If the magnitude calibration is not provided, the default PSF
        configuration is used

        Parameters:
            :param m: Instrumental magnitudes of the stars
            :type m: int, float, list, numpy.ndarray [n] (int, float)
            :param m1c: Instrumental magnitude of a star with 1 DN of intensity
            :type m1c: int, float; optional

        Returns:
            :return counts: Counts of the stars (DN)
            :rtype counts: float, numpy.ndarray [n] (float)
        """

        # Check arguments
        if not isinstance(m, (np.ma.MaskedArray, np.integer, int, float, list,
                              np.ndarray)):
            err_msg = "'m' has to be int, float, list or numpy.ndarray"
            raise message.NephastError(err_msg)

        if isinstance(m, (list, np.ndarray)) and not \
                all(isinstance(i, (np.ma.MaskedArray, np.integer, int, float))
                    for i in m):
            err_msg = "'m' elements have to be int or float"
            raise message.NephastError(err_msg)

        if not isinstance(m1c, (np.integer, int, float, type(None))):
            err_msg = "'m1c' has to be int or float"
            raise message.NephastError(err_msg)

        # Define configuration
        if m1c is None:
            conf = config.Config()
            m1c = conf.m1c

        # Convert instrumental magnitudes to counts
        if isinstance(m, list):
            m = np.array(m)

        counts = 10 ** (0.4 * (m1c - m))

        if isinstance(counts, np.ndarray):
            counts.astype(np.float)
        else:
            counts = np.float(counts)

        return counts

    @staticmethod
    def sigmam_to_sigmacounts(m, sigma_m, m1c=None):
        """Convert instrumental magnitudes STD to counts STD.
        If the magnitude calibration is not provided, the default PSF
        configuration is used

        Parameters:
            :param m: Instrumental magnitudes of the stars
            :type m: int, float, list, numpy.ndarray [n] (int, float)
            :param sigma_m: Instrumental magnitudes STD of the stars
            :type sigma_m: int, float, list, numpy.ndarray [n] (int, float)
            :param m1c: Instrumental magnitude of a star with 1 DN of intensity
            :type m1c: int, float; optional

        Returns:
            :return sigma_counts: Counts STD of the stars (DN)
            :rtype sigma_counts: float, numpy.ndarray [n] (float)
        """

        # Check arguments
        if not isinstance(m, (np.ma.MaskedArray, np.integer, int, float, list,
                              np.ndarray)):
            err_msg = "'m' has to be int, float, list or numpy.ndarray"
            raise message.NephastError(err_msg)

        if isinstance(m, (list, np.ndarray)) and not \
                all(isinstance(i, (np.ma.MaskedArray, np.integer, int, float))
                    for i in m):
            err_msg = "'m' elements have to be int or float"
            raise message.NephastError(err_msg)

        if not isinstance(sigma_m, (np.ma.MaskedArray, np.integer, int, float,
                                    list, np.ndarray)):
            err_msg = "'sigma_m' has to be int, float, list or numpy.ndarray"
            raise message.NephastError(err_msg)

        if isinstance(sigma_m, (list, np.ndarray)) and not \
            all(isinstance(i, (np.ma.MaskedArray, np.integer, int, float))
                for i in sigma_m):
                err_msg = "'sigma_m' elements have to be int or float"
                raise message.NephastError(err_msg)

        if sum([isinstance(i, (list, np.ndarray)) for i in (m, sigma_m)]) == 1:

            err_msg = \
                "Both 'm' and 'sigma_m' have to be list or numpy.ndarray"
            raise message.NephastError(err_msg)

        elif sum([isinstance(i, (list, np.ndarray)) for i in (m, sigma_m)]) == \
                2:

            if not len(np.array(sigma_m)) == len(np.array(m)):
                err_msg = "'sigma_m' has to have the same length as 'm'"
                raise message.NephastError(err_msg)

        if not isinstance(m1c, (np.integer, int, float, type(None))):
            err_msg = "'m1c' has to be int or float"
            raise message.NephastError(err_msg)

        # Define configuration
        if m1c is None:
            conf = config.Config()
            m1c = conf.m1c

        # Convert instrumental magnitudes STD to counts STD
        if isinstance(m, list):
            m = np.array(m)

        if isinstance(sigma_m, list):
            m = np.array(sigma_m)

        sigma_counts = np.abs((-0.4 * np.log(10) *
                               (10 ** (0.4 * (m1c - m)))) * sigma_m)

        return sigma_counts

    @staticmethod
    def m_to_peak(m, peak_norm, m1c=None):
        """Convert instrumental magnitudes to peak values using a normalized PSF
        peak.
        If the magnitude calibration is not provided, the default PSF
        configuration is used

        Parameters:
            :param m: Instrumental magnitudes of the stars
            :type m: int, float, list, numpy.ndarray [n] (int, float)
            :param peak_norm: Peak of a normalized PSF
            :type peak_norm: int, float
            :param m1c: Instrumental magnitude of a star with 1 DN of intensity
            :type m1c: int, float; optional

        Returns:
            :return peak: Peak values of the stars
            :rtype peak: float, numpy.ndarray [n] (float)
        """

        # Check arguments
        if not isinstance(m, (np.ma.MaskedArray, np.integer, int, float, list,
                              np.ndarray)):
            err_msg = "'m' has to be int, float, list or numpy.ndarray"
            raise message.NephastError(err_msg)

        if isinstance(m, (list, np.ndarray)) and not \
                all(isinstance(i, (np.ma.MaskedArray, np.integer, int, float))
                    for i in m):
            err_msg = "'m' elements have to be int or float"
            raise message.NephastError(err_msg)

        if not isinstance(peak_norm, (np.ma.MaskedArray, np.integer, int,
                                      float)) and not (peak_norm > 0):
            err_msg = "'peak_norm' has to be positive"
            raise message.NephastError(err_msg)

        if not isinstance(m1c, (np.integer, int, float, type(None))):
            err_msg = "'m1c' has to be int or float"
            raise message.NephastError(err_msg)

        # Define configuration
        if m1c is None:
            conf = config.Config()
            m1c = conf.m1c

        # Convert instrumental magnitudes to peak values
        if isinstance(m, list):
            m = np.array(m)

        peak = Catalog.m_to_counts(m, m1c) * peak_norm

        if isinstance(peak, np.ndarray):
            peak.astype(np.float)
        else:
            peak = np.float(peak)

        return peak

    @staticmethod
    def sigmam_to_sigmapeak(m, sigma_m, peak_norm, m1c=None):
        """Convert instrumental magnitudes STD to peak values STD  using a
        normalized PSF peak.
        If the magnitude calibration is not provided, the default PSF
        configuration is used

        Parameters:
            :param m: Instrumental magnitudes of the stars
            :type m: int, float, list, numpy.ndarray [n] (int, float)
            :param sigma_m: Instrumental magnitudes STD of the stars
            :type sigma_m: int, float, list, numpy.ndarray [n] (int, float)
            :param peak_norm: Peak of a normalized PSF
            :type peak_norm: int, float
            :param m1c: Instrumental magnitude of a star with 1 DN of intensity
            :type m1c: int, float; optional

        Returns:
            :return sigma_peak: Peak values STD of the stars
            :rtype sigma_peak: float, numpy.ndarray [n] (float)
        """

        # Check arguments
        if not isinstance(m, (np.ma.MaskedArray, np.integer, int, float, list,
                              np.ndarray)):
            err_msg = "'m' has to be int, float, list or numpy.ndarray"
            raise message.NephastError(err_msg)

        if isinstance(m, (list, np.ndarray)) and not \
                all(isinstance(i, (np.ma.MaskedArray, np.integer, int, float))
                    for i in m):
            err_msg = "'m' elements have to be int or float"
            raise message.NephastError(err_msg)

        if not isinstance(sigma_m, (np.ma.MaskedArray, np.integer, int, float,
                                    list, np.ndarray)):
            err_msg = "'sigma_m' has to be int, float, list or numpy.ndarray"
            raise message.NephastError(err_msg)

        if isinstance(sigma_m, (list, np.ndarray)) and not \
                all(isinstance(i, (np.ma.MaskedArray, np.integer, int, float))
                    for i in sigma_m):
            err_msg = "'sigma_m' elements have to be int or float"
            raise message.NephastError(err_msg)

        if sum([isinstance(i, (list, np.ndarray)) for i in (m, sigma_m)]) == 1:

            err_msg = \
                "Both 'm' and 'sigma_m' have to be list or numpy.ndarray"
            raise message.NephastError(err_msg)

        elif sum([isinstance(i, (list, np.ndarray)) for i in (m, sigma_m)]) == \
                2:

            if not len(np.array(sigma_m)) == len(np.array(m)):
                err_msg = "'sigma_m' has to have the same length as 'm'"
                raise message.NephastError(err_msg)

        if not isinstance(peak_norm, (np.ma.MaskedArray, np.integer, int,
                                      float)) and not (peak_norm > 0):
            err_msg = "'peak_norm' has to be positive"
            raise message.NephastError(err_msg)

        if not isinstance(m1c, (np.integer, int, float, type(None))):
            err_msg = "'m1c' has to be int or float"
            raise message.NephastError(err_msg)

        # Define configuration
        if m1c is None:
            conf = config.Config()
            m1c = conf.m1c

        # Convert instrumental magnitudes STD to peak values STD
        if isinstance(m, list):
            m = np.array(m)

        if isinstance(sigma_m, list):
            m = np.array(sigma_m)

        sigma_peak = Catalog.sigmam_to_sigmacounts(m, sigma_m, m1c) * peak_norm

        if isinstance(sigma_peak, np.ndarray):
            sigma_peak.astype(np.float)
        else:
            sigma_peak = np.float(sigma_peak)

        return sigma_peak

    @staticmethod
    def counts_to_m(counts, m1c=None):
        """Convert counts to instrumental magnitudes.
        If the magnitude calibration is not provided, the default PSF
        configuration is used

        Parameters:
            :param counts: Counts of the stars (DN)
            :type counts: int, float, list, numpy.ndarray [n] (int, float)
            :param m1c: Instrumental magnitude of a star with 1 DN of intensity
            :type m1c: int, float; optional

        Returns:
            :return m: Instrumental magnitudes of the stars
            :rtype m: float, numpy.ndarray [n] (float)
        """

        # Check arguments
        if not isinstance(counts, (np.ma.MaskedArray, np.integer, int, float,
                                   list, np.ndarray)):
            err_msg = "'counts' has to be int, float, list or numpy.ndarray"
            raise message.NephastError(err_msg)

        if isinstance(counts, (list, np.ndarray)) and not \
                all(isinstance(i, (np.ma.MaskedArray, np.integer, int, float))
                    for i in counts):
            err_msg = "'counts' elements have to be int or float"
            raise message.NephastError(err_msg)

        if not isinstance(m1c, (np.integer, int, float, type(None))):
            err_msg = "'m1c' has to be int or float"
            raise message.NephastError(err_msg)

        # Define configuration
        if m1c is None:
            conf = config.Config()
            m1c = conf.m1c

        # Convert counts to instrumental magnitudes
        if isinstance(counts, list):
            counts = np.array(counts)

        m = np.abs((-2.5 * np.log10(counts)) + m1c)

        return m

    @staticmethod
    def sigmacounts_to_sigmam(counts, sigma_counts):
        """Convert counts STD to instrumental magnitudes STD.

        Parameters:
            :param counts: Counts of the stars (DN)
            :type counts: int, float, list, numpy.ndarray [n] (int, float)
            :param sigma_counts: Counts STD of the stars (DN)
            :type sigma_counts: int, float, list, numpy.ndarray [n] (int, float)

        Returns:
            :return sigma_m: Instrumental magnitudes STD of the stars
            :rtype sigma_m: float, numpy.ndarray [n] (float)
        """

        # Check arguments
        if not isinstance(counts, (np.ma.MaskedArray, np.integer, int, float,
                                   list, np.ndarray)):
            err_msg = "'counts' has to be int, float, list or numpy.ndarray"
            raise message.NephastError(err_msg)

        if isinstance(counts, (list, np.ndarray)) and not \
                all(isinstance(i, (np.ma.MaskedArray, np.integer, int, float))
                    for i in counts):
            err_msg = "'counts' elements have to be int or float"
            raise message.NephastError(err_msg)

        if not isinstance(sigma_counts, (np.ma.MaskedArray, np.integer, int,
                                         float, list, np.ndarray)):
            err_msg = \
                "'sigma_counts' has to be int, float, list or numpy.ndarray"
            raise message.NephastError(err_msg)

        if isinstance(sigma_counts, (list, np.ndarray)) and not \
                all(isinstance(i, (np.ma.MaskedArray, np.integer, int, float))
                    for i in sigma_counts):
            err_msg = "'sigma_counts' elements have to be int or float"
            raise message.NephastError(err_msg)

        if sum([isinstance(i, (list, np.ndarray)) for i in
                (counts, sigma_counts)]) == 1:

            err_msg = "Both 'counts' and 'sigma_counts' have to be list or " + \
                "numpy.ndarray"
            raise message.NephastError(err_msg)

        elif sum([isinstance(i, (list, np.ndarray)) for i in
                  (counts, sigma_counts)]) == 2:

            if not len(np.array(sigma_counts)) == len(np.array(counts)):
                err_msg = \
                    "'sigma_counts' has to have the same length as 'counts'"
                raise message.NephastError(err_msg)

        # Convert counts STD to instrumental magnitudes STD
        if isinstance(counts, list):
            counts = np.array(counts)

        if isinstance(sigma_counts, list):
            sigma_counts = np.array(sigma_counts)

        sigma_m = np.abs((-2.5 / (counts * np.log(10))) * sigma_counts)

        return sigma_m

    @staticmethod
    def counts_to_peak(counts, peak_norm):
        """Convert counts to peak values using a normalized PSF peak.

        Parameters:
            :param counts: Counts of the stars (DN)
            :type counts: int, float, list, numpy.ndarray [n] (int, float)
            :param peak_norm: Peak of a normalized PSF
            :type peak_norm: int, float

        Returns:
            :return peak: Peak values of the stars
            :rtype peak: float, numpy.ndarray [n] (float)
        """

        # Check arguments
        if not isinstance(counts, (np.ma.MaskedArray, np.integer, int, float,
                                   list, np.ndarray)):
            err_msg = "'counts' has to be int, float, list or numpy.ndarray"
            raise message.NephastError(err_msg)

        if isinstance(counts, (list, np.ndarray)) and not \
                all(isinstance(i, (np.ma.MaskedArray, np.integer, int, float))
                    for i in counts):
            err_msg = "'counts' elements have to be int or float"
            raise message.NephastError(err_msg)

        if not isinstance(peak_norm, (np.ma.MaskedArray, np.integer, int,
                                      float)) and not (peak_norm > 0):
            err_msg = "'peak_norm' has to be positive"
            raise message.NephastError(err_msg)

        # Convert counts to peak values
        if isinstance(counts, list):
            counts = np.array(counts)

        peak = counts * peak_norm

        if isinstance(peak, np.ndarray):
            peak.astype(np.float)
        else:
            peak = np.float(peak)

        return peak

    @staticmethod
    def sigmacounts_to_sigmapeak(sigma_counts, peak_norm):
        """Convert counts STD to peak values STD using a normalized PSF peak.

        Parameters:
            :param sigma_counts: Counts STD of the stars (DN)
            :type sigma_counts: int, float, list, numpy.ndarray [n] (int, float)
            :param peak_norm: Peak of a normalized PSF
            :type peak_norm: int, float

        Returns:
            :return sigma_peak: Peak values STD of the stars
            :rtype sigma_peak: float, numpy.ndarray [n] (float)
        """

        # Check arguments
        if not isinstance(sigma_counts, (np.ma.MaskedArray, np.integer, int,
                                         float, list, np.ndarray)):
            err_msg = \
                "'sigma_counts' has to be int, float, list or numpy.ndarray"
            raise message.NephastError(err_msg)

        if isinstance(sigma_counts, (list, np.ndarray)) and not \
                all(isinstance(i, (np.ma.MaskedArray, np.integer, int, float))
                    for i in sigma_counts):
            err_msg = "'sigma_counts' elements have to be int or float"
            raise message.NephastError(err_msg)

        if not isinstance(peak_norm, (np.ma.MaskedArray, np.integer, int,
                                      float)) and not (peak_norm > 0):
            err_msg = "'peak_norm' has to be positive"
            raise message.NephastError(err_msg)

        # Convert counts STD to peak values STD
        if isinstance(sigma_counts, list):
            sigma_counts = np.array(sigma_counts)

        sigma_peak = peak_norm * sigma_counts

        if isinstance(sigma_peak, np.ndarray):
            sigma_peak.astype(np.float)
        else:
            sigma_peak = np.float(sigma_peak)

        return sigma_peak

    @staticmethod
    def peak_to_m(peak, peak_norm, m1c=None):
        """Convert peak values to instrumental magnitudes using a normalized PSF
        peak.
        If the magnitude calibration is not provided, the default PSF
        configuration is used

        Parameters:
            :param peak: Peak values of the stars
            :type peak: int, float, list, numpy.ndarray [n] (int, float)
            :param peak_norm: Peak of a normalized PSF
            :type peak_norm: int, float
            :param m1c: Instrumental magnitude of a star with 1 DN of intensity
            :type m1c: int, float; optional

        Returns:
            :return m: Instrumental magnitudes of the stars
            :rtype m: float, numpy.ndarray [n] (float)
        """

        # Check arguments
        if not isinstance(peak, (np.ma.MaskedArray, np.integer, int, float,
                                 list, np.ndarray)):
            err_msg = "'peak' has to be int, float, list or numpy.ndarray"
            raise message.NephastError(err_msg)

        if isinstance(peak, (list, np.ndarray)) and not \
                all(isinstance(i, (np.ma.MaskedArray, np.integer, int, float))
                    for i in peak):
            err_msg = "'peak' elements have to be int or float"
            raise message.NephastError(err_msg)

        if not isinstance(peak_norm, (np.ma.MaskedArray, np.integer, int,
                                      float)) and not (peak_norm > 0):
            err_msg = "'peak_norm' has to be positive"
            raise message.NephastError(err_msg)

        if not isinstance(m1c, (np.integer, int, float, type(None))):
            err_msg = "'m1c' has to be int or float"
            raise message.NephastError(err_msg)

        # Define configuration
        if m1c is None:
            conf = config.Config()
            m1c = conf.m1c

        # Convert peak values to instrumental magnitudes
        if isinstance(peak, list):
            peak = np.array(peak)

        m = Catalog.counts_to_m((peak / peak_norm), m1c)

        return m

    @staticmethod
    def sigmapeak_to_sigmam(peak, sigma_peak):
        """Convert peak values STD to instrumental magnitudes STD.

        Parameters:
            :param peak: Peak values of the stars
            :type peak: int, float, list, numpy.ndarray [n] (int, float)
            :param sigma_peak: Peak values STD of the stars
            :type sigma_peak: int, float, list, numpy.ndarray [n] (int, float)

        Returns:
            :return sigma_m: Instrumental magnitudes STD of the stars
            :rtype sigma_m: float, numpy.ndarray [n] (float)
        """

        # Check arguments
        if not isinstance(peak, (np.ma.MaskedArray, np.integer, int, float,
                                 list, np.ndarray)):
            err_msg = "'peak' has to be int, float, list or numpy.ndarray"
            raise message.NephastError(err_msg)

        if isinstance(peak, (list, np.ndarray)) and not \
                all(isinstance(i, (np.ma.MaskedArray, np.integer, int, float))
                    for i in peak):
            err_msg = "'peak' elements have to be int or float"
            raise message.NephastError(err_msg)

        if not isinstance(sigma_peak, (np.ma.MaskedArray, np.integer, int,
                                       float, list, np.ndarray)):
            err_msg = \
                "'sigma_peak' has to be int, float, list or numpy.ndarray"
            raise message.NephastError(err_msg)

        if isinstance(sigma_peak, (list, np.ndarray)) and not \
                all(isinstance(i, (np.ma.MaskedArray, np.integer, int, float))
                    for i in sigma_peak):
            err_msg = "'sigma_peak' elements have to be int or float"
            raise message.NephastError(err_msg)

        if sum([isinstance(i, (list, np.ndarray)) for i in (peak,
                                                            sigma_peak)]) == 1:

            err_msg = "Both 'peak' and 'sigma_peak' have to be list or " + \
                      "numpy.ndarray"
            raise message.NephastError(err_msg)

        elif sum([isinstance(i, (list, np.ndarray)) for i in
                  (peak, sigma_peak)]) == 2:

            if not len(np.array(sigma_peak)) == len(np.array(peak)):
                err_msg = "'sigma_peak' has to have the same length as 'peak'"
                raise message.NephastError(err_msg)

        # Convert peak values STD to instrumental magnitudes STD
        if isinstance(peak, list):
            peak = np.array(peak)

        if isinstance(sigma_peak, list):
            sigma_peak = np.array(sigma_peak)

        sigma_m = np.abs((-2.5 / (peak * np.log(10))) * sigma_peak)

        return sigma_m

    @staticmethod
    def peak_to_counts(peak, peak_norm):
        """Convert peak values to counts using a normalized PSF peak.

        Parameters:
            :param peak: Peak values of the stars
            :type peak: int, float, list, numpy.ndarray [n] (int, float)
            :param peak_norm: Peak of a normalized PSF
            :type peak_norm: int, float

        Returns:
            :return counts: Counts of the stars (DN)
            :rtype counts: float, numpy.ndarray [n] (float)
        """

        # Check arguments
        if not isinstance(peak, (np.ma.MaskedArray, np.integer, int, float,
                                 list, np.ndarray)):
            err_msg = "'peak' has to be int, float, list or numpy.ndarray"
            raise message.NephastError(err_msg)

        if isinstance(peak, (list, np.ndarray)) and not \
                all(isinstance(i, (np.ma.MaskedArray, np.integer, int, float))
                    for i in peak):
            err_msg = "'peak' elements have to be int or float"
            raise message.NephastError(err_msg)

        if not isinstance(peak_norm, (np.ma.MaskedArray, np.integer, int,
                                      float)) and not (peak_norm > 0):
            err_msg = "'peak_norm' has to be positive"
            raise message.NephastError(err_msg)

        # Convert peak values to counts
        if isinstance(peak, list):
            peak = np.array(peak)

        counts = peak / peak_norm

        return counts

    @staticmethod
    def sigmapeak_to_sigmacounts(sigma_peak, peak_norm):
        """Convert peak values STD to counts STD using a normalized PSF peak.

        Parameters:
            :param sigma_peak: Peak values STD of the stars
            :type sigma_peak: int, float, list, numpy.ndarray [n] (int, float)
            :param peak_norm: Peak of a normalized PSF
            :type peak_norm: int, float

        Returns:
            :return sigma_counts: Counts STD of the stars (DN)
            :rtype sigma_counts: float, numpy.ndarray [n] (float)
        """

        # Check arguments
        if not isinstance(sigma_peak, (np.ma.MaskedArray, np.integer, int,
                                       float, list, np.ndarray)):
            err_msg = \
                "'sigma_peak' has to be int, float, list or numpy.ndarray"
            raise message.NephastError(err_msg)

        if isinstance(sigma_peak, (list, np.ndarray)) and not \
                all(isinstance(i, (np.ma.MaskedArray, np.integer, int, float))
                    for i in sigma_peak):
            err_msg = "'sigma_peak' elements have to be int or float"
            raise message.NephastError(err_msg)

        if not isinstance(peak_norm, (np.ma.MaskedArray, np.integer, int,
                                      float)) and not (peak_norm > 0):
            err_msg = "'peak_norm' has to be positive"
            raise message.NephastError(err_msg)

        # Convert peak values STD to counts STD
        if isinstance(sigma_peak, list):
            sigma_peak = np.array(sigma_peak)

        sigma_counts = (1 / peak_norm) * sigma_peak

        return sigma_counts

    def match(self, cat2, dr_tol, dm_tol=None, m_offset=0, verbose=False):
        """Match two catalogs on the same coordinate and magnitude systems. A
        distance tolerance is used. A magnitude difference (absolute) tolerance
        can be used. The second catalog is rearranged to match the first one
        line by line.
        The first difference output is the catalog of stars present in the first
        input but not in the second. The second difference output is the catalog
        of stars present in the second input but not in the first.
        If more than one star in a catalog is matched to a star in the other,
        the closest in position and magnitude is chosen. If no star satisfies
        both conditions, all matches to the star are dropped.

        **********
        This function has been adapted from code in FlyStar, a program by
        Jessica R. Lu at UC Berkeley.
        **********

        Parameters:
            :param cat2: Second catalog to match
            :type cat2: catalog.Catalog
            :param dr_tol: Radius tolerance in units of x and y
            :type dr_tol: int, float
            :param dm_tol: Magnitude tolerance
            :type dm_tol: int, float; optional
            :param m_offset: Magnitude offset applied to second catalog
            :type m_offset: int, float; default 0
            :param verbose: Show process messages
            :type verbose: bool; default False

        Returns:
            :return cat_out1: First catalog after matching
            :rtype cat_out1: catalog.Catalog
            :return cat_out2: Second catalog after matching
            :rtype cat_out2: catalog.Catalog
            :return cat_diff1: First difference catalog
            :rtype cat_diff1: catalog.Catalog
            :return cat_diff1: First difference catalog
            :rtype cat_diff1: catalog.Catalog
            :return dr_match: Distances of matches in units of x and y
            :rtype dr_match: numpy.ndarray [n] (float)
            :return dm_match: Magnitude differences of matches
            :rtype dm_match: numpy.ndarray [n] (float)
        """

        # Check arguments
        if not isinstance(cat2, Catalog):
            err_msg = "'cat2' has to be catalog.Catalog"
            raise message.NephastError(err_msg)

        if not isinstance(dr_tol, (np.ma.MaskedArray, np.integer, int, float)):
            err_msg = "'dr_tol' has to be int or float"
            raise message.NephastError(err_msg)

        if not (dr_tol >= 0):
            err_msg = "'dr_tol' has to be non-negative"
            raise message.NephastError(err_msg)

        if not isinstance(dm_tol, (np.ma.MaskedArray, np.integer, int, float,
                                   type(None))):
            err_msg = "'dm_tol' has to be int, float or None"
            raise message.NephastError(err_msg)

        if dm_tol is not None:

            if not (dm_tol >= 0):
                err_msg = "'dm_tol' has to be non-negative"
                raise message.NephastError(err_msg)

        if not isinstance(m_offset, (np.ma.MaskedArray, np.integer, int,
                                     float)):
            err_msg = "'m_offset' has to be int or float"
            raise message.NephastError(err_msg)

        if not isinstance(verbose, bool):
            err_msg = "'verbose' has to be bool"
            raise message.NephastError(err_msg)

        # Apply magnitude offset
        cat2_offset = copy.deepcopy(cat2)
        cat2_offset['m'] += m_offset

        # Use k-d tree for initial match
        kdt = spatial.cKDTree(np.array([cat2_offset['x'].data,
                                        cat2_offset['y'].data]).T)
        kdt_match = kdt.query_ball_point(np.array([self['x'].data,
                                                   self['y'].data]).T,
                                         dr_tol)
        n_match = [len(i) for i in kdt_match]
        n_match_max = max(n_match)

        # Deal with different numbers of matches
        idxs1 = np.full(self.n, -1)
        idxs2 = np.full(self.n, -1)

        for i_n_match in range(1, (n_match_max + 1)):

            # Find stars with the same number of matches
            idx_n1 = np.where([i == i_n_match for i in n_match])[0]

            if len(idx_n1) > 0:

                # Single match case
                if i_n_match == 1:

                    # Find matches in second catalog for stars in first
                    idx_n2 = np.array([kdt_match[i][0] for i in idx_n1])

                    # If no magnitude tolerance is used, keep all matches
                    if dm_tol is None:
                        idxs1[idx_n1] = idx_n1
                        idxs2[idx_n1] = idx_n2

                    # If magnitude tolerance is used, keep matches that are
                    # within it
                    else:
                        dm = np.abs(self['m'].data[idx_n1] -
                                    cat2_offset['m'].data[idx_n2])
                        keep = np.where(dm < dm_tol)[0]
                        idxs1[idx_n1[keep]] = idx_n1[keep]
                        idxs2[idx_n1[keep]] = idx_n2[keep]

                # Multiple matches case
                else:

                    # Find matches in second catalog for stars in first
                    idx_n2 = [kdt_match[i] for i in idx_n1]

                    # Calculate position and magnitude differences
                    x1 = np.tile(self['x'].data[idx_n1], (i_n_match, 1)).T
                    y1 = np.tile(self['y'].data[idx_n1], (i_n_match, 1)).T
                    m1 = np.tile(self['m'].data[idx_n1], (i_n_match, 1)).T
                    x2 = np.array([cat2_offset['x'].data[i] for i in idx_n2])
                    y2 = np.array([cat2_offset['y'].data[i] for i in idx_n2])
                    m2 = np.array([cat2_offset['m'].data[i] for i in idx_n2])
                    dr = np.hypot((x1 - x2), (y1 - y2))
                    dm = np.abs(m1 - m2)

                    # Choose matches that have both closest distance and
                    # magnitude
                    dr_min_where = np.argmin(dr, axis=1)
                    dm_min_where = np.argmin(dm, axis=1)
                    keep = np.where(dr_min_where == dm_min_where)[0]
                    keep_match = copy.deepcopy(dr_min_where)

                    # Check if matches are within magnitude tolerance
                    if dm_tol is not None:
                        keep = keep[np.where(dm[keep, dr_min_where[keep]] <=
                                             dm_tol)[0]]
                        keep_match = keep_match[keep]

                    # Keep matches
                    idxs1[idx_n1[keep]] = idx_n1[keep]
                    idxs2[idx_n1[keep]] = np.array(idx_n2)[keep, keep_match]

        # Remove unmatched stars
        idxs1 = idxs1[idxs1 >= 0]
        idxs2 = idxs2[idxs2 >= 0]

        # Calculate position and magnitude differences
        dr_match = np.hypot((self['x'].data[idxs1] -
                             cat2_offset['x'].data[idxs2]),
                            (self['y'].data[idxs1] -
                             cat2_offset['y'].data[idxs2]))
        dm_match = self['m'].data[idxs1] - cat2_offset['m'].data[idxs2]

        # Remove duplicates
        idxs2_unique, idxs2_occur = np.unique(idxs2, return_counts=True)
        idxs2_repeat = idxs2_unique[idxs2_occur > 1]

        for i_star2 in idxs2_repeat:

            # Choose matches
            i_star1 = idxs1[np.where(idxs2 == i_star2)[0]]
            n_occur = idxs2_occur[np.where(idxs2_unique == i_star2)[0]][0]

            # Calculate position and magnitude difference
            x1 = self['x'].data[i_star1]
            y1 = self['y'].data[i_star1]
            m1 = self['m'].data[i_star1]
            x2 = np.tile(cat2_offset['x'].data[i_star2], (n_occur, 1)).T
            y2 = np.tile(cat2_offset['y'].data[i_star2], (n_occur, 1)).T
            m2 = np.tile(cat2_offset['m'].data[i_star2], (n_occur, 1)).T
            dr = np.hypot((x1 - x2), (y1 - y2))[0]
            dm = np.abs(m1 - m2)[0]

            # Choose match that has both closest distance and magnitude
            dr_min_where = np.argmin(dr)
            dm_min_where = np.argmin(dm)

            if dr_min_where == dm_min_where:
                rem = np.where(idxs1 == i_star1[~dr_min_where])[0]
            else:
                rem = np.where(np.isin(idxs1, i_star1))[0]

            idxs1 = np.delete(idxs1, rem)
            idxs2 = np.delete(idxs2, rem)

        # Find difference catalogs
        idxs1_diff = np.setdiff1d(np.arange(self.n), idxs1)
        idxs2_diff = np.setdiff1d(np.arange(cat2.n), idxs2)

        # Prepare output catalogs
        cat_out1 = self.select(idxs1)
        cat_out2 = cat2.select(idxs2)
        cat_diff1 = self.select(idxs1_diff)
        cat_diff2 = cat2.select(idxs2_diff)

        return cat_out1, cat_out2, cat_diff1, cat_diff2, dr_match, dm_match

    def print(self, select=None, exp=True, full=False):
        """Print a full catalog of stars or part of it. If more than 11 stars
        are selected, by default print only the first and last 5.

        Parameters:
            :param select: Selection of stars, either single or multiple. 'None'
                uses all stars
            :type select: int, list, numpy.ndarray (int); optional
            :param exp: Use exponential notation
            :type exp: bool; default True
            :param full: Print all stars, not just 11
            :type full: bool; default False
        """

        # Print catalog
        analyze.print_cat(self, select=select, exp=exp, full=full)

    def stack(self, cat2, psf_select=1, original='m'):
        """Stack two catalogs, preserving all columns of both.
        Names in the second catalog that match one in the first are changed to
        'obj_' followed by an integers that is not used.
        Peak values of both are calculated using a common PSF, if one is
        available. If only one of the two catalogs, that one is used. If both
        are used, one of the two can be selected (the first is used by default).

        Parameters:
            :param cat2: Second catalog
            :type cat2: catalog.Catalog
            :param psf_select: Catalog used to select the PSF (1 or 2)
            :type psf_select: int; default 1
            :param original: Original quantity. 'm': magnitude. 'counts':
                counts. 'peak': peak values
            :type original: str; default 'm'
        """

        # Check arguments
        if not isinstance(cat2, Catalog):
            err_msg = "'cat2' has to be catalog.Catalog"
            raise message.NephastError(err_msg)

        if psf_select not in (1, 2):
            err_msg = "'psf_select' has to 1 or 2"
            raise message.NephastError(err_msg)

        if original not in ('m', 'counts', 'peak'):
            err_msg = "'original' has to be 'm', 'counts' or 'peak'"
            raise message.NephastError(err_msg)

        # Change names
        name_same = [i in self['name'] for i in cat2['name']]
        name_idx = np.where(name_same)[0]

        if any(name_same):

            for i_idx in name_idx:
                i_new = 1

                while (('obj_' + str(i_new)) in self['name']) or \
                        (('obj_' + str(i_new)) in cat2['name']):
                    i_new += 1

                cat2['name'][i_idx] = 'obj_' + str(i_new)

        # Choose PSF and tranform intensities
        if ((self.psf is None) and (cat2.psf is not None)) or \
                ((self.psf is not None) and (cat2.psf is not None) and
                 (psf_select == 2)):
            self.psf = cat2.psf
            self.intensity_transf(original)

        elif ((cat2.psf is None) and (self.psf is not None)) or \
                ((cat2.psf is not None) and (self.psf is not None) and
                 (psf_select == 1)):
            cat2.psf = self.psf
            cat2.intensity_transf(original)

        # Stack catalogs
        self.tab = table.vstack([self.tab, cat2.tab], join_type='outer')

    def intensity_transf(self, original):
        """Transform between intensity quantities (instrumental magnitude,
        counts and peak value) and their uncertainties. One quantity is chosen
        to be the original.

        Parameters:
            :param original: Original quantity. 'm': magnitude. 'counts':
                counts. 'peak': peak values
            :type original: str
        """

        # Check arguments
        if not isinstance(original, str):
            err_msg = "'original' has to be str"
            raise message.NephastError(err_msg)

        if original not in ('m', 'counts', 'peak'):
            err_msg = "'original' has to be 'm', 'counts' or 'peak'"
            raise message.NephastError(err_msg)

        if (original is 'peak') and (self.psf is None):
            err_msg = "'psf' has not to be None when 'original' is 'peak'"
            raise message.NephastError(err_msg)

        # Transform intensity from magnitude
        tab_tmp = self.tab

        if original is 'm':
            tab_tmp['counts'] = Catalog.m_to_counts(tab_tmp['m'], m1c=self.m1c)
            tab_tmp['sigma_counts'] = \
                Catalog.sigmam_to_sigmacounts(tab_tmp['m'], tab_tmp['sigma_m'],
                                              m1c=self.m1c)

            if self.psf is not None:
                tab_tmp['peak'] = Catalog.m_to_peak(tab_tmp['m'], self.psf.peak,
                                                    m1c=self.m1c)
                tab_tmp['sigma_peak'] = \
                    Catalog.sigmam_to_sigmapeak(tab_tmp['m'],
                                                tab_tmp['sigma_m'],
                                                self.psf.peak, m1c=self.m1c)

        # Transform intensity from counts
        elif original is 'counts':
            tab_tmp['m'] = Catalog.counts_to_m(tab_tmp['counts'], m1c=self.m1c)
            tab_tmp['sigma_m'] = \
                Catalog.sigmacounts_to_sigmam(tab_tmp['counts'],
                                              tab_tmp['sigma_counts'])

            if self.psf is not None:
                tab_tmp['peak'] = Catalog.counts_to_peak(tab_tmp['counts'],
                                                         self.psf.peak)
                tab_tmp['sigma_peak'] = \
                    Catalog.sigmacounts_to_sigmapeak(tab_tmp['sigma_counts'],
                                                     self.psf.peak)

        # Transform intensity from peak value
        elif original is 'peak':
            tab_tmp['m'] = Catalog.peak_to_m(tab_tmp['peak'], self.psf.peak,
                                             m1c=self.m1c)
            tab_tmp['sigma_m'] = \
                Catalog.sigmapeak_to_sigmam(tab_tmp['peak'],
                                            tab_tmp['sigma_peak'])
            tab_tmp['counts'] = Catalog.peak_to_counts(tab_tmp['peak'],
                                                       self.psf.peak)
            tab_tmp['sigma_counts'] = \
                Catalog.sigmapeak_to_sigmacounts(tab_tmp['sigma_peak'],
                                                 self.psf.peak)

        self.tab = tab_tmp

    def add_col(self, name, col=None, index=None):
        """Add a column.
        If the column is not defined, a masked column of NaNs
        is used. If the position of the column is not specified, it is attached
        to the end. If the input array is masked, so is the column generated.

        Parameters:
            :param name: Column name
            :type name: str
            :param col: Column added
            :type col: list, numpy.ndarray; optional
            :param index: Column position
            :type index: int; optional
        """

        # Check arguments
        if not isinstance(col, (list, np.ndarray, type(None))):
            err_msg = "'col' has to be list or numpy.ndarray"
            raise message.NephastError(err_msg)

        if col is not None:
            if not (len(col) == self.n):
                err_msg = "'col' has to have the same length as 'self.tab'"
                raise message.NephastError(err_msg)

        if not isinstance(name, str):
            err_msg = "'name' has to be str"
            raise message.NephastError(err_msg)

        if name in self.colnames:
            err_msg = "'name' is already used"
            raise message.NephastError(err_msg)

        if not isinstance(index, (np.integer, int, type(None))):
            err_msg = "'index' has to be int"
            raise message.NephastError(err_msg)

        if index is not None:
            if not (index >= 0) and (index <= self.n_col):
                err_msg = \
                    "'index' values have to be within 0 and {0:d}".format(
                        self.n_col)
                raise message.NephastError(err_msg)

        # Add column
        if col is None:
            col = [None] * self.n
            col_mask = [True] * self.n
            col2 = table.MaskedColumn(col, name=name, mask=col_mask)
        else:
            if np.ma.is_masked(col):
                col2 = table.MaskedColumn(col)
            else:
                col2 = table.Column(col)

        tab_tmp = self.tab
        tab_tmp.add_column(col2, name=name, index=index)
        self.tab = tab_tmp

    def plot(self, cat2=None, intens=None, intens2=None, s_min=1, s_max=10,
             intens_lims=None, intens_lims2=None, ratio_eq=True, lims=None):
        """Plot catalog positions, with the option of spot size being a function
        of intensity (magnitude, counts or peak value). A second catalog can be
        plotted over, with spot size option too.
        Spot sizes are either a default constant or they are defined between a
        minimum and maximum value, linear with magnitudes and with logarithm of
        counts and peak values. The minimum spot size can also be used as
        constant spot size.
        The parameter that forces equal ratio overrides the axes limits
        parameter.

        Parameters:
            :param cat2: Second catalog
            :type cat2: catalog.Catalog; optional
            :param intens: Catalog's intensity column ('m', 'counts' or 'peak')
                used for spot size
            :type intens: str; optional
            :param intens2: Second catalog's intensity column ('m', 'counts' or
                'peak') used for spot size
            :type intens2: str; optional
            :param s_min: Minimum spot size. It is used also as constant spot
                size if 'intens' or 'intens2' are None
            :type s_min: int, float; default 1
            :param intens_lims: First catalog's intensity limits for variable
                spot sizes. Outside of them, spots have either the minimum or
                maximum spot size
            :type intens_lims: list [2] (int, float); optional
            :param intens_lims2: Second catalog's intensity limits for variable
                spot sizes. Outside of them, spots have either the minimum or
                maximum spot size
            :type intens_lims2: list [2] (int, float); optional
            :param s_max: Maximum spot size
            :type s_max: int, float; default 10
            :param ratio_eq: Axes with equal ratio
            :type ratio_eq: bool; default True
            :param lims: Axes limits in units of x and y ([minimum x, maximum x,
                minimum y, maximum y])
            :type lims: list [4] (int, float); optional
        """

        # Show plot
        analyze.plot_cat(self, cat2=cat2, intens=intens, intens2=intens2,
                         s_min=s_min, s_max=s_max, intens_lims=intens_lims,
                         intens_lims2=intens_lims2, ratio_eq=ratio_eq,
                         lims=lims)

    def quiver(self, cat2, scale=None, scale_key=None, lims=None, buffer=0,
               ratio_eq=True):
        """Quiver plot of two matched catalogs. The catalogs are outputs of the
        'catalog.match' function.
        The arrows origins are the positions of the first catalog, the arrows
        points are the positions of the second.
        The axes buffer zone is used if the axes limits parameter is defined.

        Parameters:
            :param cat2: Second catalog
            :type cat2: catalog.Catalog
            :param scale: Quiver scale. It is the length of an arrow of one inch
                on the plot. If None, the length of the longest arrow is used.
            :type scale: int, float; optional
            :param scale_key: Quiver key scale. It is the length of the arrow
                key. If None, a reasonable arrow with one significant digit is
                used.
            :type scale_key: int, float; optional
            :param lims: Axes limits in units of x and y ([minimum x, maximum x,
                minimum y, maximum y])
            :type lims: list [4] (int, float); optional
            :param buffer: Axes buffer zone in units of x and y (xy or [x, y])
            :type buffer: int, float, list [2] (int, float); default 0
            :param ratio_eq: Axes with equal ratio
            :type ratio_eq: bool; default True
        """

        # Show plot
        analyze.plot_quiver(self, cat2, scale=scale, scale_key=scale_key,
                            lims=lims, buffer=buffer, ratio_eq=ratio_eq)

    def f_lumin(self, m_bin=None, log=False, m_lims=None, n_lims=None,
                title=None):
        """Histogram plot of the luminosity function of the catalog. The
        magnitude column is used as the luminosity quantity.

        Parameters:
            :param m_bin: Magnitude bin width
            :type m_bin: int, float; optional
            :param log: Logarithmic y axis
            :type log: bool; default False
            :param m_lims: Magnitude limits([minimum m, maximum m])
            :type m_lims: list [2] (int, float); optional
            :param n_lims: Number limits([minimum number, maximum number])
            :type n_lims: list [2] (int, float); optional
            :param title: Title of the figure
            :type title: str; optional
        """

        # Show plot
        analyze.plot_f_lumin(self, m_bin=m_bin, log=log, m_lims=m_lims,
                             n_lims=n_lims, title=title)

    def phast_diff(self, cat2, m_ref=1):
        """Plot the photometric and astrometric difference of two matched
        catalogs. The catalogs are outputs of the 'catalog.match' function.
        The difference is defined as the values of the first catalog minus the
        second.

        Parameters:
            :param cat2: Second catalog
            :type cat2: catalog.Catalog
            :param m_ref: Catalog used for the reference magnitude on the plots'
                x axes
            :type m_ref: int, default 1
        """

        # Show plot
        analyze.plot_phast_diff(self, cat2, m_ref=m_ref)
