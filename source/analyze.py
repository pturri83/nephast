"""NEPHAST by Paolo Turri <pturri83@gmail.com>
Released under BSD License 2.0

Tools for analyzing NEPHAST.
"""


import copy

from astropy import table
from matplotlib import axes, colors, pyplot as plt, ticker
import numpy as np

import catalog
import find
import fit
import image
import message


__all__ = ['show_img', 'plot_history', 'plot_cat', 'plot_quiver', 'plot_2',
           'show_figures', 'print_cat']


def show_img(arr, cat=None, vmin=None, vmax=None, scale='lin', cmap='jet',
             x_offset=0, y_offset=0, unmask=False, title=None, cat_mark_col='k',
             cat_text_col='k', show_name=False):
    """Display an array or image object as an image with some statistics. A
    catalog can be overlaid.

    Parameters:
        :param arr: Image array to display
        :type arr: numpy.ndarray [n, m] (float, int), image.Image
        :param cat: Catalog to display with markersand stars' names on top of
            the image
        :type cat: catalog.Catalog; optional
        :param vmin: Minimum value colored
        :type vmin: int, float; optional
        :param vmax: Maximum value colored
        :type vmax: int, float; optional
        :param scale: Color scale, linear ('lin') or logarithmic ('log')
        :type scale: str; default 'lin'
        :param cmap: Colormap name
        :type cmap: str; default 'jet'
        :param x_offset: Offset of the x axis tick labels
        :type x_offset: int; default 0
        :param y_offset: Offset of the y axis tick labels
        :type y_offset: int; default 0
        :param unmask: Unmask image, if 'arr' is a numpy.ma.MaskedArray instance
        :type unmask: bool; default False
        :param title: Title of the figure
        :type title: str; optional
        :param cat_mark_col: matplotlib color of the catalog marker
        :type cat_mark_col: str; default 'k'
        :param cat_text_col: matplotlib color of the catalog text
        :type cat_text_col: str; default 'k'
        :param show_name: Show stars' names
        :type show_name: bool; default False
    """

    # Check arguments
    if not isinstance(arr, (np.ndarray, image.Image)):
        err_msg = "'arr' has to be numpy.ndarray or image.Image"
        raise message.NephastError(err_msg)

    if isinstance(arr, np.ndarray) and not (len(arr.shape) == 2):
        err_msg = "'arr' has to be a 2D numpy.ndarray"
        raise message.NephastError(err_msg)

    if isinstance(arr, np.ndarray) and not \
        any([np.issubdtype(arr.dtype, np.integer),
             np.issubdtype(arr.dtype, np.floating)]):
        err_msg = "'arr' elements have to be int or float"
        raise message.NephastError(err_msg)

    if not isinstance(cat, (catalog.Catalog, type(None))):
        err_msg = "'cat' has to be catalog.Catalog"
        raise message.NephastError(err_msg)

    if not isinstance(vmin, (np.integer, int, float, type(None))):
        err_msg = "'vmin' has to be int or float"
        raise message.NephastError(err_msg)

    if not isinstance(vmax, (np.integer, int, float, type(None))):
        err_msg = "'vmax' has to be int or float"
        raise message.NephastError(err_msg)

    if scale not in ('lin', 'log'):
        err_msg = "'scal' has to be 'lin' or 'log'"
        raise message.NephastError(err_msg)

    if not isinstance(cmap, str):
        err_msg = "'cmap' has to be str"
        raise message.NephastError(err_msg)

    if not isinstance(x_offset, (np.integer, int)):
        err_msg = "'x_offset' has to be int"
        raise message.NephastError(err_msg)

    if not isinstance(y_offset, (np.integer, int)):
        err_msg = "'y_offset' has to be int"
        raise message.NephastError(err_msg)

    if not isinstance(unmask, bool):
        err_msg = "'unmask' has to be bool"
        raise message.NephastError(err_msg)

    if not isinstance(title, (str, type(None))):
        err_msg = "'title' has to be str"
        raise message.NephastError(err_msg)

    if cat_mark_col not in colors.get_named_colors_mapping():
        err_msg = "'cat_mark_col' has to be a matplotlib color"
        raise message.NephastError(err_msg)

    if cat_text_col not in colors.get_named_colors_mapping():
        err_msg = "'cat_text_col' has to be a matplotlib color"
        raise message.NephastError(err_msg)

    if not isinstance(show_name, bool):
        err_msg = "'show_name' has to be bool"
        raise message.NephastError(err_msg)

    # Extract image array
    if isinstance(arr, image.Image):
        arr = arr.array
        arr_data = arr.data
    else:
        arr_data = arr

    arr_size = arr.shape

    # Image statistics
    arr_min = np.ma.min(arr_data.flatten())
    arr_max = np.ma.max(arr_data.flatten())
    arr_median = np.ma.median(arr_data.flatten())
    arr_std = np.ma.std(arr_data.flatten())

    # Unmask image
    if isinstance(arr, np.ma.MaskedArray) and unmask:
        arr = arr.data

    # Deal with scale
    norm = colors.Normalize
    arr_offset = copy.deepcopy(arr)
    arr_offset_warning = ""

    if scale == 'log':
        norm = colors.LogNorm

        if arr_min <= 0:
            offset = 1 - arr_min
            arr_offset += offset
            arr_offset_warning = "(color offset: {0:.3e} DN)".format(offset)

            if vmin is not None:
                vmin += offset

            if vmax is not None:
                vmax += offset

    # Show image
    fig = plt.figure(figsize=(5, 5.5))
    ax = fig.add_subplot(111)
    ax.set_position([0.15, 0.25, 0.7, 0.7])
    img = plt.imshow(arr_offset, norm=norm(vmin=vmin, vmax=vmax),
                     cmap=plt.get_cmap(cmap), aspect='equal', origin='lower')

    if cat is not None:
        for i_text in range(cat.n):
            if (cat['x', i_text] >= -0.5) and \
                    (cat['x', i_text] <= (arr_size[1] - 0.5)) and \
                    (cat['y', i_text] >= -0.5) and \
                    (cat['y', i_text] <= (arr_size[0] - 0.5)):
                plt.scatter(cat['x'][i_text], cat['y'][i_text], marker='+',
                            s=30, c=cat_mark_col)
                if show_name:
                    plt.annotate(cat['name', i_text],
                                 (cat['x', i_text], cat['y', i_text]),
                                 xytext=(2, 2), textcoords='offset points',
                                 fontsize=8, color=cat_text_col)

    plt.text(0.2, 0.05, "min", ha='left', transform=fig.transFigure,
             fontsize=8)
    plt.text(0.3, 0.05, "=  {0:.3e}".format(arr_min), ha='left',
             transform=fig.transFigure, fontsize=8)
    plt.text(0.2, 0.03, "max", ha='left', transform=fig.transFigure,
             fontsize=8)
    plt.text(0.3, 0.03, "=  {0:.3e}".format(arr_max), ha='left',
             transform=fig.transFigure, fontsize=8)
    plt.text(0.55, 0.05, "median", ha='left', transform=fig.transFigure,
             fontsize=8)
    plt.text(0.65, 0.05, "=  {0:.3e}".format(arr_median), ha='left',
             transform=fig.transFigure, fontsize=8)
    plt.text(0.55, 0.03, "std", ha='left', transform=fig.transFigure,
             fontsize=8)
    plt.text(0.65, 0.03, "=  {0:.3e}".format(arr_std), ha='left',
             transform=fig.transFigure, fontsize=8)
    plt.xlim([-0.5, (arr_size[1] - 0.5)])
    plt.ylim([-0.5, (arr_size[0] - 0.5)])
    ax.set_xlabel("x (px)")
    ax.set_ylabel("y (px)", labelpad=15)
    ax.xaxis.set_major_locator(ticker.MaxNLocator(integer=True))
    ax.yaxis.set_major_locator(ticker.MaxNLocator(integer=True))
    colorbar_ax = fig.add_axes([0.15, 0.15, 0.7, 0.02])
    plt.colorbar(img, cax=colorbar_ax, orientation='horizontal', label="DN")

    if (scale == 'log') and (arr_min <= 0):
        plt.text(0.85, 0.18, arr_offset_warning, ha='right', fontsize=7,
                 transform=fig.transFigure)

    if title:
        fig.suptitle(title)

    # Offset axes labels
    if (x_offset != 0) or (y_offset != 0):
        plt.ion()
        plt.show()
        plt.pause(0.1)
        x_ticks = [int(i.get_text().replace('−', '-')) for i in
                   ax.get_xticklabels()]
        y_ticks = [int(i.get_text().replace('−', '-')) for i in
                   ax.get_yticklabels()]
        ax.set_xticklabels([(i + x_offset) for i in x_ticks])
        ax.set_yticklabels([(i + y_offset) for i in y_ticks])

    plt.pause(0.01)


def plot_history(fitter, star):
    """Plot the fitting history of one star.

    Parameters:
        :param fitter: Fitter containing the fitting history of the star
            analyzed
        :type fitter: fit.Fitter
        :param star: Index or name of the star to plot
        :type star: int, str
    """

    # Check arguments
    if not isinstance(fitter, fit.Fitter):
        err_msg = "'fitter' has to be fit.Fitter"
        raise message.NephastError(err_msg)

    if not hasattr(fitter, 'hist'):
        err_msg = "'fitter' has to have the 'hist' attribute"
        raise message.NephastError(err_msg)

    if not isinstance(star, (np.integer, int, str)):
        err_msg = "'star' has to be int or str"
        raise message.NephastError(err_msg)

    if isinstance(star, (np.integer, int)):

        if (star < 0) or (star >= len(fitter.hist)):
            err_msg = "'star' has to be a value betweem 0 and {0:d}".format(
                len(fitter.hist) - 1)
            raise message.NephastError(err_msg)

    if isinstance(star, str):

        if star not in fitter.hist['name']:
            err_msg = "'star' has to be a star name in the catalog"
            raise message.NephastError(err_msg)

    # Find star
    if isinstance(star, (np.integer, int)):
        star_id = star
    else:
        star_id = np.where(fitter.hist['name'] == star)[0][0]

    # Check if star has a history
    if fitter.hist['iter'][star_id] == 0:
        err_msg = "Star {0} has no history".format(star_id)
        raise message.NephastError(err_msg)

    # Show x plot
    plt.figure(figsize=(6, 6))
    ax1: axes.Axes = plt.subplot(411)
    # HACK: axes' syntax used to avoid a warning caused by matplotlib 3.0.0
    plt.subplots_adjust(left=0.15, right=0.84, hspace=0.1)
    ax1.plot(np.arange(fitter.max_fit + 1), fitter.hist['x'][star_id], 'r-')
    plt.xlim([0, fitter.hist['iter'][star_id]])
    ax1.tick_params(axis='x', labelbottom=False, top=True, direction='in')
    ax1.ticklabel_format(style='plain', useOffset=False)
    ax1.set_ylabel("x (px)", color='r')
    ax1.tick_params(axis='y', which='both', colors='r')
    ax1.spines['left'].set_color('r')
    ax1.xaxis.set_major_locator(ticker.MaxNLocator(integer=True))
    ax1.yaxis.set_label_coords(-0.22, 0.5)
    ax2 = ax1.twinx()
    ax2.semilogy(np.arange(fitter.max_fit + 1),
                 np.abs(fitter.hist['dx'][star_id]), 'b--',
                 label=r"$\left|\Delta\right|$")
    ax2.semilogy([], [], 'b:', label="tol")
    leg = plt.legend(loc='upper right', bbox_to_anchor=(0.015, 1.3, 1., 0.1),
                     ncol=2)
    leg.legendHandles[0].set_color('k')
    leg.legendHandles[1].set_color('k')
    ax2.tick_params(axis='y', which='both', colors='b')
    ax2.spines['right'].set_color('b')
    plt.text(-0.11, 1.2, "Star: '{0}'".format(fitter.hist['name'][star_id]),
             size=15, transform=ax2.transAxes)

    # Show y plot
    ax1 = plt.subplot(412)
    ax1.plot(np.arange(fitter.max_fit + 1), fitter.hist['y'][star_id], 'r-')
    plt.xlim([0, fitter.hist['iter'][star_id]])
    ax1.tick_params(axis='x', labelbottom=False, top=True, direction='in')
    ax1.ticklabel_format(style='plain', useOffset=False)
    ax1.set_ylabel("y (px)", color='r')
    ax1.tick_params(axis='y', which='both', colors='r')
    ax1.spines['left'].set_color('r')
    ax1.xaxis.set_major_locator(ticker.MaxNLocator(integer=True))
    ax1.yaxis.set_label_coords(-0.22, 0.5)
    ax2 = ax1.twinx()
    ax2.semilogy(np.arange(fitter.max_fit + 1),
                 np.abs(fitter.hist['dy'][star_id]), 'b--')
    ax2.tick_params(axis='y', which='both', colors='b')
    ax2.spines['right'].set_color('b')

    # Show counts plot
    ax1 = plt.subplot(413)
    ax1.plot(np.arange(fitter.max_fit + 1), fitter.hist['counts'][star_id],
             'r-')
    ax1.plot(np.arange(fitter.max_fit + 1),
             (fitter.faint_tol * fitter.hist['sigma_counts'][star_id]), 'r:')
    plt.xlim([0, fitter.hist['iter'][star_id]])
    ax1.tick_params(axis='x', labelbottom=False, top=True, direction='in')
    ax1.ticklabel_format(style='plain', useOffset=False)
    ax1.set_ylabel("Counts (DN)", color='r')
    ax1.tick_params(axis='y', which='both', colors='r')
    ax1.spines['left'].set_color('r')
    ax1.xaxis.set_major_locator(ticker.MaxNLocator(integer=True))
    ax1.yaxis.set_label_coords(-0.22, 0.5)
    ax2 = ax1.twinx()
    ax2.semilogy(np.arange(fitter.max_fit + 1),
                 np.abs(fitter.hist['dcounts'][star_id]), 'b--')
    ax2.tick_params(axis='y', which='both', colors='b')
    ax2.spines['right'].set_color('b')

    # Show reduced chi^2 plot
    ax1 = plt.subplot(414)
    ax1.semilogy(np.arange(fitter.max_fit + 1), fitter.hist['chi2r'][star_id],
                 'r-')
    plt.xlim([0, fitter.hist['iter'][star_id]])
    ax1.tick_params(axis='x', top=True, direction='in')
    ax1.set_xlabel("Iteration #", color='k')
    ax1.set_ylabel(r"$\chi^2_r$", color='r')
    ax1.tick_params(axis='y', which='both', colors='r')
    ax1.spines['left'].set_color('r')
    ax1.xaxis.set_major_locator(ticker.MaxNLocator(integer=True))
    ax1.yaxis.set_label_coords(-0.22, 0.5)
    ax2 = ax1.twinx()
    ax2.semilogy(np.arange(fitter.max_fit + 1),
                 np.abs(fitter.hist['dchi2r'][star_id]), 'b--',)
    ax2.semilogy(np.arange(fitter.max_fit + 1),
                 (fitter.chi2r_tol * fitter.hist['chi2r'][star_id]), 'b:')
    ax2.tick_params(axis='y', which='both', colors='b')
    ax2.spines['right'].set_color('b')
    plt.tight_layout(pad=1.2)


def plot_cat(cat, cat2=None, intens=None, intens2=None, s_min=1, s_max=10,
             intens_lims=None, intens_lims2=None, ratio_eq=True, lims=None):
    """Plot catalog positions, with the option of spot size being a function of
    intensity (magnitude, counts or peak value). A second catalog can be plotted
    over, with spot size option too.
    Spot sizes are either a default constant or they are defined between a
    minimum and maximum value, linear with magnitudes and with logarithm of
    counts and peak values. The minimum spot size can also be used as constant
    spot size.
    The parameter that forces equal ratio overrides the axes limits parameter.

    Parameters:
        :param cat: Catalog
        :type cat: catalog.Catalog
        :param cat2: Second catalog
        :type cat2: catalog.Catalog; optional
        :param intens: Catalog's intensity column ('m', 'counts' or 'peak') used
            for spot size
        :type intens: str; optional
        :param intens2: Second catalog's intensity column ('m', 'counts' or
            'peak') used for spot size
        :type intens2: str; optional
        :param s_min: Minimum spot size. It is used also as constant spot size
            if 'intens' or 'intens2' are None
        :type s_min: int, float; default 1
        :param intens_lims: First catalog's intensity limits for variable spot
            sizes. Outside of them, spots have either the minimum or maximum
            spot size
        :type intens_lims: list [2] (int, float); optional
        :param intens_lims2: Second catalog's intensity limits for variable spot
            sizes. Outside of them, spots have either the minimum or maximum
            spot size
        :type intens_lims2: list [2] (int, float); optional
        :param s_max: Maximum spot size
        :type s_max: int, float; default 10
        :param ratio_eq: Axes with equal ratio
        :type ratio_eq: bool; default True
        :param lims: Axes limits in units of x and y ([minimum x, maximum x,
            minimum y, maximum y])
        :type lims: list [4] (int, float); optional
    """

    # Check arguments
    if not isinstance(cat, catalog.Catalog):
        err_msg = "'cat' has to be catalog.Catalog"
        raise message.NephastError(err_msg)

    if cat2 is not None:
        if not isinstance(cat2, catalog.Catalog):
            err_msg = "'cat2' has to be catalog.Catalog"
            raise message.NephastError(err_msg)

    if intens is not None:
        if intens not in ('m', 'counts', 'peak'):
            err_msg = "'intens' has to be 'm', 'counts' or 'peak'"
            raise message.NephastError(err_msg)

        if intens not in cat.tab.colnames:
            err_msg = "'intens' has to be a column in 'cat'"
            raise message.NephastError(err_msg)

    if intens2 is not None:
        if cat2 is None:
            err_msg = "'cat2' has to be defined to use 'intens2'"
            raise message.NephastError(err_msg)

        if intens2 not in ('m', 'counts', 'peak'):
            err_msg = "'intens2' has to be 'm', 'counts' or 'peak'"
            raise message.NephastError(err_msg)

        if intens2 not in cat2.tab.colnames:
            err_msg = "'intens2' has to be a column in 'cat2'"
            raise message.NephastError(err_msg)

    if not isinstance(s_min, (np.integer, int, float)):
        err_msg = "'s_min' has to be int or float"
        raise message.NephastError(err_msg)

    if not isinstance(s_max, (np.integer, int, float)):
        err_msg = "'s_max' has to be int or float"
        raise message.NephastError(err_msg)

    if not isinstance(intens_lims, (list, type(None))):
        err_msg = "'intens_lims' has to be list or None"
        raise message.NephastError(err_msg)

    if intens_lims is not None:
        if intens is None:
            err_msg = "'intens' has to be defined to use 'intens_lims'"
            raise message.NephastError(err_msg)

        if not (len(intens_lims) == 2):
            err_msg = "'intens_lims' has to have two elements"
            raise message.NephastError(err_msg)

        if not all(isinstance(i, (np.integer, int, float)) for i in
                   intens_lims):
            err_msg = "'intens_lims' elements have to be int or float"
            raise message.NephastError(err_msg)

        if not (intens_lims[0] < intens_lims[1]):
            err_msg = "The first element of 'intens_lims' has to be " +\
                      "smaller than the second"
            raise message.NephastError(err_msg)

    if not isinstance(intens_lims2, (list, type(None))):
        err_msg = "'intens_lims2' has to be list or None"
        raise message.NephastError(err_msg)

    if intens_lims2 is not None:
        if cat2 is None:
            err_msg = "'cat2' has to be defined to use 'intens_lims2'"
            raise message.NephastError(err_msg)

        if intens2 is None:
            err_msg = "'intens2' has to be defined to use 'intens_lims2'"
            raise message.NephastError(err_msg)

        if not (len(intens_lims2) == 2):
            err_msg = "'intens_lims2' has to have two elements"
            raise message.NephastError(err_msg)

        if not all(isinstance(i, (np.integer, int, float)) for i in
                   intens_lims2):
            err_msg = "'intens_lims2' elements have to be int or float"
            raise message.NephastError(err_msg)

        if not (intens_lims2[0] < intens_lims2[1]):
            err_msg = "The first element of 'intens_lims2' has to be " + \
                      "smaller than the second"
            raise message.NephastError(err_msg)

    if not isinstance(ratio_eq, bool):
        err_msg = "'ratio_eq' has to be bool"
        raise message.NephastError(err_msg)

    if not isinstance(lims, (list, type(None))):
        err_msg = "'lims' has to be list or None"
        raise message.NephastError(err_msg)

    if lims is not None:
        if not (len(lims) == 4):
            err_msg = "'lims' has to have four elements"
            raise message.NephastError(err_msg)

        if not all(isinstance(i, (np.integer, int, float)) for i in lims):
            err_msg = "'lims' elements have to be int or float"
            raise message.NephastError(err_msg)

        if not (lims[0] < lims[1]):
            err_msg = \
                "The first element of 'lims' has to be smaller than the second"
            raise message.NephastError(err_msg)

        if not (lims[2] < lims[3]):
            err_msg = \
                "The third element of 'lims' has to be smaller than the fourth"
            raise message.NephastError(err_msg)

    # Calculate spot size
    s1 = s_min
    s2 = s_min

    if intens is not None:
        if min(cat[intens]) != max(cat[intens]):
            intens_vals = cat[intens]
            intens_min = min(intens_vals)
            intens_max = max(intens_vals)

            if intens_lims is not None:
                intens_vals[intens_vals < intens_lims[0]] = intens_lims[0]
                intens_vals[intens_vals > intens_lims[1]] = intens_lims[1]
                intens_min = intens_lims[0]
                intens_max = intens_lims[1]

            if intens is 'm':
                s1 = ((intens_vals - intens_max) * (s_max - s_min) /
                      (intens_min - intens_max)) + s_min
            else:
                log_intens = np.log10(intens_vals)
                intens_min = np.log10(intens_min)
                intens_max = np.log10(intens_max)
                s1 = ((log_intens - intens_min) * (s_max - s_min) /
                      (intens_max - intens_min)) + s_min

    if intens2 is not None:
        if min(cat2[intens2]) != max(cat2[intens2]):
            intens_vals2 = cat2[intens2]
            intens_min2 = min(intens_vals2)
            intens_max2 = max(intens_vals2)

            if intens_lims2 is not None:
                intens_vals2[intens_vals2 < intens_lims2[0]] = intens_lims2[0]
                intens_vals2[intens_vals2 > intens_lims2[1]] = intens_lims2[1]
                intens_min2 = intens_lims2[0]
                intens_max2 = intens_lims2[1]

            if intens is 'm':
                s2 = ((intens_vals2 - intens_max2) * (s_max - s_min) /
                      (intens_min2 - intens_max2)) + s_min
            else:
                log_intens2 = np.log10(intens_vals2)
                intens_min2 = np.log10(intens_min2)
                intens_max2 = np.log10(intens_max2)
                s2 = ((log_intens2 - intens_min2) * (s_max - s_min) /
                      (intens_max2 - intens_min2)) + s_min

    # Show plot
    fig = plt.figure(figsize=(5, 5))
    ax = fig.add_subplot(111)
    ax.set_position([0.2, 0.15, 0.7, 0.7])
    plt.scatter(cat['x'], cat['y'], marker='o', c='b', s=s1,
                label="First catalog")

    if cat2 is not None:
        plt.scatter(cat2['x'], cat2['y'], marker='o', c='r', s=s2,
                    label="Second catalog")

    if ratio_eq:
        ax.axis('equal')

    if lims is not None:
        plt.xlim(lims[0], lims[1])
        plt.ylim(lims[2], lims[3])

    ax.xaxis.set_tick_params(top=True)
    ax.yaxis.set_tick_params(right=True)
    ax.set_xlabel("x")
    ax.set_ylabel("y")

    if cat2 is not None:
        lgnd = plt.legend(bbox_to_anchor=(0.5, 1.05), loc=8, ncol=2)

        for handle in lgnd.legendHandles:
            handle.set_sizes([15])


def plot_quiver(cat1, cat2, scale=None, scale_key=None, lims=None, buffer=0,
                ratio_eq=True):
    """Quiver plot of two matched catalogs. The catalogs are outputs of the
    'catalog.match' function.
    The arrows origins are the positions of the first catalog, the arrows points
    are the positions of the second.
    The axes buffer zone is used if the axes limits parameter is defined.

    Parameters:
        :param cat1: First catalog
        :type cat1: catalog.Catalog
        :param cat2: Second catalog
        :type cat2: catalog.Catalog
        :param scale: Quiver scale. It is the length of an arrow of half inch on
            the plot. If None, the length of the longest arrow is used.
        :type scale: int, float; optional
        :param scale_key: Quiver key scale. It is the length of the arrow key.
            If None, a reasonable arrow with one significant digit is used.
        :type scale_key: int, float; optional
        :param lims: Axes limits in units of x and y ([minimum x, maximum x,
            minimum y, maximum y])
        :type lims: list [4] (int, float); optional
        :param buffer: Axes buffer zone in units of x and y (xy or [x, y])
        :type buffer: int, float, list [2] (int, float); default 0
        :param ratio_eq: Axes with equal ratio
        :type ratio_eq: bool; default True
    """

    # Check arguments
    if not isinstance(cat1, catalog.Catalog):
        err_msg = "'cat1' has to be catalog.Catalog"
        raise message.NephastError(err_msg)

    if not isinstance(cat2, catalog.Catalog):
        err_msg = "'cat2' has to be catalog.Catalog"
        raise message.NephastError(err_msg)

    if not (cat1.n == cat2.n):
        err_msg = "'cat1' and 'cat2' have to be matched catalogs"
        raise message.NephastError(err_msg)

    if not isinstance(scale, (np.integer, int, float, type(None))):
        err_msg = "'lims' has to be list"
        raise message.NephastError(err_msg)

    if isinstance(scale, (np.integer, int, float)) and not (scale > 0):
        err_msg = "'scale' has to be positive"
        raise message.NephastError(err_msg)

    if not isinstance(scale_key, (np.integer, int, float, type(None))):
        err_msg = "'lims' has to be list"
        raise message.NephastError(err_msg)

    if isinstance(scale_key, (np.integer, int, float)) and not (scale_key > 0):
        err_msg = "'scale_key' has to be positive"
        raise message.NephastError(err_msg)

    if not isinstance(lims, (list, type(None))):
        err_msg = "'lims' has to be list or None"
        raise message.NephastError(err_msg)

    if lims is not None:
        if not isinstance(lims, list):
            err_msg = "'lims' has to be list"
            raise message.NephastError(err_msg)

        if not (len(lims) == 4):
            err_msg = "'lims' has to have four elements"
            raise message.NephastError(err_msg)

        if not all(isinstance(i, (np.integer, int, float)) for i in lims):
            err_msg = "'lims' elements have to be int or float"
            raise message.NephastError(err_msg)

        if not (lims[0] < lims[1]):
            err_msg = \
                "The first element of 'lims' has to be smaller than the second"
            raise message.NephastError(err_msg)

        if not (lims[2] < lims[3]):
            err_msg = \
                "The third element of 'lims' has to be smaller than the fourth"
            raise message.NephastError(err_msg)

    if isinstance(buffer, list):
        if not (len(buffer) == 2):
            err_msg = "'buffer' has to have two elements"
            raise message.NephastError(err_msg)

        if not all(isinstance(i, (np.integer, int, float)) for i in buffer):
            err_msg = "'buffer' elements have to be int or float"
            raise message.NephastError(err_msg)
    else:
        if not isinstance(buffer, (np.integer, int, float)):
            err_msg = "'buffer' has to be int or float"
            raise message.NephastError(err_msg)

    if not isinstance(ratio_eq, bool):
        err_msg = "'ratio_eq' has to be bool"
        raise message.NephastError(err_msg)

    # Calculate scale
    if scale is not None:
        quiver_scale = scale
    else:
        quiver_scale = np.max(np.hypot((cat2['x'] - cat1['x']),
                                       (cat2['y'] - cat1['y']))) * 2

    if scale_key is not None:
        quiver_key = scale_key
    else:
        quiver_key = quiver_scale
        quiver_key = round(quiver_key, -int(np.floor(np.log10(quiver_key))))

    # Show plot
    fig = plt.figure(figsize=(5, 5))
    ax = fig.add_subplot(111)
    ax.set_position([0.15, 0.24, 0.7, 0.6])
    quiv = plt.quiver(cat1['x'], cat1['y'], (cat2['x'] - cat1['x']),
                      (cat2['y'] - cat1['y']), width=0.005, units='width',
                      scale=quiver_scale, scale_units='inches')
    plt.quiverkey(quiv, 0.4, 0.93, quiver_key, str(quiver_key), labelpos='W',
                  coordinates='figure', fontproperties={'size': 15})

    if lims is not None:
        if not isinstance(buffer, list):
            buffer = [buffer] * 2

        plt.axis([(lims[0] - buffer[0]), (lims[1] + buffer[0]),
                  (lims[2] - buffer[1]), (lims[3] + buffer[1])])

    if ratio_eq:
        plt.gca().set_aspect('equal', adjustable='box')

    ax.xaxis.set_tick_params(top=True)
    ax.yaxis.set_tick_params(right=True)
    ax.set_xlabel("x")
    ax.set_ylabel("y")


def plot_2(data1, data2, lims=None, labels=None, dot_s=3):
    """Make a 2D scatter plot.

    Parameters:
        :param data1: x axis data
        :type data1: list, numpy.ndarray, astropy.table.column.Column [n] (int,
            float)
        :param data2: y axis data
        :type data2: list, numpy.ndarray, astropy.table.column.Column [n] (int,
            float)
        :param lims: Axes limits in units of x and y ([minimum x, maximum x,
            minimum y, maximum y])
        :type lims: list [4] (int, float); optional
        :param labels: Axes' labels
        :type labels: list [n] (str); optional
        :param dot_s: Dots size (pt^2)
        :type dot_s: int; default 3
    """

    # Check arguments
    if not isinstance(data1, (list, np.ndarray, table.column.Column)):
        err_msg = "'data1' has to be list, numpy.ndarray or " + \
                  "astropy.table.column.Column"
        raise message.NephastError(err_msg)

    if not all((isinstance(i, (np.integer, int, float)) or (i is np.ma.masked))
               for i in data1):
        err_msg = "'data1' elements have to be int or float"
        raise message.NephastError(err_msg)

    if not isinstance(data2, (list, np.ndarray, table.column.Column)):
        err_msg = "'data2' has to be list, numpy.ndarray or " + \
                  "astropy.table.column.Column"
        raise message.NephastError(err_msg)

    if not (len(data2) == len(data1)):
        err_msg = "'data2' has to have the same length as 'data1'"
        raise message.NephastError(err_msg)

    if not all((isinstance(i, (np.integer, int, float)) or (i is np.ma.masked))
               for i in data2):
        err_msg = "'data2' elements have to be int or float"
        raise message.NephastError(err_msg)

    if not isinstance(lims, (list, type(None))):
        err_msg = "'lims' has to be list or None"
        raise message.NephastError(err_msg)

    if lims is not None:
        if not len(lims) == 4:
            err_msg = "'lims' has to have four elements"
            raise message.NephastError(err_msg)

        if not all(isinstance(i, (np.integer, int, float)) for i in lims):
            err_msg = "'lims' elements have to be int or float"
            raise message.NephastError(err_msg)

        if not lims[0] < lims[1]:
            err_msg = \
                "The first element of 'lims' has to be smaller than the second"
            raise message.NephastError(err_msg)

        if not lims[2] < lims[3]:
            err_msg = \
                "The third element of 'lims' has to be smaller than the fourth"
            raise message.NephastError(err_msg)

    if labels is not None:
        if not len(labels) == 2:
            err_msg = "'labels' has to have two elements"
            raise message.NephastError(err_msg)

        if not all(isinstance(i, str) for i in labels):
            err_msg = "'labels' elements have to be str"
            raise message.NephastError(err_msg)

    if not isinstance(dot_s, int):
        err_msg = "'dot_s' has to be int"
        raise message.NephastError(err_msg)

    if not dot_s > 0:
        err_msg = "'dot_s' has to be positive"
        raise message.NephastError(err_msg)

    # Show plot
    fig = plt.figure(figsize=(5, 5))
    ax = fig.add_subplot(111)
    ax.set_position([0.2, 0.24, 0.7, 0.6])
    plt.scatter(data1, data2, c='k', s=dot_s)

    if lims is not None:
        plt.xlim(lims[0], lims[1])
        plt.ylim(lims[2], lims[3])

    if labels is not None:
        ax.set_xlabel(labels[0])
        ax.set_ylabel(labels[1])


def show_figures():
    """Show all figures generated so far.
    """

    # Show figures
    plt.show()


def print_cat(cat, select=None, exp=True, full=False):
    """Print a catalog of stars or part of it. If more than 11 stars are
    selected, by default print only the first and last 5.

    Parameters:
        :param cat: Catalog to print
        :type cat: catalog.Catalog
        :param select: Selection of stars, either single or multiple. 'None'
            uses all stars
        :type select: int, list, numpy.ndarray (int); optional
        :param exp: Use exponential notation
        :type exp: bool; default True
        :param full: Print all stars, not just 11
        :type full: bool; default False
        """

    # Check arguments
    if not isinstance(cat, catalog.Catalog):
        err_msg = "'cat' has to be catalog.Catalog"
        raise message.NephastError(err_msg)

    if select is not None:
        if not isinstance(select, (np.ma.MaskedArray, np.integer, int, list,
                                   np.ndarray)):
            err_msg = "'select' has to be int, list, numpy.ndarray or None"
            raise message.NephastError(err_msg)

        if isinstance(select, (np.ma.MaskedArray, np.integer, int)):
            if (select < 0) or (select >= cat.n):
                err_msg = \
                    "'select' has to be between 0 and {0:d}".format(cat.n - 1)
                raise message.NephastError(err_msg)

        else:
            if not all(isinstance(i, (np.ma.MaskedArray, np.integer, int)) for i
                       in select):
                err_msg = "'select' elements have to be int"
                raise message.NephastError(err_msg)

            if any((i < 0) or (i >= cat.n) for i in select):
                err_msg = \
                    "all 'select' elements have to be between 0 and " + \
                    "{0:d}".format(cat.n - 1)
                raise message.NephastError(err_msg)

    if not isinstance(exp, bool):
        err_msg = "'exp' has to be bool"
        raise message.NephastError(err_msg)

    if not isinstance(full, bool):
        err_msg = "'full' has to be bool"
        raise message.NephastError(err_msg)

    # Select stars
    if isinstance(select, (np.ma.MaskedArray, np.integer, int)):
        select = [select]

    if select is None:
        select = np.arange(cat.n)

    dots = False

    if (len(select) > 11) and not full:
        dots = True
        select = np.concatenate((select[: 5], select[-5:]))

    # Select columns
    ok_col = []

    for i_col in cat.tab.colnames:
        if not ((cat[i_col].dtype.char == 'U') and (i_col is "name")):
            if np.ma.count(cat[i_col][select]) > 0:
                ok_col.append(i_col)

    # Print header
    col_width = 12
    header_format = " {0:>" + str(col_width) + "s}"
    header = "\n"
    header += "  {0:s}  ".format("index")

    if np.ma.count(cat['name', select]) > 0:
        header += "     {0:<14s}".format("name")

    for i_col in ok_col:
        header += header_format.format(i_col)

    len_dash = len(header) - 11
    print(header)
    print(("-" * 9) + "   " + ("-" * len_dash))

    # Print catalog
    for i_star in select:
        if dots:
            if i_star == select[5]:
                print("         ...")

        row = " {0:>6d}".format(i_star)

        if np.ma.count(cat['name', select]) > 0:
            row += "       {0:<14s}".format(cat['name', i_star])

        col_format = ""

        for i_col in ok_col:
            col_dtype = cat[i_col].dtype.char
            if (col_dtype == 'i') or (col_dtype == 'l'):
                col_format = "d"
            elif col_dtype == 'd':
                if exp:
                    col_format = ".5e"
                else:
                    col_format = ".3f"
            elif col_dtype == 'U':
                col_format = "s"

            row += (" {0:>" + str(col_width) + col_format +
                    "}").format(cat[i_col, i_star])

        print(row)

    print(("-" * 9) + "   " + ("-" * len_dash) + "\n")


def plot_find(finder, sharp_lim=None, sharp_percent=None):
    """Plot found stars.
    Sharpness limit values have precedence over the limit percentiles.

    Parameters:
        :param finder: Finder containing the found stars
        :type finder: find.Finder
        :param sharp_lim: Sharpness limit values (minimum, maximum)
        :type sharp_lim: list [2] (int, float); optional
        :param sharp_percent: Sharpness limit percentiles (minimum, maximum)
        :type sharp_percent: list [2] (int, float); optional
    """

    # Check arguments
    if not isinstance(finder, find.Finder):
        err_msg = "'fitter' has to be fit.Fitter"
        raise message.NephastError(err_msg)

    if sharp_lim is not None:
        if not (len(sharp_lim) == 2):
            err_msg = "'sharp_lim' has to have two elements"
            raise message.NephastError(err_msg)

        if not all(isinstance(i, (np.integer, int, float)) for i in sharp_lim):
            err_msg = "'sharp_lim' elements have to be int or float"
            raise message.NephastError(err_msg)

        if not (sharp_lim[0] < sharp_lim[1]):
            err_msg = "The first element of 'sharp_lim' has to be smaller " + \
                "than the second"
            raise message.NephastError(err_msg)

    if sharp_percent is not None:
        if not (len(sharp_percent) == 2):
            err_msg = "'sharp_percent' has to have two elements"
            raise message.NephastError(err_msg)

        if not all(isinstance(i, (np.integer, int, float)) for i in
                   sharp_percent):
            err_msg = "'sharp_percent' elements have to be int or float"
            raise message.NephastError(err_msg)

        if not (sharp_percent[0] < sharp_percent[1]):
            err_msg = "The first element of 'sharp_percent' has to be " + \
                      "smaller than the second"
            raise message.NephastError(err_msg)

    # Show noise plot
    plt.figure(figsize=(6, 6))
    plt.subplot(211)
    plt.subplots_adjust(left=0.15, right=0.84, hspace=0.35)
    plt.scatter(finder.cat_init['find_counts'], finder.cat_init['find_bkg'],
                c='r', s=0.1, label='candidates')
    plt.scatter(finder.cat['find_counts'], finder.cat['find_bkg'], c='b', s=0.1,
                label='found')
    plt.xscale('log')
    xlim = np.array(plt.xlim())
    ylim = np.array(plt.ylim())
    plt.plot((ylim * finder.find_std), ylim, 'k--')
    ax = plt.gca()
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    plt.xlabel("Counts")
    plt.ylabel("Counts noise")
    lgnd = plt.legend(bbox_to_anchor=(0.5, 1.05), loc=8, ncol=2)

    for handle in lgnd.legendHandles:
        handle.set_sizes([15])

    # Show sharpness plot
    plt.subplot(212)
    plt.scatter(finder.cat_init['m'], finder.cat_init['sharp'], c='r', s=0.1)
    plt.scatter(finder.cat['m'], finder.cat['sharp'], c='b', s=0.1)
    xlim = np.array(plt.xlim())
    plt.plot(xlim, ([finder.sharp[0]] * 2), 'k--')
    plt.plot(xlim, ([finder.sharp[1]] * 2), 'k--')
    ax = plt.gca()
    ylim = ax.get_ylim()

    if sharp_lim is not None:
        ylim = sharp_lim
    elif sharp_percent is not None:
        ylim = np.percentile(finder.cat['sharp'], sharp_percent)

    ax.set_ylim(ylim)
    plt.xlabel("m")
    plt.ylabel("Sharpness")


def plot_f_lumin(cat, m_bin=None, log=False, m_lims=None, n_lims=None,
                 title=None):
    """Histogram plot of the luminosity function of a catalog. The magnitude
        column is used as the luminosity quantity.

        Parameters:
            :param cat: Catalog to plot
            :type cat: catalog.Catalog
            :param m_bin: Magnitude bin width
            :type m_bin: int, float; optional
            :param log: Logarithmic y axis
            :type log: bool; default False
            :param m_lims: Magnitude limits([minimum m, maximum m])
            :type m_lims: list [2] (int, float); optional
            :param n_lims: Number limits([minimum number, maximum number])
            :type n_lims: list [2] (int); optional
            :param title: Title of the figure
            :type title: str; optional
    """

    # Check arguments
    if not isinstance(cat, catalog.Catalog):
        err_msg = "'cat' has to be catalog.Catalog"
        raise message.NephastError(err_msg)

    if not isinstance(m_bin, (np.integer, int, float, type(None))):
        err_msg = "'m_bin' has to be int or float"
        raise message.NephastError(err_msg)

    if m_bin is not None:
        if not m_bin > 0:
            err_msg = "'m_bin' has to be positive"
            raise message.NephastError(err_msg)

    if not isinstance(log, bool):
        err_msg = "'log' has to be bool"
        raise message.NephastError(err_msg)

    if not isinstance(m_lims, (list, type(None))):
        err_msg = "'m_lims' has to be list or None"
        raise message.NephastError(err_msg)

    if m_lims is not None:
        if not len(m_lims) == 2:
            err_msg = "'m_lims' has to have two elements"
            raise message.NephastError(err_msg)

        if not all(isinstance(i, (np.integer, int, float)) for i in m_lims):
            err_msg = "'m_lims' elements have to be int or float"
            raise message.NephastError(err_msg)

        if not m_lims[0] < m_lims[1]:
            err_msg = ("The first element of 'm_lims' has to be smaller than " +
                       "the second")
            raise message.NephastError(err_msg)

    if not isinstance(n_lims, (list, type(None))):
        err_msg = "'n_lims' has to be list or None"
        raise message.NephastError(err_msg)

    if n_lims is not None:
        if not len(n_lims) == 2:
            err_msg = "'n_lims' has to have two elements"
            raise message.NephastError(err_msg)

        if not all(isinstance(i, (np.integer, int)) for i in n_lims):
            err_msg = "'n_lims' elements have to be int or float"
            raise message.NephastError(err_msg)

        if not n_lims[0] < n_lims[1]:
            err_msg = ("The first element of 'n_lims' has to be smaller than " +
                       "the second")
            raise message.NephastError(err_msg)

    if not isinstance(title, (str, type(None))):
        err_msg = "'title' has to be str"
        raise message.NephastError(err_msg)

    # Calculate bins
    if m_bin is not None:
        m_min = np.floor(np.min(cat['m']))
        m_max = np.ceil(np.max(cat['m']))
        ms = np.arange(m_min, m_max, m_bin)
    else:
        ms = None

    # Show histogram
    fig = plt.figure(figsize=(5, 5))
    ax = fig.add_subplot(111)
    ax.set_position([0.2, 0.24, 0.7, 0.6])
    plt.hist(cat['m'], bins=ms, log=log)

    if m_lims is not None:
        plt.xlim(m_lims[0], m_lims[1])

    if n_lims is not None:
        plt.ylim(n_lims[0], n_lims[1])

    ax.set_xlabel("m")
    ax.set_ylabel("N")

    if title:
        fig.suptitle(title)


def plot_phast_diff(cat1, cat2, m_ref=1):
    """Plot the photometric and astrometric difference of two matched catalogs.
    The catalogs are outputs of the 'catalog.match' function.
    The difference is defined as the values of the first catalog minus the
    second.

    Parameters:
        :param cat1: First catalog
        :type cat1: catalog.Catalog
        :param cat2: Second catalog
        :type cat2: catalog.Catalog
        :param m_ref: Catalog used for the reference magnitude on the plots' x
            axes
        :type m_ref: int, default 1
    """

    # Check arguments
    if not isinstance(cat1, catalog.Catalog):
        err_msg = "'cat1' has to be catalog.Catalog"
        raise message.NephastError(err_msg)

    if not isinstance(cat2, catalog.Catalog):
        err_msg = "'cat2' has to be catalog.Catalog"
        raise message.NephastError(err_msg)

    if not (cat1.n == cat2.n):
        err_msg = "'cat1' and 'cat2' have to be matched catalogs"
        raise message.NephastError(err_msg)

    if not isinstance(m_ref, (int, np.int)):
        err_msg = "'m_ref' has to be int"
        raise message.NephastError(err_msg)

    if not (m_ref in [1, 2]):
        err_msg = "'m_ref' has to be 1 or 2"
        raise message.NephastError(err_msg)

    # Calculate differencies
    m_diff = cat1['m'] - cat2['m']
    x_diff = cat1['x'] - cat2['x']
    y_diff = cat1['y'] - cat2['y']
    r_diff = np.hypot(x_diff, y_diff)

    if m_ref == 1:
        m = cat1['m']
    else:
        m = cat2['m']

    # Show photometric difference plot
    plt.figure(figsize=(6, 6))
    plt.subplot(411)
    plt.subplots_adjust(left=0.15, right=0.84, hspace=0.1)
    plt.scatter(m, m_diff, c='k', s=0.1)
    ax1 = plt.gca()
    ax1.tick_params(axis='x', labelbottom=False, top=True, direction='in')
    plt.ylabel(r"$\Delta$m")

    # Show astrometric difference plot
    plt.subplot(412)
    plt.scatter(m, r_diff, c='k', s=0.1)
    ax2 = plt.gca()
    ax2.tick_params(axis='x', labelbottom=False, top=True, direction='in')
    plt.ylabel(r"$\Delta$r")

    # Show astrometric (x) difference plot
    plt.subplot(413)
    plt.scatter(m, x_diff, c='k', s=0.1)
    ax3 = plt.gca()
    ax3.tick_params(axis='x', labelbottom=False, top=True, direction='in')
    plt.ylabel(r"$\Delta$x")

    # Show astrometric (y) difference plot
    plt.subplot(414)
    plt.scatter(m, y_diff, c='k', s=0.1)
    ax4 = plt.gca()
    ax4.tick_params(axis='x', top=True, direction='in')
    plt.xlabel("m")
    plt.ylabel(r"$\Delta$y")
    plt.tight_layout(pad=1.2)
