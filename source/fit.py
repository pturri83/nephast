"""NEPHAST by Paolo Turri <pturri83@gmail.com>
Released under BSD License 2.0

This module deals with fitting profiles.
"""


import copy

from astropy import table
import numpy as np

import analyze
import catalog
import config
import image
import instrument
import message
import object
import observation
import psf


__all__ = ['Fitter']


class Fitter(object.Object):
    """Fitter object to perform astrometric and photometric non-linear least
    square fitting.
    It includes a guess catalog for the first iteration, the image to be fitted
    and the PSF to be used.
    """

    def __init__(self, cat_in, imag, psfun, inst, obs, conf=None):
        """Initialize the fitter by providing a catalog of initial guesses,
        image and PSF model.

        Parameters:
            :param cat_in: Catalog of initial guesses
            :type cat_in: catalog.Catalog
            :param imag: Image fitted
            :type imag: image.Image
            :param psfun: Point spread function fitted
            :type psfun: psf.Psf
            :param inst: Detector
            :type inst: instrument.Instrument
            :param obs: Observation
            :type obs: observation.Observation
            :param conf: Configuration
            :type conf: config.Config; optional

        # Attributes:
            :attr cat_in: Catalog of initial guesses
            :atype cat_in: catalog.Catalog
            :attr image: Image fitted
            :atype image: image.Image
            :attr psf: Point spread function fitted
            :atype psf: psf.Psf
            :attr inst: Detector
            :atype inst: instrument.Instrument
            :attr obs: Observation
            :atype obs: observation.Observation
            :attr cat_out: Catalog after fitting
            :atype cat_out: catalog.Catalog or None
                :col idx_in: Index in 'cat_in'
                :ctype idx_in: int
            :attr cat_drop: Catalog of stars dropped from fitting
            :atype cat_drop: catalog.Catalog or None
                :col idx_in: Index in 'cat_in'
                :ctype idx_in: int
                :col drop: Reason for dropping. 'out': outside the image.
                    'npix': not enough good pixels. 'conv': not converged.
                    'near': another brighter star is too close. 'faint': too
                    faint
                :ctype drop: str
            :attr image_model: Model image after fitting
            :atype image_model: image.Image
            :attr image_sub: Subtracted image after fitting
            :atype image_sub: image.Image
            :attr min_fit: Minimum number of fitting iterations
            :atype min_fit: int
            :attr max_fit: Maximum number of fitting iterations
            :atype max_fit: int
            :attr iter_sub: Initial fitting iterations that do not use star
                subtraction. Use a value larger than 'max_fit' to disable star
                subtraction. It has to be smaller than or equal to 'iter_sigma'.
            :atype iter_sub: int
            :attr iter_sigma: Initial fitting iterations that do not use star
                sigma-clipping. Use a value larger than 'max_fit' to disable
                star subtraction. It has to be greater than or equal to
                'iter_sub'.
            :atype iter_sigma: int
            :attr fit_radius: Radius over which the PSF is fitted (px). Pixels'
                centers have to be within it. It has to be smaller than or equal
                to 'psf_radius'
            :atype fit_radius: float
            :attr bkg_radius_in: Inner radius over which the background is
                calculated with a median when 'fit_nbkg' is 0
            :atype bkg_radius_in: float
            :attr sigma_clip: Number of sigmas for counts of residual to remove
                pixels from fitting
            :atype sigma_clip: float; optional
            :attr fit_npx: Minimum number of good pixels that an object must
                have for fitting, or it is dropped. The number of fitting
                parameters is the lower limit
            :atype fit_npx: int
            :attr fit_noise: Method used to calculate the noise in assigning
                weights during the fitting. Available options are 'data'
                (variance equals the image) and 'model' (variance equals the
                model image of stars and background plus read noise)
            :atype fit_noise: str
            :attr fit_nbkg: Number of parameters used to fit the background. 0:
                background not fitted. 1: flat background. 3: background with
                gradient
            :atype fit_nbkg: int
            :attr fit_npar: Number of parameters used to fit the PSF
            :atype fit_npar: int
            :attr fit_gain: Initial fitting gain
            :atype fit_gain: float
            :attr fit_max: Maximum relative step size in counts of a fitting
                iteration
            :atype fit_max: float
            :attr iter_gain: Number of iterations after 'min_fit' before halving
                the fitting gain
            :atype iter_gain: int
            :attr chi2r_tol: Fraction of reduced chi^2 to converge to
            :atype chi2r_tol: float
            :attr near_tol: Distance between stars in FWHM to consider them too
                close
            :atype near_tol: float
            :attr faint_tol: Number of sigmas for counts when stars are
                considered too faint
            :atype faint_tol: float
            :attr hist: Table with history of fitting parameters
            :atype hist: astropy.table.Table [n]
                :col name: Names of the stars
                :ctype name: str
                :col x: x coordinate of the stars (px)
                :ctype x: float
                :col sigma_x: x coordinate STD of the stars (px)
                :ctype sigma_x: float
                :col dx: Change in x coordinate of the stars (px)
                :ctype dx: float
                :col y: y coordinate of the stars (px)
                :ctype y: float
                :col sigma_y: y coordinate STD of the stars (px)
                :ctype sigma_y: float
                :col dy: Change in y coordinate of the stars (px)
                :ctype dy: float
                :col counts: Counts of the stars (DN)
                :ctype counts: float
                :col sigma_counts: Counts STD of the stars (DN)
                :ctype sigma_counts: float
                :col dcounts: Change in counts of the stars (DN)
                :ctype dcounts: float
                :col bkgc: Background constant intensities of the stars (DN/px)
                :ctype bkgc: float
                :col sigma_bkgc: Background constant intensities STD of the
                    stars (DN/px)
                :ctype sigma_bkgc: float
                :col dbkgc: Change in background constant intensities of the
                    stars (DN/px)
                :ctype dbkgc: float
                :col bkgx: Background x gradients of the stars (DN/px/px)
                :ctype bkgx: float
                :col sigma_bkgx: Background x gradients STD of the stars
                    (DN/px/px)
                :ctype sigma_bkgx: float
                :col dbkgx: Change in background x gradients of the stars
                    (DN/px/px)
                :ctype dbkgx: float
                :col bkgy: Background y gradients of the stars (DN/px/px)
                :ctype bkgy: float
                :col sigma_bkgy: Background y gradients STD of the stars
                    (DN/px/px)
                :ctype sigma_bkgy: float
                :col dbkgy: Change in background y gradients of the stars
                    (DN/px/px)
                :ctype dbkgy: float
                :col chi2r: Reduced chi^2
                :ctype chi2r: float
                :col dchi2r: Change in reduced chi^2
                :ctype dchi2r: float
        """

        # Check arguments
        if not isinstance(conf, (config.Config, type(None))):
            err_msg = "'conf' has to be config.Config"
            raise message.NephastError(err_msg)

        # Define configuration
        if conf is None:
            conf = config.Config()

        # Store attributes
        self.cat_in = cat_in
        self.image = imag
        self.psf = psfun
        self.inst = inst
        self.obs = obs
        self.cat_out = None
        self.cat_drop = None
        self.image_model = None
        self.image_sub = None
        self.min_fit = conf.min_fit
        self.max_fit = conf.max_fit
        self.iter_sub = conf.iter_sub
        self.iter_sigma = conf.iter_sigma
        self.fit_radius = conf.fit_radius
        self.bkg_radius_in = conf.bkg_radius_in
        self.sigma_clip = conf.sigma_clip
        self.fit_npx = conf.fit_npx
        self.fit_noise = conf.fit_noise
        self.fit_nbkg = conf.fit_nbkg
        self.fit_npar = self.fit_nbkg + 3
        self.fit_gain = conf.fit_gain
        self.fit_max = conf.fit_max
        self.iter_gain = conf.iter_gain
        self.chi2r_tol = conf.chi2r_tol
        self.near_tol = conf.near_tol
        self.faint_tol = conf.faint_tol
        self.hist = table.Table([self.cat_in['name'],
                                 ([[None] * (self.max_fit + 1)] *
                                  self.cat_in.n),
                                 ([[None] * (self.max_fit + 1)] *
                                  self.cat_in.n),
                                 ([[None] * (self.max_fit + 1)] *
                                  self.cat_in.n),
                                 ([[None] * (self.max_fit + 1)] *
                                  self.cat_in.n),
                                 ([[None] * (self.max_fit + 1)] *
                                  self.cat_in.n),
                                 ([[None] * (self.max_fit + 1)] *
                                  self.cat_in.n),
                                 ([[None] * (self.max_fit + 1)] *
                                  self.cat_in.n),
                                 ([[None] * (self.max_fit + 1)] *
                                  self.cat_in.n),
                                 ([[None] * (self.max_fit + 1)] *
                                  self.cat_in.n),
                                 ([[None] * (self.max_fit + 1)] *
                                  self.cat_in.n),
                                 ([[None] * (self.max_fit + 1)] *
                                  self.cat_in.n), ([0] * self.cat_in.n)],
                                names=('name', 'x', 'sigma_x', 'dx', 'y',
                                       'sigma_y', 'dy', 'counts',
                                       'sigma_counts', 'dcounts', 'chi2r',
                                       'dchi2r', 'iter'),
                                dtype=(np.str, np.float, np.float, np.float,
                                       np.float, np.float, np.float, np.float,
                                       np.float, np.float, np.float, np.float,
                                       np.int))
        self.hist['name'] = self.hist['name'].astype('U20')
        self.hist.add_columns([table.Column(([[None] * (self.max_fit + 1)] *
                                             self.cat_in.n), dtype=np.float),
                               table.Column(([[None] * (self.max_fit + 1)] *
                                             self.cat_in.n), dtype=np.float),
                               table.Column(([[None] * (self.max_fit + 1)] *
                                             self.cat_in.n), dtype=np.float)],
                              names=['bkgc', 'sigma_bkgc', 'dbkgc'],
                              indexes=[10, 10, 10])

        if self.fit_nbkg > 1:
            self.hist.add_columns([table.Column(([[None] * (self.max_fit + 1)] *
                                                 self.cat_in.n),
                                                dtype=np.float),
                                   table.Column(([[None] * (self.max_fit + 1)] *
                                                 self.cat_in.n),
                                                dtype=np.float),
                                   table.Column(([[None] * (self.max_fit + 1)] *
                                                 self.cat_in.n),
                                                dtype=np.float),
                                   table.Column(([[None] * (self.max_fit + 1)] *
                                                 self.cat_in.n),
                                                dtype=np.float),
                                   table.Column(([[None] * (self.max_fit + 1)] *
                                                 self.cat_in.n),
                                                dtype=np.float),
                                   table.Column(([[None] * (self.max_fit + 1)] *
                                                 self.cat_in.n),
                                                dtype=np.float)],
                                  names=['bkgx', 'sigma_bkgx', 'dbkgx',
                                         'bkgy', 'sigma_bkgy', 'dbkgy'],
                                  indexes=[13, 13, 13, 13, 13, 13])

    def __setattr__(self, name, val):
        """Set attribute.

        Parameters:
            :param name: Attribute name
            :type name: str
            :param val: Attribute value
            :type val: any
        """

        # Check attribute
        if name is 'cat_in':
            if not isinstance(val, catalog.Catalog):
                err_msg = "'{0}' has to be catalog.Catalog".format(name)
                raise message.NephastError(err_msg)

            if not val.fit_ready:
                err_msg = ("'{0}' has to be ready for fitting " +
                           "('{0}.fit_ready' = True)").format(name)
                raise message.NephastError(err_msg)

        if name is 'imag':
            if not isinstance(val, image.Image):
                err_msg = "'{0}' has to be image.Image".format(name)
                raise message.NephastError(err_msg)

        if name is 'psfun':
            if not isinstance(val, psf.Psf):
                err_msg = "'{0}' has to be psf.Psf".format(name)
                raise message.NephastError(err_msg)

        if name is 'inst':
            if not isinstance(val, instrument.Instrument):
                err_msg = "'{0}' has to be instrument.Instrument".format(name)
                raise message.NephastError(err_msg)

        if name is 'obs':
            if not isinstance(val, observation.Observation):
                err_msg = "'{0}' has to be catalog.Catalog".format(name)
                raise message.NephastError(err_msg)

        # Set attribute
        super().__setattr__(name, val)

    def __call__(self, verbose=False):
        """Perform non-linear least square fitting of all stars in the fitter.
        The output catalog contains only stars that have converged. It is saved
        in the fitter 'self.cat_out' attribute.
        If 'npar_bkg' is 0, then 'bkgc' is the median background value,
        When measuring fitting weights using the model profile and background,
        if there are non-positive values, they are set to the mean of the other
        values in order to calculate the noise as the square root.
        Stars' distances from each other (to find stars too close or stars that
        have to be subtracted) are calculated at every iteration. When stars are
        too close to each other, the fainter ones are taken out.

        Parameters:
            :param verbose: Show process messages
            :type verbose: bool; default False

        Returns:
            :return cat_out: Catalog after fitting
            :rtype cat_out: catalog.Catalog
        """

        # Check arguments
        if not isinstance(verbose, bool):
            err_msg = "'verbose' has to be bool"
            raise message.NephastError(err_msg)

        # Prepare history of fitting parameters
        for i_star in range(self.cat_in.n):
            self.hist['x'][i_star][0] = self.cat_in['x', i_star]
            self.hist['y'][i_star][0] = self.cat_in['y', i_star]
            self.hist['counts'][i_star][0] = self.cat_in['counts', i_star]
            self.hist['bkgc'][i_star][0] = self.cat_in['bkgc', i_star]

            if self.cat_in.nbkg == 3:
                self.hist['bkgx'][i_star][0] = self.cat_in['bkgx', i_star]
                self.hist['bkgy'][i_star][0] = self.cat_in['bkgy', i_star]

        # Prepare catalogs
        cat_in_tmp = copy.deepcopy(self.cat_in)
        cat_in_tmp.add_col(name='iter', col=([0] * self.cat_in.n))
        idx_sub = [[]] * cat_in_tmp.n
        idx_near = [[]] * cat_in_tmp.n

        for i_star, _ in enumerate(range(self.cat_in.n)):
            idx_sub[i_star] = cat_in_tmp.neighbors(i_star,
                                                   np.ceil(self.psf.psf_radius
                                                           + self.fit_radius +
                                                           1))
            idx_near[i_star] = cat_in_tmp.neighbors(i_star,
                                                    np.ceil(self.near_tol *
                                                            self.psf.fwhma))

        if self.fit_nbkg < 3:
            cat_in_tmp_nbkg = 1
        else:
            cat_in_tmp_nbkg = 3

        cat_in_tmp.nbkg_change(cat_in_tmp_nbkg)
        cat_in_tmp2 = copy.deepcopy(cat_in_tmp)

        # Repeat fitting
        i_fit = 0
        converged = 0
        dropped = 0
        image_model = []
        idx_drop = []
        flag_drop = []
        idx_remain = list(np.arange(self.cat_in.n))
        idx_remain_tmp = copy.deepcopy(idx_remain)
        remain = len(idx_remain)
        gain = self.fit_gain

        while (i_fit < self.max_fit) and (remain > 0):

            # Update gain
            i_fit += 1
            if (((i_fit - self.min_fit) % self.iter_gain) == 0) and \
                    (i_fit > self.min_fit):
                gain /= 2

            # Fit every star
            for n_star, idx_star in enumerate(idx_remain):
                if verbose:
                    print(("\rIteration {0:4d} / {1:<4d}:   star {2:6d} / " +
                           "{3:d}").format(i_fit, self.max_fit, (n_star + 1),
                                           remain), end="")

                # Check if star was dropped
                if idx_star in idx_remain_tmp:

                    # Check if star center is in the image
                    if (cat_in_tmp['x', idx_star] < -0.5) or \
                            (cat_in_tmp['x', idx_star] >
                             (self.image.side_x - 0.5)) or \
                            (cat_in_tmp['y', idx_star] < -0.5) or \
                            (cat_in_tmp['y', idx_star] >
                             (self.image.side_y - 0.5)):
                        idx_remain_tmp.remove(idx_star)
                        idx_drop.append(idx_star)
                        flag_drop.append('out')
                        dropped += 1
                        continue

                    # Check if star is too faint
                    if cat_in_tmp2['counts', idx_star] < \
                        (self.faint_tol *
                         cat_in_tmp2['sigma_counts', idx_star]):
                        idx_remain_tmp.remove(idx_star)
                        idx_drop.append(idx_star)
                        flag_drop.append('faint')
                        dropped += 1
                        continue

                    # Check if star is too close
                    if (len(idx_near[idx_star]) > 0) and \
                            (i_fit > self.iter_sub):
                        if np.any(cat_in_tmp['m', idx_star] >=
                                  cat_in_tmp['m', idx_near[idx_star]]):
                            for i_near in range(cat_in_tmp.n):
                                idx_near[i_near] = \
                                    list(np.setdiff1d(idx_near[i_near],
                                                      idx_star))

                            idx_remain_tmp.remove(idx_star)
                            idx_drop.append(idx_star)
                            flag_drop.append('near')
                            dropped += 1
                            continue
                        else:
                            for i_idx in idx_near[idx_star]:
                                for i_near in range(cat_in_tmp.n):
                                    idx_near[i_near] = \
                                        list(np.setdiff1d(idx_near[i_near],
                                                          i_idx))

                                idx_remain_tmp.remove(i_idx)
                                idx_drop.append(i_idx)
                                flag_drop.append('near')
                                dropped += 1

                    # Subtract nearby stars
                    image_sub = copy.deepcopy(self.image)

                    if len(idx_sub[idx_star]) > 0:
                        cat_sub = cat_in_tmp.select(idx_sub[idx_star])

                        if i_fit > self.iter_sub:
                            image_sub.sub_stars(cat_sub, self.psf)

                    # Cut image around the PSF
                    lim_x_min = max(int(np.floor(cat_in_tmp['x', idx_star] -
                                                 self.fit_radius)), 0)
                    lim_x_max = min(int(np.ceil(cat_in_tmp['x', idx_star] +
                                                self.fit_radius)),
                                    image_sub.side_x)
                    lim_y_min = max(int(np.floor(cat_in_tmp['y', idx_star] -
                                                 self.fit_radius)), 0)
                    lim_y_max = min(int(np.ceil(cat_in_tmp['y', idx_star] +
                                                self.fit_radius)),
                                    image_sub.side_y)
                    stamp = image_sub.array[lim_y_min: lim_y_max,
                                            lim_x_min: lim_x_max]

                    # Evalute profile
                    guess_x0 = cat_in_tmp['x', idx_star] - lim_x_min
                    guess_y0 = cat_in_tmp['y', idx_star] - lim_y_min
                    prof_eval, prof_pos, mesh_grid_psf = \
                        self.psf.eval_prof(stamp.shape[1], stamp.shape[0],
                                           guess_x0, guess_y0,
                                           counts=cat_in_tmp['counts',
                                                             idx_star],
                                           fit_rad=True)
                    prof_eval_snr = copy.deepcopy(prof_eval)
                    len_eval = len(prof_pos)
                    stamp_eval = stamp.flatten()[prof_pos]
                    eval_mask = stamp_eval.mask
                    prof_eval_hole, prof_pos_hole, _ = \
                        self.psf.eval_prof(stamp.shape[1], stamp.shape[0],
                                           guess_x0, guess_y0,
                                           counts=cat_in_tmp['counts',
                                                             idx_star],
                                           fit_rad=True,
                                           hole=self.bkg_radius_in)
                    stamp_eval_bkg = stamp.flatten()[prof_pos_hole]
                    eval_mask_bkg = stamp_eval_bkg.mask

                    # Evalute residuals
                    mesh_grid_img = [(mesh_grid_psf[0] + lim_x_min),
                                     (mesh_grid_psf[1] + lim_y_min)]

                    if cat_in_tmp.nbkg <= 1:
                        prof_eval = image.Image.add_bkg_px(prof_eval,
                                                           cat_in_tmp['bkgc',
                                                                      idx_star])
                    else:
                        prof_eval = \
                            image.Image.add_bkg_px(prof_eval,
                                                   cat_in_tmp['bkgc', idx_star],
                                                   cat_in_tmp['bkgx', idx_star],
                                                   cat_in_tmp['bkgx0',
                                                              idx_star],
                                                   cat_in_tmp['bkgy', idx_star],
                                                   cat_in_tmp['bkgy0',
                                                              idx_star],
                                                   mesh_grid_img)

                    prof_eval = np.ma.masked_array(prof_eval, eval_mask)
                    prof_eval_snr = np.ma.masked_array(prof_eval_snr, eval_mask)
                    res_eval = stamp_eval - prof_eval

                    # Evaluate noise
                    mesh_grid_img = [(mesh_grid_psf[0] + lim_x_min),
                                     (mesh_grid_psf[1] + lim_y_min)]

                    if (self.fit_noise == 'data') or (i_fit == 1):
                        data_variance = self.image[mesh_grid_img[0],
                                                   mesh_grid_img[1]]
                    else:
                        data_variance = image_model.array[mesh_grid_img[1],
                                                          mesh_grid_img[0]]

                        if cat_in_tmp.nbkg <= 1:
                            data_variance = \
                                image.Image.add_bkg_px(data_variance,
                                                       cat_in_tmp['bkgc',
                                                                  idx_star])
                        else:
                            data_variance = \
                                image.Image.add_bkg_px(data_variance,
                                                       cat_in_tmp['bkgc',
                                                                  idx_star],
                                                       cat_in_tmp['bkgx',
                                                                  idx_star],
                                                       cat_in_tmp['bkgx0',
                                                                  idx_star],
                                                       cat_in_tmp['bkgy',
                                                                  idx_star],
                                                       cat_in_tmp['bkgy0',
                                                                  idx_star],
                                                       mesh_grid_img)

                    noise = image.Image.noise(data_variance, self.obs.gain,
                                              self.obs.rn)
                    noise2 = noise ** 2

                    # Mask star
                    mesh_grid_psf[0] = np.ma.masked_array(mesh_grid_psf[0],
                                                          eval_mask)
                    mesh_grid_psf[1] = np.ma.masked_array(mesh_grid_psf[1],
                                                          eval_mask)
                    mesh_grid_img[0] = np.ma.masked_array(mesh_grid_img[0],
                                                          eval_mask)
                    mesh_grid_img[1] = np.ma.masked_array(mesh_grid_img[1],
                                                          eval_mask)

                    # Sigma-clip star
                    if (i_fit > self.iter_sigma) and \
                            (self.sigma_clip is not None) and \
                            np.any(res_eval >= (self.sigma_clip * noise)):
                        idx_sigma = np.where(res_eval >=
                                             (self.sigma_clip * noise))[0]
                        eval_mask[idx_sigma] = True
                        res_eval.mask = eval_mask
                        eval_mask_sigma_bkg = np.isin(prof_pos_hole,
                                                      prof_pos[idx_sigma])
                        eval_mask_bkg = eval_mask_bkg | eval_mask_sigma_bkg
                        stamp_eval_bkg.mask = eval_mask_bkg

                    # Check number of good pixels
                    n_good_px = np.sum(~eval_mask)

                    if (n_good_px < self.fit_npar) or (n_good_px <
                                                       self.fit_npx):
                        idx_remain_tmp.remove(idx_star)
                        idx_drop.append(idx_star)
                        flag_drop.append('npix')
                        dropped += 1
                        continue

                    # Evaluate weights and reduced chi^2
                    weight = 1 / noise2
                    chi2 = np.sum((res_eval ** 2) * weight)
                    chi2r = chi2 / (n_good_px - self.fit_npar)

                    # Least square fitting
                    df_dx0 = self.psf.df_dx0(mesh_grid_psf[0], mesh_grid_psf[1],
                                             guess_x0, guess_y0) * \
                        cat_in_tmp['counts', idx_star]
                    df_dy0 = self.psf.df_dy0(mesh_grid_psf[0], mesh_grid_psf[1],
                                             guess_x0, guess_y0) * \
                        cat_in_tmp['counts', idx_star]
                    df_dcounts = self.psf.df_dcounts(mesh_grid_psf[0],
                                                     mesh_grid_psf[1], guess_x0,
                                                     guess_y0)
                    m_arr = np.array([[np.ma.sum(df_dx0 * df_dx0 * weight),
                                       np.ma.sum(df_dx0 * df_dy0 * weight),
                                       np.ma.sum(df_dx0 * df_dcounts * weight)],
                                      [np.ma.sum(df_dy0 * df_dx0 * weight),
                                       np.ma.sum(df_dy0 * df_dy0 * weight),
                                       np.ma.sum(df_dy0 * df_dcounts * weight)],
                                      [np.ma.sum(df_dcounts * df_dx0 * weight),
                                       np.ma.sum(df_dcounts * df_dy0 * weight),
                                       np.ma.sum(df_dcounts * df_dcounts *
                                                 weight)]])
                    v_arr = np.array([[np.ma.sum(res_eval * df_dx0 * weight)],
                                      [np.ma.sum(res_eval * df_dy0 * weight)],
                                      [np.ma.sum(res_eval * df_dcounts *
                                                 weight)]])

                    if self.fit_nbkg > 0:
                        df_dbkgc = np.ma.masked_array(([1] * len_eval),
                                                      eval_mask)
                        m_arr = np.hstack((m_arr,
                                           np.array([[np.ma.sum(df_dx0 *
                                                                df_dbkgc *
                                                                weight)],
                                                     [np.ma.sum(df_dy0 *
                                                                df_dbkgc *
                                                                weight)],
                                                     [np.ma.sum(df_dcounts *
                                                                df_dbkgc *
                                                                weight)]])))
                        m_arr = np.vstack((m_arr,
                                           np.array([np.ma.sum(df_dbkgc * df_dx0
                                                               * weight),
                                                     np.ma.sum(df_dbkgc * df_dy0
                                                               * weight),
                                                     np.ma.sum(df_dbkgc *
                                                               df_dcounts *
                                                               weight),
                                                     np.ma.sum(df_dbkgc *
                                                               df_dbkgc *
                                                               weight)])))
                        v_arr = np.vstack((v_arr,
                                           np.array([np.ma.sum(res_eval *
                                                               df_dbkgc *
                                                               weight)])))

                        if self.fit_nbkg > 1:
                            df_dbkgx = mesh_grid_img[0] - \
                                       cat_in_tmp['bkgx0', idx_star]
                            df_dbkgy = mesh_grid_img[1] - \
                                cat_in_tmp['bkgy0', idx_star]
                            m_arr = np.hstack((m_arr,
                                               np.array([[np.ma.sum(df_dx0 *
                                                                    df_dbkgx *
                                                                    weight),
                                                          np.ma.sum(df_dx0 *
                                                                    df_dbkgy *
                                                                    weight)],
                                                         [np.ma.sum(df_dy0 *
                                                                    df_dbkgx *
                                                                    weight),
                                                          np.ma.sum(df_dy0 *
                                                                    df_dbkgy *
                                                                    weight)],
                                                         [np.ma.sum(df_dcounts *
                                                                    df_dbkgx *
                                                                    weight),
                                                          np.ma.sum(df_dcounts *
                                                                    df_dbkgy *
                                                                    weight)],
                                                         [np.ma.sum(df_dbkgc *
                                                                    df_dbkgx *
                                                                    weight),
                                                          np.ma.sum(df_dbkgc *
                                                                    df_dbkgy *
                                                                    weight)]])))
                            m_arr = np.vstack((m_arr,
                                               np.array([[np.ma.sum(df_dbkgx *
                                                                    df_dx0 *
                                                                    weight),
                                                          np.ma.sum(df_dbkgx *
                                                                    df_dy0 *
                                                                    weight),
                                                          np.ma.sum(df_dbkgx *
                                                                    df_dcounts *
                                                                    weight),
                                                          np.ma.sum(df_dbkgx *
                                                                    df_dbkgc *
                                                                    weight),
                                                          np.ma.sum(df_dbkgx *
                                                                    df_dbkgx *
                                                                    weight),
                                                          np.ma.sum(df_dbkgx *
                                                                    df_dbkgy *
                                                                    weight)],
                                                         [np.ma.sum(df_dbkgy *
                                                                    df_dx0 *
                                                                    weight),
                                                          np.ma.sum(df_dbkgy *
                                                                    df_dy0 *
                                                                    weight),
                                                          np.ma.sum(df_dbkgy *
                                                                    df_dcounts *
                                                                    weight),
                                                          np.ma.sum(df_dbkgy *
                                                                    df_dbkgc *
                                                                    weight),
                                                          np.ma.sum(df_dbkgy *
                                                                    df_dbkgx *
                                                                    weight),
                                                          np.ma.sum(df_dbkgy *
                                                                    df_dbkgy *
                                                                    weight)]])))
                            v_arr = np.vstack((v_arr,
                                               np.array([[np.ma.sum(res_eval *
                                                                    df_dbkgx *
                                                                    weight)],
                                                         [np.ma.sum(res_eval *
                                                                    df_dbkgy *
                                                                    weight)]])))

                    m_arr1 = np.linalg.inv(m_arr)
                    variances = np.sqrt(m_arr1.diagonal())
                    a_arr = np.dot(m_arr1, v_arr)

                    # Check fitting step size
                    gain_tmp = gain

                    if np.abs(gain_tmp * a_arr[2][0]) > \
                            (cat_in_tmp2['counts', idx_star] * self.fit_max):
                        gain_tmp = np.abs(cat_in_tmp2['counts', idx_star] *
                                          self.fit_max / a_arr[2][0])

                    # Update astrometry and photometry
                    cat_in_tmp2['x', idx_star] += gain_tmp * a_arr[0][0]
                    cat_in_tmp2['sigma_x', idx_star] = variances[0]
                    cat_in_tmp2['y', idx_star] += gain_tmp * a_arr[1][0]
                    cat_in_tmp2['sigma_y', idx_star] = variances[1]
                    cat_in_tmp2['counts', idx_star] += gain_tmp * a_arr[2][0]
                    cat_in_tmp2['sigma_counts', idx_star] = variances[2]
                    cat_in_tmp2['m', idx_star] = \
                        catalog.Catalog.counts_to_m(cat_in_tmp2['counts',
                                                                idx_star],
                                                    m1c=self.cat_in.m1c)
                    cat_in_tmp2['sigma_m', idx_star] = \
                        catalog.Catalog.sigmacounts_to_sigmam(
                            cat_in_tmp2['counts', idx_star],
                            cat_in_tmp2['sigma_counts', idx_star])
                    cat_in_tmp2['peak', idx_star] = \
                        catalog.Catalog.counts_to_peak(cat_in_tmp2['counts',
                                                                   idx_star],
                                                       self.psf.peak)
                    cat_in_tmp2['sigma_peak', idx_star] = \
                        catalog.Catalog.sigmacounts_to_sigmapeak(
                            cat_in_tmp2['sigma_counts', idx_star],
                            self.psf.peak)
                    cat_in_tmp2['bkg', idx_star] = \
                        np.ma.median(stamp_eval_bkg - prof_eval_hole)
                    cat_in_tmp2['sigma_bkg', idx_star] = \
                        np.std(stamp_eval_bkg - prof_eval_hole)

                    if self.fit_nbkg == 0:
                        cat_in_tmp2['bkgc', idx_star] = \
                            cat_in_tmp2['bkg', idx_star]
                        cat_in_tmp2['sigma_bkgc', idx_star] = \
                            cat_in_tmp2['sigma_bkg', idx_star]
                    else:
                        cat_in_tmp2['bkgc', idx_star] += (gain_tmp *
                                                          a_arr[3][0])
                        cat_in_tmp2['sigma_bkgc', idx_star] = variances[3]

                        if self.fit_nbkg == 3:
                            cat_in_tmp2['bkgx', idx_star] += (gain_tmp *
                                                              a_arr[4][0])
                            cat_in_tmp2['sigma_bkgx', idx_star] = variances[4]
                            cat_in_tmp2['bkgy', idx_star] += (gain_tmp *
                                                              a_arr[5][0])
                            cat_in_tmp2['sigma_bkgy', idx_star] = variances[5]

                    cat_in_tmp2['snr', idx_star] = \
                        np.ma.sum(prof_eval_snr) / np.sqrt(np.ma.sum(noise2))
                    cat_in_tmp2['chi2r', idx_star] = chi2r
                    cat_in_tmp2['iter', idx_star] = i_fit

                    # Update history of fitting parameters
                    self.hist['x'][idx_star][i_fit] = cat_in_tmp2['x', idx_star]
                    self.hist['sigma_x'][idx_star][i_fit] = \
                        cat_in_tmp2['sigma_x', idx_star]
                    self.hist['dx'][idx_star][i_fit] = gain_tmp * a_arr[0][0]
                    self.hist['y'][idx_star][i_fit] = cat_in_tmp2['y', idx_star]
                    self.hist['sigma_y'][idx_star][i_fit] = \
                        cat_in_tmp2['sigma_y', idx_star]
                    self.hist['dy'][idx_star][i_fit] = gain_tmp * a_arr[1][0]
                    self.hist['counts'][idx_star][i_fit] = cat_in_tmp2['counts',
                                                                       idx_star]
                    self.hist['sigma_counts'][idx_star][i_fit] = \
                        cat_in_tmp2['sigma_counts', idx_star]
                    self.hist['dcounts'][idx_star][i_fit] = \
                        gain_tmp * a_arr[2][0]
                    self.hist['bkgc'][idx_star][i_fit] = cat_in_tmp2['bkgc',
                                                                     idx_star]
                    self.hist['sigma_bkgc'][idx_star][i_fit] = \
                        cat_in_tmp2['sigma_bkgc', idx_star]

                    if self.fit_nbkg == 0:
                        self.hist['dbkgc'][idx_star][i_fit] = \
                            self.hist['bkgc'][idx_star][i_fit] - \
                            self.hist['bkgc'][idx_star][i_fit - 1]
                    else:
                        self.hist['dbkgc'][idx_star][i_fit] = \
                            gain_tmp * a_arr[3][0]

                    if self.fit_nbkg > 1:
                        self.hist['bkgx'][idx_star][i_fit] = \
                            cat_in_tmp2['bkgx', idx_star]
                        self.hist['sigma_bkgx'][idx_star][i_fit] = \
                            cat_in_tmp2['sigma_bkgx', idx_star]
                        self.hist['dbkgx'][idx_star][i_fit] = \
                            gain_tmp * a_arr[4][0]
                        self.hist['bkgy'][idx_star][i_fit] = \
                            cat_in_tmp2['bkgy', idx_star]
                        self.hist['sigma_bkgy'][idx_star][i_fit] = \
                            cat_in_tmp2['sigma_bkgy', idx_star]
                        self.hist['dbkgy'][idx_star][i_fit] = \
                            gain_tmp * a_arr[5][0]

                    self.hist['chi2r'][idx_star][i_fit] = \
                        cat_in_tmp2['chi2r', idx_star]

                    if i_fit > 1:
                        self.hist['dchi2r'][idx_star][i_fit] = \
                            self.hist['chi2r'][idx_star][i_fit] - \
                            self.hist['chi2r'][idx_star][i_fit - 1]

                    self.hist['iter'][idx_star] = i_fit

                    # Check convergence
                    if (i_fit >= self.min_fit) and \
                            (np.abs(self.hist['dchi2r'][idx_star][i_fit]) <
                             self.chi2r_tol):
                        idx_remain_tmp.remove(idx_star)
                        converged += 1

            # End iteration
            idx_remain = copy.deepcopy(idx_remain_tmp)
            remain = len(idx_remain)

            if verbose:
                print(("\rIteration {0:4d} / {1:<4d}:   " +
                       "completing...       {2:7d} remaining {3:7d} " +
                       "converged {4:7d} dropped").format(i_fit, self.max_fit,
                                                          remain, converged,
                                                          dropped), end="")

            cat_in_tmp = copy.deepcopy(cat_in_tmp2)
            cat_remain = copy.deepcopy(cat_in_tmp2)
            cat_remain.remove(idx_drop)
            image_model = image.Image.from_cat(self.inst, self.obs, cat_remain,
                                               self.psf, bkg=False)

            for i_star in idx_remain:
                idx_sub[i_star] = \
                    cat_in_tmp.neighbors(i_star, np.ceil(self.psf.psf_radius +
                                                         self.fit_radius + 1))
                idx_sub[i_star] = np.setdiff1d(idx_sub[i_star], idx_drop)
                idx_near[i_star] = \
                    cat_in_tmp.neighbors(i_star, np.ceil(self.near_tol *
                                                         self.psf.fwhma))
                idx_near[i_star] = np.setdiff1d(idx_near[i_star], idx_drop)

        # Final catalogs
        if verbose:
            print(("\rIteration {0:4d} / {1:<4d}:   fitting completed   " +
                   "{2:7d} remaining {3:7d} converged {4:7d} dropped").format(
                i_fit, self.max_fit, remain, converged, dropped))

        for i_remain in idx_remain:
            idx_remain.remove(i_remain)
            idx_drop.append(i_remain)
            flag_drop.append('conv')
            dropped += 1

        cat_remain = copy.deepcopy(cat_in_tmp2)
        cat_remain.remove(idx_drop)
        flag_drop = [i for _, i in sorted(zip(idx_drop, flag_drop))]
        idx_drop.sort()
        cat_drop = cat_in_tmp.select(idx_drop)
        cat_drop.add_col(name='idx_in', col=idx_drop)
        cat_drop.add_col(name='drop', col=flag_drop)
        idx_out = np.setdiff1d(np.arange(self.cat_in.n), idx_drop)
        cat_remain.add_col(name='idx_in', col=idx_out)
        image_model = image.Image.from_cat(self.inst, self.obs, cat_remain,
                                           self.psf, bkg=False)

        # Final outputs
        self.cat_drop = cat_drop
        self.cat_out = cat_remain
        self.image_model = copy.deepcopy(image_model)
        self.image_sub = copy.deepcopy(self.image)
        self.image_sub.sub_stars(self.cat_out, self.psf)

        if verbose:
            print()

        return self.cat_out

    def plot(self, star):
        """Plot the fitting history of one star.

        Parameters:
            :param star: Index or name of the star to plot
            :type star: int, str
        """

        # Show plots
        analyze.plot_history(self, star)
