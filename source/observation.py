"""NEPHAST by Paolo Turri <pturri83@gmail.com>
Released under BSD License 2.0

This module deals with the class that stores he parameters defining how
images were taken.
"""


import numpy as np

import config
import instrument
import message
import object


__all__ = ['Observation']


class Observation(object.Object):
    """Observation object used to create and analyze image frames.
    """

    def __init__(self, inst, conf=None, **kwargs):
        """Initialize an observation that contains parameters used to create and
        analyze image frame.
        If both 'coadd' and 'coavg' in the configuration are larger than 1, it
        means the image is the average of 'coavg' groups made of 'coadd'
        exposures.
        The gain of the image is the gain of the detector multiplied by the
        coaverages. The read noise of the image is the read noise of the
        detector multiplied by the square root of the coadds over the
        coaverages.
        Background value includes sky, optics thermal and detector thermal
        backgrounds. It can be flat or with a gradient across the field,
        measured from the (0, 0) pixel of the image.

        Parameters:
            :param inst: Detector
            :type inst: instrument.Instrument
            :param conf: Configuration
            :type conf: config.Config; optional

        Optional keyword arguments:
            :kwar bkgc: Background constant intensity (sky + instrument + dark)
                (DN/px)
            :kwtype bkgc: int, float
            :kwar bkgx: Background x gradient (DN/px/px)
            :kwtype bkgx: int, float
            :kwar bkgy: Background y gradient (DN/px/px)
            :kwtype bkgy: int, float

        Attributes:
            :attr gain: Image gain (e-/DN)
            :atype gain: float
            :attr rn: Image read noise (e-)
            :atype rn: float
            :attr coadd: Number of exposures co-added
            :atype coadd: int
            :attr coavg: Number of exposures co-averaged
            :atype coavg: int
            :attr bkgc: Background constant intensity (sky + instrument + dark)
                (DN/px)
            :atype bkgc: float
            :attr bkgx: Background x gradient (DN/px/px)
            :atype bkgx: float
            :attr bkgy: Background y gradient (DN/px/px)
            :atype bkgy: float
            :attr nbkg: Number of parameters used to describe the background. 0:
                no background. 1: flat background. 3: background with gradient
            :atype nbkg: int
        """

        # Check arguments
        if not isinstance(inst, instrument.Instrument):
            err_msg = "'inst' has to be instrument.Instrument"
            raise message.NephastError(err_msg)

        if not isinstance(conf, (config.Config, type(None))):
            err_msg = "'conf' has to be config.Config"
            raise message.NephastError(err_msg)

        for i_kwarg in kwargs:
            if i_kwarg not in ('bkgc', 'bkgx', 'bkgy'):
                err_msg = "'{0}' is not a valid argument".format(i_kwarg)
                raise message.NephastError(err_msg)

            if not isinstance(kwargs[i_kwarg], (int, float)):
                err_msg = "'{0}' has to be int or float".format(i_kwarg)
                raise message.NephastError(err_msg)

            if i_kwarg == 'bkgc':
                if not (kwargs[i_kwarg] >= 0):
                    err_msg = "'bkgc' has to be non-negative"
                    raise message.NephastError(err_msg)

            if i_kwarg in ('bkgx', 'bkgy'):
                if 'bkgc' not in kwargs:
                    err_msg = \
                        "'{0}' has to be declared with 'bkgc'".format(i_kwarg)
                    raise message.NephastError(err_msg)

                if not all([(i in kwargs) for i in ('bkgx', 'bkgy')]):
                    err_msg = "Both 'bkgx' and 'bkgy' have to be declared"
                    raise message.NephastError(err_msg)

        # Define configuration
        if conf is None:
            conf = config.Config()

        # Store attributes
        self.gain = inst.gain * conf.coavg
        self.rn = inst.rn * np.sqrt(conf.coadd / conf.coavg)
        self.coadd = conf.coadd
        self.coavg = conf.coavg

        if 'bkgc' in kwargs:
            self.bkgc = np.float64(kwargs['bkgc'])
            self.nbkg = 1

            if all([(i in kwargs) for i in ('bkgx', 'bkgy')]):
                self.bkgx = np.float64(kwargs['bkgx'])
                self.bkgy = np.float64(kwargs['bkgy'])
                self.nbkg = 3
        else:
            self.nbkg = 0
