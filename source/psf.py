"""NEPHAST by Paolo Turri <pturri83@gmail.com>
Released under BSD License 2.0

This module deals with the point-spread function (PSF) class for profile
fitting.
"""


import warnings

import numpy as np
from scipy import integrate

import catalog
import config
import message
import object


__all__ = ['Psf']


class Psf(object.Object):
    """Generic PSF object used for planting and extracting stars.
    """

    def __init__(self, peak, fwhm, var, conf):
        """Initialize a generic PSF.

        Parameters:
            :param peak: Peak value of a normalized profile
            :type peak: int, float
            :param fwhm: FWHMa (px) [long, short, arithmetic mean, geometric
                mean]
            :type fwhm: list [4] (int, float)
            :param var: Spatially variable
            :type var: bool
            :param conf: Configuration
            :type conf: config.Config

        Attributes:
            :attr peak: Peak value of a normalized profile
            :atype peak: float
            :attr fwhm1: Long FWHM (px)
            :atype fwhm1: float
            :attr fwhm2: Short FWHM (px)
            :atype fwhm2: float
            :attr fwhma: Arithmetic mean FWHM (px)
            :atype fwhma: float
            :attr fwhmg: Geometric mean FWHM (px)
            :atype fwhmg: float
            :attr psf_radius: Radius over which the PSF is defined (px)
            :atype psf_radius: float
            :attr fit_radius: Radius over which the PSF is fitted (px)
            :atype fit_radius: float
            :attr m1c: Instrumental magnitude of a star with 1 DN of intensity
            :atype m1c: float
            :attr var: Spatially variable
            :atype var: bool
        """

        # Store attributes
        self.peak = np.float64(peak)
        self.fwhm1 = np.float64(fwhm[0])
        self.fwhm2 = np.float64(fwhm[1])
        self.fwhma = np.float64(fwhm[2])
        self.fwhmg = np.float64(fwhm[3])
        self.psf_radius = conf.psf_radius
        self.fit_radius = conf.fit_radius
        self.m1c = conf.m1c
        self.var = var

    def psf_pos(self, side_x, side_y, x0, y0, fit_rad=False, rad=None,
                no_rad=False, hole=0.):
        """Find the position (index) of the pixels in the image that belong to a
        PSF centered on (x0, y0).
        The PSF can be calculated within the PSF radius, the fitting radius, a
        custom radius or with no outer radius (in inverse order of precedence).
        Pixels can be excluded for a hole in the center. Use a radius of zero
        pixels for the hole to exclude no pixels in the center.

        Parameters:
            :param side_x: Detector width (px)
            :type side_x: int
            :param side_y: Detector height (px)
            :type side_y: int
            :param x0: Central x coordinate of the star (px)
            :type x0: float
            :param y0: Central y coordinate of the star (px)
            :type y0: float
            :param fit_rad: Evaluate within the fitting radius instead of the
                PSF radius
            :type fit_rad: bool; default False
            :param rad: Custom evaluation radius
            :type rad: float; optional
            :param no_rad: Evaluate without an outer radius. It takes precedence
                over `fit_rad`
            :type no_rad: bool; default False
            :param hole: Radius of central hole
            :type hole: float; default 0.

        Returns:
            :return psf_pos: Indices of the pixels in the PSF
            :rtype psf_pos: numpy.ndarray (int)
            :return psf_dist: x, y distances of the pixels in the PSF from its
                center
            :rtype psf_dist: list [2] (numpy.ndarray (float))
            :return mesh_grid_flat: x, y coordinates of the pixels in the PSF
            :rtype mesh_grid_flat: list [2] (numpy.ndarray (int))
        """

        # Find position of the star's PSF
        mesh_grid = (np.mgrid[: side_y, : side_x]).astype(int)
        dist_x_grid = np.concatenate(mesh_grid[1]) - x0
        dist_y_grid = np.concatenate(mesh_grid[0]) - y0
        dist_grid = np.hypot(dist_x_grid, dist_y_grid)

        # Evaluate PSF
        if not no_rad:
            if rad is None:
                if not fit_rad:
                    where_rad = (dist_grid <= self.psf_radius)
                else:
                    where_rad = (dist_grid <= self.fit_radius)
            else:
                where_rad = (dist_grid <= rad)
        else:
            where_rad = np.array([True] * len(dist_grid))

        if hole == 0:
            psf_pos = np.where(where_rad)[0]
        else:
            psf_pos = np.where(where_rad & (dist_grid >= hole))[0]

        psf_dist = [dist_x_grid[psf_pos], dist_y_grid[psf_pos]]
        mesh_grid_flat = [mesh_grid[1].flatten()[psf_pos],
                          mesh_grid[0].flatten()[psf_pos]]

        return psf_pos, psf_dist, mesh_grid_flat

    def eval_psf(self, side_x, side_y, x0, y0, fit_rad=False, rad=None,
                 no_rad=False, hole=0.):
        """Evaluate the normalized analytic PSF.
        The PSF can be calculated within the PSF radius, the fitting radius, a
        custom radius or with no outer radius (in inverse order of precedence).
        Pixels can be excluded for a hole in the center. Use a radius of zero
        pixels for the hole to exclude no pixels in the center.

        Parameters:
            :param side_x: Detector width (px)
            :type side_x: int
            :param side_y: Detector height (px)
            :type side_y: int
            :param x0: Central x coordinate of the star (px)
            :type x0: float
            :param y0: Central y coordinate of the star (px)
            :type y0: float
            :param fit_rad: Evaluate within the fitting radius instead of the
                PSF radius
            :type fit_rad: bool; default False
            :param rad: Custom evaluation radius
            :type rad: float; optional
            :param no_rad: Evaluate without an outer radius. It takes precedence
                over `fit_rad`
            :type no_rad: bool; default False
            :param hole: Radius of central hole
            :type hole: float; default 0.
        """

        pass

    def eval_prof(self, side_x, side_y, x, y, m=None, counts=None, peak=None,
                  fit_rad=False, rad=None, no_rad=False, hole=0.):
        """Evaluate a profile from the analytic PSF and a magnitude.
        The profile can be calculated within the PSF radius, the fitting radius,
        a custom radius or with no outer radius (in inverse order of
        precedence).
        Pixels can be excluded for a hole in the center. Use a radius of zero
        pixels for the hole to exclude no pixels in the center.

        Parameters:
            :param side_x: Detector width (px)
            :type side_x: int
            :param side_y: Detector height (px)
            :type side_y: int
            :param x: x coordinate of the star (px)
            :type x: float
            :param y: y coordinate of the star (px)
            :type y: float
            :param m: Instrumental magnitude of the star
            :type m: float; optional
            :param counts: Counts of the star (DN)
            :type counts: float; optional
            :param peak: Peak value of the star
            :type peak: float; optional
            :param fit_rad: Evaluate within the fitting radius instead of the
                PSF radius
            :type fit_rad: bool; default False
            :param rad: Custom evaluation radius
            :type rad: float; optional
            :param no_rad: Evaluate without an outer radius. It takes precedence
                over `fit_rad`
            :type no_rad: bool; default False
            :param hole: Radius of central hole
            :type hole: float; default 0.
        """

        pass

    def f(self, x, y):
        """Calculate a normalized profile.

        Parameters:
            :param x: x coordinates of the pixel center from the PSF center (px)
            :type x: int, float
            :param y: y coordinates of the pixel center from the PSF center (px)
            :type y: int, float
        """

        pass

    def df_dx0(self, x, y, x0, y0):
        """Calculate the derivative of a normalized profile respect to its
        center coordinate x0.

        Parameters:
            :param x: x coordinate (px)
            :type x: int, float, numpy.ndarray [m, n] (int, float)
            :param y: y coordinate (px)
            :type y: int, float, numpy.ndarray [m, n] (int, float)
            :param x0: x coordinate of the PSF center (px)
            :type x: int, float
            :param y0: y coordinate of the PSF center (px)
            :type y: int, float
        """

        pass

    def df_dy0(self, x, y, x0, y0):
        """Calculate the derivative of a normalized profile respect to its
        center coordinate y0.

        Parameters:
            :param x: x coordinate (px)
            :type x: int, float, numpy.ndarray [m, n] (int, float)
            :param y: y coordinate (px)
            :type y: int, float, numpy.ndarray [m, n] (int, float)
            :param x0: x coordinate of the PSF center (px)
            :type x: int, float
            :param y0: y coordinate of the PSF center (px)
            :type y: int, float
        """

        pass

    def df_dcounts(self, x, y, x0, y0):
        """Calculate the derivative of a normalized analytic profile respect to
        counts. Because the profile is normalized, the derivative is the same as
        the normalized function itself.

        Parameters:
            :param x: x coordinate (px)
            :type x: int, float, numpy.ndarray [m, n] (int, float)
            :param y: y coordinate (px)
            :type y: int, float, numpy.ndarray [m, n] (int, float)
            :param x0: x coordinate of the PSF center (px)
            :type x: int, float
            :param y0: y coordinate of the PSF center (px)
            :type y: int, float

        Returns:
            :return z: Value of the PSF derivative respect to counts
            :rtype z: float, numpy.ndarray [m, n] (float)
        """

        # Analytic function derivative respect to counts
        z = self.f((x - x0), (y - y0))

        return z


class Analytic(Psf):
    """Generic PSF defined by an analytic function.
    """

    def __init__(self, peak, fwhm, name, conf):
        """Initialize a generic analytic PSF. Set also the pixel evaluation.

        Parameters:
            :param peak: Peak value of a normalized profile
            :type peak: int, float
            :param fwhm: FWHMa (px) [long, short]
            :type fwhm: list [2] (int, float)
            :param name: Analytic function name. Choices are 'gauss', 'moffat'
            :type name: str
            :param conf: Configuration
            :type conf: config.Config

        Attributes:
            :attr name: Analytic function name
            :atype name: str
            :attr eval_method: Pixel evaluation method. Available options:
                'center', 'corners', 'grid' and 'integral'
            :atype eval_method: str
            :attr eval_grid: Pixel evaluation sampling density
            :atype eval_grid: int
            :attr conf: Configuration
            :atype conf: config.Config
        """

        # Calculate FWHMa
        fwhma = np.average(fwhm)
        fwhmg = np.sqrt(fwhm[0] * fwhm[1])
        fwhm.append(fwhma)
        fwhm.append(fwhmg)

        # Store attributes
        self.name = name
        self.eval_method = None
        self.eval_grid = None
        self.conf = conf

        # Set evaluation
        self.set_method(conf.eval_method)
        self.set_grid(conf.eval_grid)
        # TODO: if evaluation methods are useful for an empirical PSF too, move
        #  this part to the Psf class

        # Pass to parent class
        # TODO: analytic PSFs are spatially constant. If this is going to
        #  change, the following line has to allow for it
        super().__init__(peak, fwhm, False, conf)

    def set_method(self, method):
        """Set the evaluation method.

        Parameters:
            :param method: Pixel evaluation method. Available options: 'center',
                'corners', 'grid' and 'integral'
            :type method: str

        Attributes:
            :attr eval_method: Pixel evaluation method
            :atype eval_method: str
        """

        # Check arguments
        if method not in ('center', 'corners', 'grid', 'integral'):
            err_msg = \
                "'method' has to be 'center', 'corners', 'grid' or 'integral'"
            raise message.NephastError(err_msg)

        # Store attributes
        self.eval_method = method

    def set_grid(self, grid):
        """Set the sampling density for the 'grid' evaluation method.

        Parameters:
            :param grid: Pixel evaluation sampling density
            :type grid: int

        Attributes:
            :attr eval_grid: Pixel evaluation sampling density
            :atype eval_grid: int
        """

        # Check arguments
        if not isinstance(grid, (np.integer, int)):
            err_msg = "'grid' has to be int"
            raise message.NephastError(err_msg)

        if not grid >= 1:
            err_msg = "'grid' has to be positive"
            raise message.NephastError(err_msg)

        # Store attributes
        self.eval_grid = grid

    def f(self, x, y):
        """Calculate a normalized profile.

        Parameters:
            :param x: x coordinates of the pixel center from the PSF center (px)
            :type x: int, float
            :param y: y coordinates of the pixel center from the PSF center (px)
            :type y: int, float
        """

        pass

    def eval_pix(self, x, y):
        """Evaluate a pixel in the analytic PSF.
        If the method is 'center', the analytic function is evaluated at the
        pixel center. If it's 'corners', the unction is evaluated at the four
        pixel corners and then averaged to calculate it at the center. If it's
        'grid', the function is evaluated on a regular grid (not along the
        pixel's edge) and then averaged. If it's 'integral', the function is
        integrated numerically around the pixel center with range [-0.5, 0.5] in
        x and y. The pixel fill factor is assumed to be 1.

        Parameters:
            :param x: x coordinate of the pixel center from the PSF center (px)
            :type x: float
            :param y: y coordinate of the pixel center from the PSF center (px)
            :type y: float

        Returns:
            :return pix_eval: Value of the pixel (DN)
            :rtype pix_eval: float
        """

        # Evaluate pixel
        pix_eval = None

        if self.eval_method == 'center':
            pix_eval = self.f(x, y)
        elif self.eval_method == 'corners':
            corners = [[-0.5, -0.5], [-0.5, 0.5], [0.5, -0.5], [0.5, 0.5]]
            f_corners = [None] * 4

            for i_corner, corner in enumerate(corners):
                f_corners[i_corner] = self.f((x + corner[0]), (y + corner[1]))

            pix_eval = np.mean(f_corners)
        elif self.eval_method == 'grid':
            grid_lin = np.linspace(0, 1, (self.eval_grid + 1))[: -1] + \
                       (1 / self.eval_grid / 2) - 0.5
            grid_x, grid_y = np.meshgrid(grid_lin, grid_lin)
            pix_eval = np.mean([self.f((x + grid_x), (y + grid_y))])
        elif self.eval_method == 'integral':
            pix_eval = integrate.dblquad(self.f, (x - 0.5), (x + 0.5),
                                         (lambda a: (y - 0.5)),
                                         (lambda a: (y + 0.5)))[0]

        return pix_eval

        # TODO: if evaluation methods are useful for an empirical PSF too, move
        #  this function to the Psf class and modify the docstring

    def eval_psf(self, side_x, side_y, x0, y0, fit_rad=False, rad=None,
                 no_rad=False, hole=0.):
        """Evaluate the normalized analytic PSF.
        The PSF can be calculated within the PSF radius, the fitting radius, a
        custom radius or with no outer radius (in inverse order of precedence).
        Pixels can be excluded for a hole in the center. Use a radius of zero
        pixels for the hole to exclude no pixels in the center.

        Parameters:
            :param side_x: Detector width (px)
            :type side_x: int
            :param side_y: Detector height (px)
            :type side_y: int
            :param x0: Central x coordinate of the star (px)
            :type x0: float
            :param y0: Central y coordinate of the star (px)
            :type y0: float
            :param fit_rad: Evaluate within the fitting radius instead of the
                PSF radius
            :type fit_rad: bool; default False
            :param rad: Custom evaluation radius
            :type rad: float; optional
            :param no_rad: Evaluate without an outer radius. It takes precedence
                over `fit_rad`
            :type no_rad: bool; default False
            :param hole: Radius of central hole
            :type hole: float; default 0.

        Returns:
            :return psf_eval: Value of the pixels (DN)
            :rtype psf_eval: numpy.ndarray [n] (float)
            :return psf_pos: Indices of the pixels
            :rtype psf_pos: numpy.ndarray [n] (float)
            :return mesh_grid: x, y coordinates of pixels respect to the PSF
            :rtype mesh_grid: list [2] (numpy.ndarray (int))
        """

        # Find position of the star's PSF
        psf_pos, psf_xy, mesh_grid = self.psf_pos(side_x, side_y, x0, y0,
                                                  fit_rad=fit_rad, rad=rad,
                                                  no_rad=no_rad, hole=hole)

        # Calculate pixel values
        psf_eval = np.full(len(psf_pos), np.nan)

        for i_pix in range(len(psf_pos)):
            psf_eval[i_pix] = self.eval_pix(psf_xy[0][i_pix], psf_xy[1][i_pix])

        return psf_eval, psf_pos, mesh_grid

        # TODO: if evaluation methods are useful for an empirical PSF too, move
        #  this function to the Psf class and modify the docstring

    def eval_prof(self, side_x, side_y, x, y, m=None, counts=None, peak=None,
                  fit_rad=False, rad=None, no_rad=False, hole=0.):
        """Evaluate a profile from the analytic PSF.
        The profile can be calculated within the PSF radius, the fitting radius,
        a custom radius or with no outer radius (in inverse order of
        precedence).
        Pixels can be excluded for a hole in the center. Use a radius of zero
        pixels for the hole to exclude no pixels in the center.

        Parameters:
            :param side_x: Detector width (px)
            :type side_x: int
            :param side_y: Detector height (px)
            :type side_y: int
            :param x: x coordinate of the star (px)
            :type x: float
            :param y: y coordinate of the star (px)
            :type y: float
            :param m: Instrumental magnitude of the star
            :type m: float; optional
            :param counts: Counts of the star (DN)
            :type counts: float; optional
            :param peak: Peak value of the star
            :type peak: float; optional
            :param fit_rad: Evaluate within the fitting radius instead of the
                PSF radius
            :type fit_rad: bool; default False
            :param rad: Custom evaluation radius
            :type rad: float; optional
            :param no_rad: Evaluate without an outer radius. It takes precedence
                over `fit_rad`
            :type no_rad: bool; default False
            :param hole: Radius of central hole
            :type hole: float; default 0.

        Returns:
            :return prof_eval: Value of the pixels (DN)
            :rtype prof_eval: numpy.ndarray [n] (float)
            :return prof_pos: Indices of the pixels
            :rtype prof_pos: numpy.ndarray [n] (int)
            :return mesh_grid: x, y coordinates of the pixels respect to the PSF
            :rtype mesh_grid: list [2] (numpy.ndarray (int))
        """

        # Calculate PSF
        psf_eval, prof_pos, mesh_grid = self.eval_psf(side_x, side_y, x, y,
                                                      fit_rad=fit_rad, rad=rad,
                                                      no_rad=no_rad, hole=hole)

        # Express the number of counts
        if counts is None:
            if m is None:
                counts = catalog.Catalog.peak_to_counts(peak, self.peak)
            else:
                counts = catalog.Catalog.m_to_counts(m, m1c=self.m1c)

        # Calculate profile
        prof_eval = psf_eval * counts

        return prof_eval, prof_pos, mesh_grid
        # TODO: if evaluation methods are useful for an empirical PSF too, move
        #  this function to the Psf class and modify the docstring


class Gauss(Analytic):
    """Analytic PSF defined by a bivariate Gaussian function.
    The function normalized to unit and centered at (0, 0) is defined
    f(x, y) =
        1 / (2 pi s1 s2)
        e^(-(
        x^2 ((cos(theta)^2 / (2 s1^2)) + (sin(theta)^2 / (2 s2^2))) +
        y^2 ((sin(theta)^2 / (2 s1^2)) + (cos(theta)^2 / (2 s2^2))) +
        xy ((sin(2 theta) / (2 s1^2)) - (sin(2 theta) / (2 s2^2)))))
    , with x, y the distance coordinates from the center of the function, s1 and
    s2 the characteristic radii (standard deviations) and theta the position
    angle of the first characteristic radius. The FWHM is
    FWHM = 2 s sqrt(2 ln(2))
    """

    def __init__(self, s1, s2, theta=0, conf=None):
        """Initialize a Gaussian analytic PSF.

        Parameters:
            :param s1: First characteristic radius (px)
            :type s1: int, float
            :param s2: Second characteristic radius (px)
            :type s2: int, float
            :param theta: Position angle of the first characteristic radius
                (rad)
            :type theta: int, float; default 0
            :param conf: Configuration
            :type conf: config.Config; optional

        Attributes:
            :attr s1: First characteristic radius (px)
            :atype s1: float
            :attr s2: Second characteristic radius (px)
            :atype s2: float
            :attr theta: Position angle of the first characteristic radius (rad)
            :atype theta: float
        """

        # Check arguments
        if not isinstance(s1, (np.integer, int, float)):
            err_msg = "'s1' has to be int or float"
            raise message.NephastError(err_msg)

        if not s1 > 0:
            err_msg = "'s1' has to be positive"
            raise message.NephastError(err_msg)

        if not isinstance(s2, (np.integer, int, float)):
            err_msg = "'s2' has to be int or float"
            raise message.NephastError(err_msg)

        if not s2 > 0:
            err_msg = "'s2' has to be positive"
            raise message.NephastError(err_msg)

        if not isinstance(theta, (np.integer, int, float)):
            err_msg = "'theta' has to be int or float"
            raise message.NephastError(err_msg)

        if not isinstance(conf, (config.Config, type(None))):
            err_msg = "'conf' has to be config.Config"
            raise message.NephastError(err_msg)

        # Define configuration
        if conf is None:
            conf = config.Config()

        # Store attributes
        self.s1 = np.float64(s1)
        self.s2 = np.float64(s2)
        self.theta = np.float64(theta)

        # Calculate FWHMa
        fwhm = 2 * np.array([self.s1, self.s2]) * np.sqrt(2 * np.log(2))

        # Calculate the peak
        peak = 1 / (2 * np.pi * self.s1 * self.s2)

        # Pass to parent class
        super().__init__(peak, [fwhm[0], fwhm[1]], 'gauss', conf)

    def f(self, x, y):
        """Define the Gaussian function. Calculate the profile value.

        Parameters:
            :param x: x coordinates from the PSF center (px)
            :type x: int, float
            :param y: y coordinates from the PSF center (px)
            :type y: int, float

        Returns:
            :return z: Values of the Gaussian PSF
            :rtype z: float
        """

        # Function
        z = self.peak * \
            np.exp(-(((x ** 2) *
                      (((np.cos(self.theta) ** 2) / (2 * (self.s1 ** 2))) +
                       ((np.sin(self.theta) ** 2) / (2 * (self.s2 ** 2))))) +
                     ((y ** 2) *
                      (((np.sin(self.theta) ** 2) / (2 * (self.s1 ** 2))) +
                       ((np.cos(self.theta) ** 2) / (2 * (self.s2 ** 2))))) +
                     (x * y *
                      ((np.sin(2 * self.theta) / (2 * (self.s1 ** 2))) -
                       (np.sin(2 * self.theta) / (2 * (self.s2 ** 2)))))))

        return z

    def df_dx0(self, x, y, x0, y0):
        """Calculate the derivative of a normalized Gaussian profile respect to
        its center coordinate x0.

        Parameters:
            :param x: x coordinate (px)
            :type x: int, float, numpy.ndarray [m, n] (int, float)
            :param y: y coordinate (px)
            :type y: int, float, numpy.ndarray [m, n] (int, float)
            :param x0: x coordinate of the PSF center (px)
            :type x: int, float
            :param y0: y coordinate of the PSF center (px)
            :type y: int, float

        Returns:
            :return z: Value of the Gaussian PSF derivative respect to x0
            :rtype z: float, numpy.ndarray [m, n] (float)
        """

        # Derivative respect to x0
        z = self.peak * \
            np.exp(-((((x - x0) ** 2) *
                      (((np.cos(self.theta) ** 2) / (2 * (self.s1 ** 2))) +
                       ((np.sin(self.theta) ** 2) / (2 * (self.s2 ** 2))))) +
                     (((y - y0) ** 2) *
                      (((np.sin(self.theta) ** 2) / (2 * (self.s1 ** 2))) +
                       ((np.cos(self.theta) ** 2) / (2 * (self.s2 ** 2))))) +
                     ((x - x0) * (y - y0) *
                      ((np.sin(2 * self.theta) / (2 * (self.s1 ** 2))) -
                       (np.sin(2 * self.theta) / (2 * (self.s2 ** 2))))))) * \
            ((2 * (x - x0)) + (y - y0))

        return z

    def df_dy0(self, x, y, x0, y0):
        """Calculate the derivative of a normalized Gaussian profile respect to
        its center coordinate y0.

        Parameters:
            :param x: x coordinate (px)
            :type x: int, float, numpy.ndarray [m, n] (int, float)
            :param y: y coordinate (px)
            :type y: int, float, numpy.ndarray [m, n] (int, float)
            :param x0: x coordinate of the PSF center (px)
            :type x: int, float
            :param y0: y coordinate of the PSF center (px)
            :type y: int, float

        Returns:
            :return z: Value of the Gaussian PSF derivative respect to y0
            :rtype z: float, numpy.ndarray [m, n] (float)
        """

        # Derivative respect to y0
        z = self.peak * \
            np.exp(-((((x - x0) ** 2) *
                      (((np.cos(self.theta) ** 2) / (2 * (self.s1 ** 2))) +
                       ((np.sin(self.theta) ** 2) / (2 * (self.s2 ** 2))))) +
                     (((y - y0) ** 2) *
                      (((np.sin(self.theta) ** 2) / (2 * (self.s1 ** 2))) +
                       ((np.cos(self.theta) ** 2) / (2 * (self.s2 ** 2))))) +
                     ((x - x0) * (y - y0) *
                      ((np.sin(2 * self.theta) / (2 * (self.s1 ** 2))) -
                       (np.sin(2 * self.theta) / (2 * (self.s2 ** 2))))))) * \
            ((2 * (y - y0)) + (x - x0))

        return z


class Moffat(Analytic):
    """Analytic PSF defined by a bivariate Moffat function.
    The function normalized to unit and centered at (0, 0) is defined
    [ref. 1,2,3]
    f(x, y) =
        (beta - 1) / (pi s1 s2) /
        (1 +
        x^2 ((cos(theta) / s1)^2 + (sin(theta) / s2)^2) +
        y^2 ((sin(theta) / s1)^2 + (cos(theta) / s2)^2) +
        x y (sin(2 theta) / s1^2 - sin(2 theta) / s2^2))^beta
    , with x, y the distance coordinates from the center of the function, s1 and
    s2 the characteristic radii, theta the position angle of the first
    characteristic radius and beta the shape parameter. The FWHM is
    FWHM = 2 s sqrt(2^(1 / beta) - 1)
    """

    def __init__(self, s1, s2, theta=0, beta=3, conf=None):
        """Initialize a Moffat analytic PSF.
        The beta parameter is required to be greater than 1, otherwise the
        integral would diverge. A value greater than 2 is likely to produce a
        profile that is normalized to at least 0.9 within a typical PSF radius.
        The smaller the value of beta, the greater the intensity in the wings of
        the profile, compared to the core.

        Parameters:
            :param s1: First characteristic radius (px)
            :type s1: int, float
            :param s2: Second characteristic radius (px)
            :type s2: int, float
            :param theta: Position angle of the first characteristic radius
                (rad)
            :type theta: int, float; default 0
            :param beta: Shape parameter
            :type beta: int, float; default 3
            :param conf: Configuration
            :type conf: config.Config; optional

        Attributes:
            :attr s1: First characteristic radius (px)
            :atype s1: float
            :attr s2: Second characteristic radius (px)
            :atype s2: float
            :attr theta: Position angle of the first characteristic radius (rad)
            :atype theta: float
            :attr beta: Shape parameter
            :atype beta: float
        """

        # Check arguments
        if not isinstance(s1, (np.integer, int, float)):
            err_msg = "'s1' has to be int or float"
            raise message.NephastError(err_msg)

        if not s1 > 0:
            err_msg = "'s1' has to be positive"
            raise message.NephastError(err_msg)

        if not isinstance(s2, (np.integer, int, float)):
            err_msg = "'s2' has to be int or float"
            raise message.NephastError(err_msg)

        if not s2 > 0:
            err_msg = "'s2' has to be positive"
            raise message.NephastError(err_msg)

        if not isinstance(theta, (np.integer, int, float)):
            err_msg = "'theta' has to be int or float"
            raise message.NephastError(err_msg)

        if not isinstance(beta, (np.integer, int, float)):
            err_msg = "'beta' has to be int or float"
            raise message.NephastError(err_msg)

        if not beta > 1:
            err_msg = "'beta' has to be > 1"
            raise message.NephastError(err_msg)

        if not beta >= 2:
            warn_msg = "'beta' is suggested to be >= 2"
            warnings.warn(warn_msg, message.NephastWarning)

        if not isinstance(conf, (config.Config, type(None))):
            err_msg = "'conf' has to be config.Config"
            raise message.NephastError(err_msg)

        # Define configuration
        if conf is None:
            conf = config.Config()

        # Store attributes
        self.s1 = np.float64(s1)
        self.s2 = np.float64(s2)
        self.theta = np.float64(theta)
        self.beta = np.float64(beta)

        # Calculate FWHMa
        fwhm = 2 * np.array([self.s1, self.s2]) * \
            np.sqrt((2 ** (1 / self.beta)) - 1)

        # Calculate the peak
        peak = (self.beta - 1) / (np.pi * self.s1 * self.s2)

        # Pass to parent class
        super().__init__(peak, [fwhm[0], fwhm[1]], 'moffat', conf)

    def f(self, x, y):
        """Define the Moffat function. Calculate the profile value.

        Parameters:
            :param x: x coordinates from the PSF center (px)
            :type x: int, float
            :param y: y coordinates from the PSF center (px)
            :type y: int, float

        Returns:
            :return z: Values of the Moffat PSF
            :rtype z: float
        """

        # Function
        z = self.peak / ((1 +
                          ((x ** 2) *
                           (((np.cos(self.theta) / self.s1) ** 2) +
                            ((np.sin(self.theta) / self.s2) ** 2))) +
                          ((y ** 2) *
                           (((np.sin(self.theta) / self.s1) ** 2) +
                            ((np.cos(self.theta) / self.s2) ** 2))) +
                          ((x * y) *
                           ((np.sin(2 * self.theta) / (self.s1 ** 2)) -
                            (np.sin(2 * self.theta) / (self.s2 ** 2)))))
                         ** self.beta)

        return z

    def df_dx0(self, x, y, x0, y0):
        """Calculate the derivative of a normalized Moffat profile respect to
        its center coordinate x0.

        Parameters:
            :param x: x coordinate (px)
            :type x: int, float, numpy.ndarray [m, n] (int, float)
            :param y: y coordinate (px)
            :type y: int, float, numpy.ndarray [m, n] (int, float)
            :param x0: x coordinate of the PSF center (px)
            :type x: int, float
            :param y0: y coordinate of the PSF center (px)
            :type y: int, float

        Returns:
            :return z: Value of the Moffat PSF derivative respect to x0
            :rtype z: float, numpy.ndarray [m, n] (float)
        """

        # Derivative respect to x0
        z = self.peak * self.beta * \
            ((2 * (x - x0) * (((np.cos(self.theta) / self.s1) ** 2) +
                              ((np.sin(self.theta) / self.s2) ** 2))) +
             ((y - y0) * ((np.sin(2 * self.theta) / (self.s1 ** 2)) -
                          (np.sin(2 * self.theta) / (self.s2 ** 2))))) / \
            ((1 +
              (((x - x0) ** 2) *
               (((np.cos(self.theta) / self.s1) ** 2) +
                ((np.sin(self.theta) / self.s2) ** 2))) +
              (((y - y0) ** 2) *
               (((np.sin(self.theta) / self.s1) ** 2) +
                ((np.cos(self.theta) / self.s2) ** 2))) +
              (((x - x0) * (y - y0)) *
               ((np.sin(2 * self.theta) / (self.s1 ** 2)) -
                (np.sin(2 * self.theta) / (self.s2 ** 2))))) **
             (self.beta + 1))

        return z

    def df_dy0(self, x, y, x0, y0):
        """Calculate the derivative of a normalized Moffat profile respect to
        its center coordinate y0.

        Parameters:
            :param x: x coordinate (px)
            :type x: int, float, numpy.ndarray [m, n] (int, float)
            :param y: y coordinate (px)
            :type y: int, float, numpy.ndarray [m, n] (int, float)
            :param x0: x coordinate of the PSF center (px)
            :type x: int, float
            :param y0: y coordinate of the PSF center (px)
            :type y: int, float

        Returns:
            :return z: Value of the Moffat PSF derivative respect to y0
            :rtype z: float, numpy.ndarray [m, n] (float)
        """

        # Derivative respect to y0
        z = self.peak * self.beta * \
            ((2 * (y - y0) * (((np.sin(self.theta) / self.s1) ** 2) +
                              ((np.cos(self.theta) / self.s2) ** 2))) +
             ((x - x0) * ((np.sin(2 * self.theta) / (self.s1 ** 2)) -
                          (np.sin(2 * self.theta) / (self.s2 ** 2))))) / \
            ((1 +
              (((x - x0) ** 2) *
               (((np.cos(self.theta) / self.s1) ** 2) +
                ((np.sin(self.theta) / self.s2) ** 2))) +
              (((y - y0) ** 2) *
               (((np.sin(self.theta) / self.s1) ** 2) +
                ((np.cos(self.theta) / self.s2) ** 2))) +
              (((x - x0) * (y - y0)) *
               ((np.sin(2 * self.theta) / (self.s1 ** 2)) -
                (np.sin(2 * self.theta) / (self.s2 ** 2))))) **
             (self.beta + 1))

        return z
