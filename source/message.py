"""NEPHAST by Paolo Turri <pturri83@gmail.com>
Released under BSD License 2.0

Error and warning messages.
"""


class NephastError(Exception):
    """Exception class for printing error messages.
    """

    def __init__(self, message):
        """Print an error message.

        Parameters:
            :param message: Error message to print
            :type message: str
        """

        # Print message
        self.message = message


class NephastWarning(Warning):
    """Warning class for printing warning messages.
    """

    def __init__(self, message):
        """Print a warning message.

        Parameters:
            :param message: Warning message to print
            :type message: str
        """

        # Print message
        self.message = message
