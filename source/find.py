"""NEPHAST by Paolo Turri <pturri83@gmail.com>
Released under BSD License 2.0

This module deals with finding stars.
"""


import copy

import numpy as np
from scipy import signal

import analyze
import catalog
import config
import image
import message
import object
import observation
import psf


__all__ = ['Finder']


class Finder(object.Object):
    """Finder object to detect stellar sources on an image by fitting a PSF
    across the frame.
    """

    def __init__(self, imag, obs, psfun, conf=None):
        """Initialize the finder by providing an image and PSF model.
        If a spatially variable PSF is used, finding stars is more time
        consuming.

        Parameters:
            :param imag: Image fitted
            :type imag: image.Image
            :param obs: Observation
            :type obs: observation.Observation
            :param psfun: Point spread function fitted
            :type psfun: psf.Psf
            :param conf: Configuration
            :type conf: config.Config; optional

        # Attributes:
            :attr image: Image fitted
            :atype image: image.Image
            :attr obs: Observation
            :atype obs: observation.Observation
            :attr psf: Point spread function fitted
            :atype psf: psf.Psf
            :attr find_radius: Fitting radius during the finding, in units of
                arithmetic mean FWHM
            :atype find_radius: float
            :attr find_bkg_in: Background inner radius during the finding (px).
                'None' uses whole image
            :atype find_bkg_in: float
            :attr find_bkg_out: Background outer radius during the finding (px).
                'None' uses whole image
            :atype find_bkg_out: float
            :attr find_npx: Minimum number of good pixels that an object must
                have for finding, or it is dropped
            :atype find_npx: int
            :attr find_std: Number of background STD above which a star is found
            :atype find_std: float
            :attr sharp: Sharpness limits for stars found (minimum,vmaximum)
            :atype sharp: list [2] (float)
            :return cat_init: Catalog of candidate detections before applying
                cuts
            :rtype cat_init: catalog.Catalog or None
            :attr cat: Catalog of candidate detections after applying cuts
            :atype cat: catalog.Catalog or None
            :attr fit: Fitting image used to detect stars
            :atype fit: numpy.ndarray (float) or None
        """

        # Check arguments
        if not isinstance(conf, (config.Config, type(None))):
            err_msg = "'conf' has to be config.Config"
            raise message.NephastError(err_msg)

        # Define configuration
        if conf is None:
            conf = config.Config()

        # Store attributes
        self.image = imag
        self.obs = obs
        self.psf = psfun
        self.find_radius = conf.find_radius
        self.find_bkg_in = conf.find_bkg_in
        self.find_bkg_out = conf.find_bkg_out
        self.find_npx = conf.find_npx
        self.find_std = conf.find_std
        self.sharp = conf.sharp
        self.cat_init = None
        self.cat = None
        self.fit = None

    def __setattr__(self, name, val):
        """Set attribute.

        Parameters:
            :param name: Attribute name
            :type name: str
            :param val: Attribute value
            :type val: any
        """

        # Check attribute
        if name is 'imag':
            if not isinstance(val, image.Image):
                err_msg = "'{0}' has to be image.Image".format(name)
                raise message.NephastError(err_msg)

        if name is 'obs':
            if not isinstance(val, observation.Observation):
                err_msg = "'{0}' has to be observation.Observation".format(name)
                raise message.NephastError(err_msg)

        if name is 'psfun':
            if not isinstance(val, psf.Psf):
                err_msg = "'{0}' has to be psf.Psf".format(name)
                raise message.NephastError(err_msg)

        # Set attribute
        super().__setattr__(name, val)

    def __call__(self, cut_noise=True, cut_sharp=True, verbose=False):
        """Find stars by linear least square fitting of the image using a PSF.

        Parameters:
            :param cut_noise: Apply noise cuts to the catalog of candidate
                detections
            :type cut_noise: bool; default True
            :param cut_sharp: Apply sharpness cuts to the catalog of candidate
                detections
            :type cut_sharp: bool; default True
            :param verbose: Show process messages
            :type verbose: bool; default False

        Returns:
            :return cat: Catalog of candidate detections after applying cuts
            :rtype cat: catalog.Catalog
        """

        # Check arguments
        if not isinstance(cut_noise, bool):
            err_msg = "'cut_noise' has to be bool"
            raise message.NephastError(err_msg)

        if not isinstance(cut_sharp, bool):
            err_msg = "'cut_sharp' has to be bool"
            raise message.NephastError(err_msg)

        if not isinstance(verbose, bool):
            err_msg = "'verbose' has to be bool"
            raise message.NephastError(err_msg)

        # Measure background
        bkg_all = np.ma.median(self.image.array)

        # Prepare fitting scan
        psf_eval = None
        mesh_grid = None
        fit_image = np.ma.masked_array(np.zeros([self.image.side_y,
                                                 self.image.side_x]),
                                       mask=np.tile(False, (self.image.side_y,
                                                            self.image.side_x)))
        noise_image = copy.deepcopy(fit_image)
        bkg_image = copy.deepcopy(fit_image)
        var_rn = (self.obs.rn / self.obs.gain) ** 2

        if not self.psf.var:
            psf_eval, psf_pos, mesh_grid = \
                self.psf.eval_psf(((np.floor(self.find_radius * self.psf.fwhma)
                                    * 2) + 1),
                                  ((np.floor(self.find_radius * self.psf.fwhma)
                                    * 2) + 1),
                                  np.floor(self.find_radius * self.psf.fwhma),
                                  np.floor(self.find_radius * self.psf.fwhma),
                                  rad=self.find_radius * self.psf.fwhma)
            mesh_grid[0] -= int(np.floor(self.find_radius * self.psf.fwhma))
            mesh_grid[1] -= int(np.floor(self.find_radius * self.psf.fwhma))

        # Fitting scan
        if verbose:
            print(("\rFinding stars:   scanning pixel {0:9d} / " +
                   "{1:<9d}").format(0, self.image.n_px), end="")

        verbose_step = 100  # Number of pixels that trigger a scanning message

        for i_px in range(self.image.side_x * self.image.side_y):

            # Evaluate profile
            if verbose:
                if (((i_px + 1) % verbose_step) == 0) or \
                        ((i_px + 1) == self.image.n_px):
                    print(("\rFinding stars:   scanning pixel {0:9d} / " +
                           "{1:<9d}").format((i_px + 1), self.image.n_px),
                          end="")

            x_px = i_px % self.image.side_x
            y_px = i_px // self.image.side_x

            if self.psf.var:
                psf_eval_tmp, _, psf_grid = \
                    self.psf.eval_psf(self.image.side_x, self.image.side_y,
                                      x_px, y_px,
                                      rad=(self.find_radius * self.psf.fwhma))
                image_eval = self.image[psf_grid[0], psf_grid[1]]
            else:
                mesh_grid_tmp = copy.deepcopy(mesh_grid)
                mesh_grid_tmp[0] += x_px
                mesh_grid_tmp[1] += y_px
                mesh_grid_in = (mesh_grid_tmp[0] >= 0) & \
                               (mesh_grid_tmp[0] < self.image.side_x) & \
                               (mesh_grid_tmp[1] >= 0) & \
                               (mesh_grid_tmp[1] < self.image.side_y)
                mesh_grid_tmp[0] = mesh_grid_tmp[0][mesh_grid_in]
                mesh_grid_tmp[1] = mesh_grid_tmp[1][mesh_grid_in]
                psf_eval_tmp = psf_eval[mesh_grid_in]
                image_eval = self.image[mesh_grid_tmp[0], mesh_grid_tmp[1]]

            # Fit profile
            n_good_px = np.sum(~image_eval.mask)

            if n_good_px >= self.find_npx:
                noise = image.Image.noise(image_eval, self.obs.gain,
                                          self.obs.rn)
                weight = 1 / (noise ** 2)
                fit_image[y_px, x_px] = \
                    ((np.ma.sum(psf_eval_tmp * image_eval * weight) *
                      np.ma.sum(weight)) -
                     (np.ma.sum(psf_eval_tmp * weight) *
                      np.ma.sum(image_eval * weight))) / \
                    ((np.ma.sum((psf_eval_tmp ** 2) * weight) *
                      np.ma.sum(weight)) -
                     (np.ma.sum(psf_eval_tmp * weight) ** 2))
            else:
                fit_image.mask[y_px, x_px] = True

            # Evaluate fitting noise
            if n_good_px >= self.find_npx:
                if (self.find_bkg_in is not None) and \
                        (self.find_bkg_out is not None):
                    _, _, bkg_grid = self.psf.psf_pos(self.image.side_x,
                                                      self.image.side_x, x_px,
                                                      y_px,
                                                      rad=self.find_bkg_out,
                                                      hole=self.find_bkg_in)
                    bkg_image[y_px, x_px] = \
                        np.ma.median(self.image[bkg_grid[0], bkg_grid[1]])
                else:
                    bkg_image[y_px, x_px] = bkg_all

                var_sn = bkg_image[y_px, x_px] / self.obs.gain
                var = var_sn + var_rn
                psf_eval_tmp2 = psf_eval_tmp[~image_eval.mask]
                noise_image[y_px, x_px] = \
                    np.sqrt(var / (np.ma.sum(psf_eval_tmp2 ** 2) -
                                   ((np.ma.sum(psf_eval_tmp2) ** 2) /
                                    len(psf_eval_tmp2))))
            else:
                noise_image.mask[y_px, x_px] = True

        if verbose:
            print(("\rFinding stars:   scanning pixel {0:9d} / " +
                   "{0:<9d}").format(self.image.n_px), end="")

        # Detect peaks
        if verbose:
            print("\rFinding stars:   detecting peaks...", end="")

        peaks_ver = np.transpose(signal.argrelmax(fit_image, axis=0))
        peaks_hor = np.transpose(signal.argrelmax(fit_image, axis=1))
        peaks_pos = []

        for i_peak in peaks_ver:
            peak_ver_hor = np.where(np.all((peaks_hor == i_peak), axis=1))[0]

            if peak_ver_hor:
                peaks_pos.append(peaks_hor[peak_ver_hor[0]].tolist())

        peaks_pos = np.asarray(peaks_pos)

        # Apply positive cut
        counts = fit_image[peaks_pos[:, 0], peaks_pos[:, 1]]
        sigma_counts = noise_image[peaks_pos[:, 0], peaks_pos[:, 1]]
        bkgc = bkg_image[peaks_pos[:, 0], peaks_pos[:, 1]]
        counts_positive = (counts > 0)
        peaks_pos = peaks_pos[counts_positive]
        sigma_counts = sigma_counts[counts_positive]
        bkgc = bkgc[counts_positive]
        counts = counts[counts_positive]
        candid_init = catalog.Catalog(peaks_pos[:, 1], peaks_pos[:, 0],
                                      counts=counts, sigma_counts=sigma_counts,
                                      bkgc=bkgc, psfun=self.psf,
                                      original='counts')

        # Calculate noise cut
        if verbose:
            print("\rFinding stars:   calculating cuts...", end="")

        find_counts = np.ma.masked_array(np.full(candid_init.n, np.nan),
                                         mask=([False] * candid_init.n))
        find_bkg = np.ma.masked_array(np.full(candid_init.n, np.nan),
                                      mask=([False] * candid_init.n))

        for i_candid in range(candid_init.n):
            peak_pos = [int(candid_init[i_candid, 'x']),
                        int(candid_init[i_candid, 'y'])]
            find_counts[i_candid] = fit_image[peak_pos[1], peak_pos[0]]
            find_bkg[i_candid] = noise_image[peak_pos[1], peak_pos[0]]

        candid_init.add_col('find_counts', col=find_counts)
        candid_init.add_col('find_bkg', col=find_bkg)

        # Calculate sharpness cut
        sharp = np.ma.masked_array(np.full(candid_init.n, np.nan),
                                   mask=([False] * candid_init.n))

        for i_candid in range(candid_init.n):
            peak_pos = [int(candid_init[i_candid, 'x']),
                        int(candid_init[i_candid, 'y'])]
            peak = self.image[peak_pos[0], peak_pos[1]]

            if np.ma.is_masked(peak):
                sharp.mask[i_candid] = True
            else:
                fit_peak = candid_init[i_candid, 'peak']
                _, _, circ_pos = self.psf.psf_pos(self.image.side_x,
                                                  self.image.side_y,
                                                  peak_pos[0], peak_pos[1],
                                                  rad=(self.find_radius *
                                                       self.psf.fwhma),
                                                  hole=0.5)
                circ = self.image[circ_pos[0], circ_pos[1]]
                circ_mean = np.ma.median(circ)
                sharp[i_candid] = (peak - circ_mean) / fit_peak

        candid_init.add_col('sharp', col=sharp)

        # Prepare outputs
        self.cat_init = candid_init
        self.cat = copy.deepcopy(candid_init)
        self.fit = fit_image

        # Find and apply cuts
        self.cut(cut_noise=cut_noise, cut_sharp=cut_sharp)

        if verbose:
            print("\rStars found: {0:>6d}".format(self.cat.n))

        return self.cat

    def cut(self, cut_noise=True, cut_sharp=True, exclude_sharp_masked=False,
            verbose=False):
        """Calculate and apply cuts to the catalog of candidate detections.

        Parameters:
            :param cut_noise: Apply noise cuts
            :type cut_noise: bool; default True
            :param cut_sharp: Apply sharpness cuts
            :type cut_sharp: bool; default True
            :param exclude_sharp_masked: Exclude objects that do not have a
                sharpness value
            :type exclude_sharp_masked: bool; default False
            :param verbose: Show process messages
            :type verbose: bool; default False
        """

        # Check arguments
        if not isinstance(cut_noise, bool):
            err_msg = "'cut_noise' has to be bool"
            raise message.NephastError(err_msg)

        if not isinstance(cut_sharp, bool):
            err_msg = "'cut_sharp' has to be bool"
            raise message.NephastError(err_msg)

        if not isinstance(verbose, bool):
            err_msg = "'verbose' has to be bool"
            raise message.NephastError(err_msg)

        if not isinstance(exclude_sharp_masked, bool):
            err_msg = "'exclude_sharp_masked' has to be bool"
            raise message.NephastError(err_msg)

        # Apply cuts
        cat_cut = copy.deepcopy(self.cat)

        if cut_noise:
            find_noise = np.ma.where(cat_cut['find_counts'] <
                                     (self.find_std * cat_cut['find_bkg']))[0]
            cat_cut.remove(find_noise)

        if cut_sharp:
            cat_cut = cat_cut.range('sharp', low=self.sharp[0],
                                    high=self.sharp[1],
                                    exclude_masked=exclude_sharp_masked)

        if verbose:
            print("Remaining stars: {0:d} / {1:d}".format(cat_cut.n,
                                                          self.cat.n))

        self.cat = cat_cut

    def plot(self, sharp_lim=None, sharp_percent=None):
        """Plot found stars.
        Sharpness limit values have precedence over the limit percentiles.

        Parameters:
            :param sharp_lim: Sharpness limit values (minimum, maximum)
            :type sharp_lim: list [2] (int, float); optional
            :param sharp_percent: Sharpness limit percentiles (minimum, maximum)
            :type sharp_percent: list [2] (int, float); optional
        """

        # Show plots
        analyze.plot_find(self, sharp_lim=sharp_lim,
                          sharp_percent=sharp_percent)
