"""NEPHAST by Paolo Turri <pturri83@gmail.com>
Released under BSD License 2.0

Multiple stars test.
Generate an image with a field of stars with random positions and magnitudes.
Perform profile fitting astrometry and photometry. Random bad pixels are added.
"""


import analyze
import catalog
import config
import find
import fit
import image
import instrument
import object
import observation
import psf


# Parameters
n_stars = 40  # Number of stars generated
m_range = [19, 22]  # Range of magnitudes of the stars
bkgc = 600  # Background constant intensity (DN/px)
bkgx = -4  # Background x gradient (DN/px/px)
bkgy = 2  # Background y gradient (DN/px/px)
sigma_r = 0.5  # Absolute STD of positions of random starting guess (px)
sigma_counts = 0.1  # Relative STD of counts of random starting guess (DN)
n_bad = 50  # Number of random masked and unmasked bad pixels
r0x = 3  # Moffat first r0 parameter (px)
r0y = 2.5  # Moffat second r0 parameter (px)
beta = 2.5  # Moffat beta parameter
theta = 0.5  # PSF position angle (rad)
new_cat_img = True  # Generate a new catalog and image

# Configure NEPHAST
conf = config.Config()
conf.load('multi_star_config', 'misc')

# Prepare instrument
print("\nAnalysis started\n")
inst = instrument.Instrument(conf=conf)

# Define observation
obs = observation.Observation(inst, conf=conf, bkgc=bkgc, bkgx=bkgx, bkgy=bkgy)

# Choose PSF
psfun = psf.Moffat(r0x, r0y, theta=theta, beta=beta, conf=conf)

# Build catalog
if new_cat_img:
    cat_in = catalog.Catalog.rnd(n_stars, [(-conf.psf_radius - 1),
                                           (conf.sides[0] + conf.psf_radius +
                                            1),
                                           (-conf.psf_radius - 1),
                                           (conf.sides[1] + conf.psf_radius +
                                            1)],
                                 m_range, psfun=psfun, conf=conf)
    cat_in.save_obj(file='cat_in.nph', overwrite=True, conf=conf)
else:
    cat_in = object.Object.load_obj(file='cat_in.nph', conf=conf)

# Simulate image
if new_cat_img:
    new_image = image.Image.from_cat(inst, obs, cat_in, psfun, n_bad=n_bad,
                                     noise=True)
    new_image.add_bad_px(n_bad, val=(2 * max(cat_in['peak'])), masked=False)
    new_image.add_bad_px(n_bad, val=65355)
    new_image.save_obj(file='image.nph', overwrite=True, conf=conf)
else:
    new_image = object.Object.load_obj(file='image.nph', conf=conf)

# Initial guess
# finder = find.Finder(new_image, obs, psfun, conf=conf)
# cat_find = finder.find_all(verbose=True)
# new_image.show(cat=cat_in)#####
# analyze.show_img(finder.fit, cat=cat_find)####
# analyze.show_figures()#####
finder = find.Finder(new_image, obs, psfun, conf=conf)
cat_find = finder(verbose=True)
# TODO: find stars
# cat_find = catalog.Catalog(cat_in['x'].data, cat_in['y'].data,
#                            m=cat_in['m'].data, bkgc=([bkgc] * cat_in.n),
#                            psfun=psfun, conf=conf)
# cat_find.rnd_guess(sigma_r, sigma_counts)

# Fit profiles
fitter = fit.Fitter(cat_find, new_image, psfun, inst, obs, conf=conf)
cat_out = fitter(verbose=True)
cat_drop = fitter.cat_drop
image_model = fitter.image_model
image_sub = fitter.image_sub

# Show result
cat_in_match, cat_out_match, _, _, _, _ = cat_out.match(cat_in, 0.5, dm_tol=0.5)
chi2_r = cat_out['chi2r'].data
print('\nINPUT CATALOG:')
cat_in.print(exp=False)
print('\nOUTPUT CATALOG:')
cat_out.print(exp=False)
print('\nDROPPED CATALOG:')
cat_drop.print()
new_image.show(cat=cat_in)
image_model.show(cat=cat_out, cat_text_col='r')
image_sub.show(cat=cat_drop)
cat_in_match.phast_diff(cat_out_match)
cat_in_match.quiver(cat_out_match, lims=[0, conf.sides[0], 0, conf.sides[1]],
                    buffer=10)
analyze.plot_2(cat_out['m'], chi2_r, labels=["m", r"$\chi^2_r$"])
analyze.show_figures()
print("\nAnalysis completed\n")
