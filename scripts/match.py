"""NEPHAST by Paolo Turri <pturri83@gmail.com>
Released under BSD License 2.0

Match stars test.
Generate two custom catalogs of stars and match them.
"""


import numpy as np

import analyze
import catalog
import config


# Parameters
cat1_lst = [[24, 11, 15.5], [84, 15, 15.5], [44, 53, 16.5], [80, 79, 15.5],
            [65, 32, 17], [7, 85, 16], [55, 67, 15], [88, 15.5, 16.2],
            [34, 56, 17]]  # First catalog
cat2_lst = [[46, 34, 17], [6, 85, 15.5], [64, 32, 16.2], [86, 16, 16],
            [22.5, 12.5, 15.8], [43, 53, 15], [81, 80, 17], [66, 33, 16.5],
            [8, 86, 16.8], [80, 78, 16.7]]  # Second catalog
dr_tol = 3  # Distance tolerance
dm_tol = 1  # Magnitude difference tolerance

# Build catalogs
print("\nAnalysis started\n")
conf = config.Config()
cat1_arr = np.array(cat1_lst)
cat2_arr = np.array(cat2_lst)
cat1 = catalog.Catalog(cat1_arr[:, 0], cat1_arr[:, 1], m=cat1_arr[:, 2],
                       conf=conf)
cat2 = catalog.Catalog(cat2_arr[:, 0], cat2_arr[:, 1], m=cat2_arr[:, 2],
                       conf=conf)

# Match catalogs
cat1_match, cat2_match, _, _, dr, _ = cat1.match(cat2, dr_tol, dm_tol=dm_tol)

# Show result
analyze.plot_cat(cat1, cat2=cat2, intens='m', intens2='m', intens_lims=[15, 17],
                 intens_lims2=[15, 17], lims=[0, 100, 0, 100])
analyze.plot_cat(cat1_match, cat2=cat2_match, intens='m', intens2='m',
                 intens_lims=[15, 17], intens_lims2=[15, 17],
                 lims=[0, 100, 0, 100])
cat1_match.quiver(cat2_match, lims=[0, 100, 0, 100])
analyze.show_figures()
print("\nAnalysis terminated\n")
