"""NEPHAST by Paolo Turri <pturri83@gmail.com>
Released under BSD License 2.0

Collection of configuration parameters for `multi_star.py`.
"""


sides = [100, 100]
gain = 2.64
rn = 32.26
psf_radius = 15
fit_nbkg = 3
