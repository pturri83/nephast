"""NEPHAST by Paolo Turri <pturri83@gmail.com>
Released under BSD License 2.0

Double stars test.
Generate an image with several double stars and several fake stars and perform
profile fitting astrometry and photometry.

Stars:
    'a': fake, close to 'b'
    'b': real, close to 'a' and 'c'
    'c': fake, close to 'b'
    'd': real, close to 'e'
    'e': real, close to 'd'
    'f': fake, isolated
    'g': real, close to 'h'
    'h': real, close to 'g', without initial guess
    'i': real, isolated
    'l': real, close to 'm'
    'm': real, close to 'l'
    'n': real, saturated
"""


import numpy as np

import analyze
import config
import catalog
import fit
import image
import instrument
import observation
import psf


# Parameters
name = ['b', 'd', 'e', 'g', 'h', 'i', 'l', 'm', 'n']  # Names of the stars
x = [32.8, 16, 19, 44, 48, 50, 20, 23, 10]  # x coordinates of the stars (px)
y = [20.4, 20, 26, 12, 17, 30, 10, 13, 30]  # y coordinates of the stars (px)
m = [19, 18.5, 19.5, 19, 20.5, 19, 19, 19, 17.5]  # Instrumental magnitudes of
# the stars
name_guess = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'i', 'l', 'm', 'n']  # Names of
# the guessed stars
x_guess = [32.1, 32.9, 33.4, 16.5, 19.3, 41.5, 45, 49.5, 19.9, 23.1, 9.7]  # x
# coordinate guesses (px)
y_guess = [20.2, 20.7, 21.6, 19.5, 26.1, 31.2, 13, 30.5, 9.9, 13.1, 30.2]  # y
# coordinate guesses (px)
m_guess = [19.5, 19.3, 20, 18.2, 19.7, 19.7, 19.1, 18.9, 19.1, 19.2, 17.6]
# Instrumental magnitude guesses
bkgc = 10  # Background constant intensity (DN/px)
bkgx = 2  # Background x gradient (DN/px/px)
bkgy = 1  # Background y gradient (DN/px/px)
img_sides = [60, 40]  # Image sides (width, height) (px)
gain = 2.64  # Detector gain (e-/DN)
rn = 32.26  # Read noise (e-)
psf_radius = 20  # PSF radius (px)
r0x = 5  # Moffat first r0 parameter (px)
r0y = 3  # Moffat second r0 parameter (px)
beta = 2.5  # Moffat beta parameter
theta = 0.5  # PSF position angle (rad)

# Configure NEPHAST
conf = config.Config()
conf.sides = img_sides
conf.gain = gain
conf.rn = rn
conf.psf_radius = psf_radius
conf.fit_nbkg = 3
conf.satur = 1700

# Prepare instrument
print("\nAnalysis started\n")
inst = instrument.Instrument(conf=conf)

# Define observation
obs = observation.Observation(inst, conf=conf, bkgc=bkgc, bkgx=bkgx, bkgy=bkgy)

# Choose PSF
psfun = psf.Moffat(r0x, r0y, theta=theta, beta=beta, conf=conf)

# Build catalog
cat_in = catalog.Catalog(x, y, name=name, m=m, conf=conf)

# Simulate image
new_image = image.Image.from_cat(inst, obs, cat_in, psfun, noise=True)

# Initial guess
bkgc_guess = [np.median(new_image.array.data)] * len(name_guess)
cat_guess = catalog.Catalog(x_guess, y_guess, name=name_guess, m=m_guess,
                            bkgc=bkgc_guess, psfun=psfun, conf=conf)

# Fit profiles
fitter = fit.Fitter(cat_guess, new_image, psfun, inst, obs, conf=conf)
cat_out = fitter(verbose=True)
cat_drop = fitter.cat_drop
image_model = fitter.image_model
image_sub = fitter.image_sub

# Show result
print('\n\nINPUT CATALOG:')
cat_in.print(exp=False)
print('\n\nOUTPUT CATALOG:')
cat_out.print()
print('\n\nDROPPED CATALOG:')
cat_drop.print()
cat_in_match, cat_out_match, _, _, _, _ = cat_out.match(cat_in, 0.5, dm_tol=0.5)
new_image.show(cat=cat_guess)
image_model.show(cat=cat_out)
image_sub.show(cat=cat_drop)
fitter.plot(1)
fitter.plot(3)
fitter.plot(4)
fitter.plot(5)
fitter.plot(6)
fitter.plot(7)
fitter.plot(8)
fitter.plot(9)
analyze.show_figures()
print("\nAnalysis terminated\n")
