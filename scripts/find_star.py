"""NEPHAST by Paolo Turri <pturri83@gmail.com>
Released under BSD License 2.0

Find stars test.
Generate an image with a field of stars with random positions and magnitudes.
Find them and their approximate astrometry and photometry. Random bad pixels
are added.
"""


import analyze
import catalog
import config
import find
import image
import instrument
import object
import observation
import psf


# Parameters
n_stars = 100  # Number of stars generated
m_range = [20, 24]  # Range of magnitudes of the stars
bkgc = 600  # Background constant intensity (DN/px)
bkgx = -4  # Background x gradient (DN/px/px)
bkgy = 2  # Background y gradient (DN/px/px)
sigma_r = 0.5  # Absolute STD of positions of random starting guess (px)
sigma_counts = 0.1  # Relative STD of counts of random starting guess (DN)
n_bad = 50  # Number of random masked and unmasked bad pixels
r0 = 2  # Moffat r0 parameter (px)
new_cat_img = True  # Generate a new catalog and image

# Configure NEPHAST
conf = config.Config()
conf.load('find_star_config', 'misc')

# Prepare instrument
print("\nAnalysis started\n")
inst = instrument.Instrument(conf=conf)

# Define observation
obs = observation.Observation(inst, conf=conf, bkgc=bkgc, bkgx=bkgx, bkgy=bkgy)

# Choose PSF
psfun = psf.Moffat(r0, r0, conf=conf)

# Build catalog
if new_cat_img:
    cat_in = catalog.Catalog.rnd(n_stars, [(-conf.psf_radius - 1),
                                           (conf.sides[0] + conf.psf_radius +
                                            1),
                                           (-conf.psf_radius - 1),
                                           (conf.sides[1] + conf.psf_radius +
                                            1)],
                                 m_range, psfun=psfun, conf=conf)
    cat_in.save_obj(file='cat_in.nph', overwrite=True, conf=conf)
else:
    cat_in = object.Object.load_obj(file='cat_in.nph', conf=conf)

# Simulate image
if new_cat_img:
    new_image = image.Image.from_cat(inst, obs, cat_in, psfun, n_bad=n_bad,
                                     noise=True)
    vmin = new_image.min_mask
    vmax = new_image.max_mask
    new_image.add_bad_px(n_bad, val=(2 * max(cat_in['peak'])), masked=False)
    new_image.add_bad_px(n_bad, val=65355)
    new_image.save_obj(file='image.nph', overwrite=True, conf=conf)
else:
    new_image = object.Object.load_obj(file='image.nph', conf=conf)
    vmin = new_image.min_mask
    vmax = new_image.max_mask

# Initial guess
finder = find.Finder(new_image, obs, psfun, conf=conf)
cat_find = finder(verbose=True)

# Show result
cat_in_match, cat_find_match, cat_diff1, cat_diff2, _, _ = \
    cat_find.match(cat_in, 1, dm_tol=0.5)
print('\nINPUT CATALOG:')
cat_in.print(exp=False)
print('\nOUTPUT CATALOG:')
cat_find.print(exp=False)
new_image.show(cat=cat_in, vmin=vmin, vmax=vmax, title="Stars created")
new_image.show(cat=finder.cat_init, vmin=vmin, vmax=vmax,
               title="Stars found (raw)")
new_image.show(cat=cat_find, vmin=vmin, vmax=vmax, title="Stars found (clean)")
new_image.show(cat=cat_diff2, vmin=vmin, vmax=vmax, title="Stars missed")
new_image.show(cat=cat_diff1, vmin=vmin, vmax=vmax, title="Stars fake")
finder.plot(sharp_lim=[0, 15])
cat_diff2.f_lumin(m_bin=0.5, title="Stars missed")
cat_diff1.f_lumin(m_bin=0.5, title="Stars fake")
analyze.show_figures()
print("\nAnalysis completed\n")
