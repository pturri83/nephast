"""NEPHAST by Paolo Turri <pturri83@gmail.com>
Released under BSD License 2.0

Test for different methods of pixel evaluation with different sampling
densities.
The tests are repeated interlaced to reduce the effect of computer hiccups on
results.
"""

import time

from matplotlib import pyplot as plt
import numpy as np

import analyze
import catalog
import image
import instrument
import observation
import psf
import config


# Test parameters
radius = 30  # PSF radius (px)
n_tests = 10  # Number of tests per method
grids = [1, 2, 5, 10, 20, 50, 100]  # Pixel evaluation grid densities tested

# Prepare the observation
# FIXME: This test is broken, the "corner" method doesn't work
print("\nAnalysis started\n")
methods = ['integral', 'center', 'corners'] + ['grid' for i in grids]
methods_lab = ['integral', 'center', 'corners'] + \
              ['grid (' + str(i) + ')' for i in grids]
grids_new = [1, 1, 1] + grids
conf = config.Config()
conf.sides = [radius * 2] * 2
conf.psf_radius = radius
inst = instrument.Instrument(conf=conf)
obs = observation.Observation(inst, conf=conf)
psfun = psf.Moffat(2, 2, conf=conf)
# TODO: change the PSF to Gaussian
cat = catalog.Catalog([radius - 0.5], [radius - 0.5], m=[conf.m1c], psfun=psfun,
                      conf=conf)

# Find number of pixels evaluated
mesh_grid = np.mgrid[: (radius * 4), : (radius * 4)]
dist_x_grid = np.concatenate(mesh_grid[1]) - round(radius * 2)
dist_y_grid = np.concatenate(mesh_grid[0]) - round(radius * 2)
dist_grid = np.hypot(dist_x_grid, dist_y_grid)
psf_npx = len(np.where(dist_grid <= radius)[0])
print("PSF radius            =  {0:d} px".format(radius))
print("Pixels evaluated      =  {0:d}\n".format(psf_npx))

# Test evaluation modes
t = [[] for i_t in range(len(methods))]
imgs = [[] for i in range(len(methods))]
res = []
imgs_res = []
img_where = []
img_ref = []

for i_test in range(n_tests):

    for i_method, method in enumerate(methods):

        # Select evaluation method
        print("\rEvaluation test {0:2d} / {1} ...".format((i_test + 1),
                                                          n_tests), end="")
        psfun.set_method(method)
        psfun.set_grid(grids_new[i_method])

        # Generate image
        t0 = time.time()
        imgs[i_method].append(image.Image.from_cat(inst, obs, cat, psfun))
        t[i_method].append(time.time() - t0)

        # Measure image
        if i_test == 0:
            res.append(np.abs(1 - np.sum(imgs[i_method][0].array)))

            if i_method == 0:
                img_where = np.where(imgs[0][0].array)
                img_ref = imgs[0][0].array[img_where]

            if i_method > 0:
                img = imgs[i_method][0].array[img_where]
                imgs_res.append((img - img_ref) / img_ref)

# Show results
print("\n")
t_mean = []
t_mean_px = []
t_min = []
t_max = []
t_std = []
res_tot = []
res_max = []
res_std = []

for i_method in range(len(methods)):

    # Show timing info
    t_mean.append(np.mean(t[i_method]))
    t_mean_px.append(1000 * t_mean[i_method] / psf_npx)
    t_min.append(np.min(t[i_method]))
    t_max.append(np.max(t[i_method]))
    t_std.append(np.std(t[i_method]))
    method_more = ''

    if i_method == 0:
        method_more = '(reference) '

    print("--- Evaluation method '{0}' {1}---".format(methods_lab[i_method],
                                                      method_more))
    print("Mean time              =  {0:.3f} s".format(t_mean[i_method]))
    print("Minimum time           =  {0:.3f} s".format(t_min[i_method]))
    print("Maximum time           =  {0:.3f} s".format(t_max[i_method]))
    print("STD time               =  {0:.3f} s".format(t_std[i_method]))
    print("Mean time per pixel    =  {0:.3f} ms\n".format(t_mean_px[i_method]))

    # Show residual info
    res_tot.append(res[i_method])
    print("Flux residual          =  {0:.4%} ".format(res_tot[i_method]))

    if i_method > 0:
        res_max.append(np.max(np.abs(imgs_res[i_method - 1])))
        res_std.append(np.std(imgs_res[i_method - 1]))
        print("Maximum residual       =  {0:.4%}".format(res_max[i_method - 1]))
        print("STD residual           =  {0:.4%}".format(res_std[i_method - 1]))

    print()

# Plot residual
method_col = np.zeros((len(grids), 4))
method_col[:, 0] = 1.0
method_col[:, 3] = [(1 - i) for i in np.linspace(0, 1, (len(grids) + 1))][:-1]
method_col = np.vstack(([0.8, 0, 0.8, 1], [0, 0, 1, 1], method_col))
fig, ax = plt.subplots(figsize=(8, 4.5))
barlist = plt.bar(np.arange(len(methods) - 1), res_max, width=1, edgecolor='k')

for i_col in range(len(methods) - 1):
    barlist[i_col].set_facecolor(method_col[i_col])

ax.set_yscale('log')
plt.xticks(np.arange(len(methods) - 1),
           [i.capitalize() for i in methods_lab[1:]])
plt.tick_params(axis='x', labelsize=8)
plt.ylabel('Maximum relative residual')

# Plot timing
fig, ax = plt.subplots(figsize=(8, 4.5))
barlist = plt.bar(np.arange(len(methods) - 1), t_mean_px[1:], width=1,
                  edgecolor='k')

for i_col in range(len(methods) - 1):
    barlist[i_col].set_facecolor(method_col[i_col])

plt.xticks(np.arange(len(methods) - 1),
           [i.capitalize() for i in methods_lab[1:]])
plt.tick_params(axis='x', labelsize=8)
plt.ylabel('Mean time per pixel (ms)')
analyze.show_figures()
print("\nAnalysis completed\n")
