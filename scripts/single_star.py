"""NEPHAST by Paolo Turri <pturri83@gmail.com>
Released under BSD License 2.0

Single star test.
Generate an image with a single star using an analytic PSF and perform profile
fitting astrometry and photometry.
"""


import analyze
import catalog
import config
import fit
import image
import instrument
import observation
import psf


# Parameters
x = 34.8  # x coordinate of the star (px)
dx = 0.7  # Perturbation of the x coordinate guess (px)
y = 22.3  # y coordinate of the star (px)
dy = -0.4  # Perturbation of the y coordinate guess (px)
m = 20  # Instrumental magnitude of the star
dm = 0.5  # Perturbation of the instrumental magnitude guess
bkgc = 45  # Background constant intensity (DN/px)
dbkgc = 5  # Perturbation of the background constant intensity guess (DN/px)
bkgx = 2  # Background x gradient (DN/px/px)
bkgy = 1  # Background y gradient (DN/px/px)
img_sides = [60, 40]  # Image sides (width, height) (px)
gain = 2.64  # Detector gain (e-/DN)
rn = 32.26  # Detector read noise (e-)
psf_radius = 15  # PSF radius (px)
r0x = 5  # Moffat first r0 parameter (px)
r0y = 3  # Moffat second r0 parameter (px)
beta = 2.5  # Moffat beta parameter
theta = 0.5  # PSF position angle (rad)

# Configure NEPHAST
conf = config.Config()
conf.sides = img_sides
conf.gain = gain
conf.rn = rn
conf.psf_radius = psf_radius
conf.fit_nbkg = 3

# Prepare instrument
print("\nAnalysis started\n")
inst = instrument.Instrument(conf=conf)

# Define observation
obs = observation.Observation(inst, conf=conf, bkgc=bkgc, bkgx=bkgx, bkgy=bkgy)

# Choose PSF
psfun = psf.Moffat(r0x, r0y, theta=theta, beta=beta, conf=conf)

# Build catalog
cat_in = catalog.Catalog([x], [y], m=[m], conf=conf)

# Simulate image
new_image = image.Image.from_cat(inst, obs, cat_in, psfun, noise=True)

# Initial guess
name_guess = ['1']
x_guess = [x + dx]
y_guess = [y + dy]
m_guess = [m + dm]
bkgc_guess = [bkgc + dbkgc]
cat_guess = catalog.Catalog(x_guess, y_guess, name=name_guess, m=m_guess,
                            bkgc=bkgc_guess, psfun=psfun, conf=conf)

# Fit profiles
fitter = fit.Fitter(cat_guess, new_image, psfun, inst, obs, conf=conf)
fitter(verbose=True)
image_model = fitter.image_model
image_sub = fitter.image_sub

# Show result
x_fin = fitter.cat_out['x', 0]
x_res = x_fin - x
x_sigma = fitter.cat_out['sigma_x', 0]
y_fin = fitter.cat_out['y', 0]
y_res = y_fin - y
y_sigma = fitter.cat_out['sigma_y', 0]
m_fin = fitter.cat_out['m', 0]
m_res = m_fin - m
m_sigma = fitter.cat_out['sigma_m', 0]
chi2_r = fitter.cat_out['chi2r', 0]
print("\nResults:         x = {0:.3f} +/- {1:.3f} px".format(x_fin, x_sigma))
print("                 y = {0:.3f} +/- {1:.3f} px".format(y_fin, y_sigma))
print("                 m = {0:.3f} +/- {1:.3f}".format(m_fin, m_sigma))
print("\nResiduals:       Delta_x = {0:.4f} px".format(x_res))
print("                 Delta_y = {0:.4f} px".format(y_res))
print("                 Delta_m = {0:.4f} px".format(m_res))
print("\nReduced chi^2:   chi^2_r = {0:.4f}".format(chi2_r))
new_image.show()
image_model.show()
image_sub.show()
fitter.plot(0)
analyze.show_figures()
print("\nAnalysis completed\n")
